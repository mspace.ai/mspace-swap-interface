FROM node:14.17-alpine as dependencies
RUN apk add --no-cache git
ENV CHILD_CONCURRENCY 1
WORKDIR /app
COPY package.json yarn.lock ./
RUN yarn install
COPY . .
RUN yarn build

FROM hub.coinplug.com/public/httpd:2
EXPOSE 80
COPY --from=dependencies /app/build /usr/local/apache2/htdocs/
