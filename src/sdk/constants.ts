import { METADIUM_CONTRACTS, METATESTNET_CONTRACTS, DEFAULT_CHAIN_ID } from '../constants-metadium'
import JSBI from 'jsbi'

// exports for external consumption
export type BigintIsh = JSBI | bigint | string

export enum ChainId {
  METADIUM = 11,
  META_TESTNET = 12,
}
export const ChainIdDefault = DEFAULT_CHAIN_ID === 11 ? ChainId.METADIUM:ChainId.META_TESTNET;

export enum TradeType {
  EXACT_INPUT,
  EXACT_OUTPUT
}

export enum Rounding {
  ROUND_DOWN,
  ROUND_HALF_UP,
  ROUND_UP
}

export const FACTORY_ADDRESS: { [chainId in ChainId]: string } = {
  [ChainId.METADIUM]: METADIUM_CONTRACTS.Factory,
  [ChainId.META_TESTNET]: METATESTNET_CONTRACTS.Factory
}

export const ROUTER_ADDRESS: { [chainId in ChainId]: string } = {
  [ChainId.METADIUM]: METADIUM_CONTRACTS.Router02,
  [ChainId.META_TESTNET]: METATESTNET_CONTRACTS.Router02
}

export const MSPACE_ADDRESS: { [chainId in ChainId]: string } = {
  [ChainId.METADIUM]: METADIUM_CONTRACTS.MSpace,
  [ChainId.META_TESTNET]: METATESTNET_CONTRACTS.MSpace
}

export const MINING_ADDRESS: { [chainId in ChainId]: string } = {
  [ChainId.METADIUM]: METADIUM_CONTRACTS.Mining,
  [ChainId.META_TESTNET]: METATESTNET_CONTRACTS.Mining
}

export const BANK_ADDRESS: { [chainId in ChainId]: string } = {
  [ChainId.METADIUM]: METADIUM_CONTRACTS.Bank,
  [ChainId.META_TESTNET]: METATESTNET_CONTRACTS.Bank
}

export const EXTRACTOR_ADDRESS: { [chainId in ChainId]: string } = {
  [ChainId.METADIUM]: METADIUM_CONTRACTS.Extractor,
  [ChainId.META_TESTNET]: METATESTNET_CONTRACTS.Extractor
}

export const TIMELOCK_ADDRESS: { [chainId in ChainId]: string } = {
  [ChainId.METADIUM]: METADIUM_CONTRACTS.Timelock,
  [ChainId.META_TESTNET]: METATESTNET_CONTRACTS.Timelock
}

export const MINIMUM_LIQUIDITY = JSBI.BigInt(1000)

// exports for internal consumption
export const ZERO = JSBI.BigInt(0)
export const ONE = JSBI.BigInt(1)
export const TWO = JSBI.BigInt(2)
export const THREE = JSBI.BigInt(3)
export const FIVE = JSBI.BigInt(5)
export const TEN = JSBI.BigInt(10)
export const _100 = JSBI.BigInt(100)
export const _997 = JSBI.BigInt(997)
export const _1000 = JSBI.BigInt(1000)

export enum SolidityType {
  uint8 = 'uint8',
  uint256 = 'uint256'
}

export const SOLIDITY_TYPE_MAXIMA = {
  [SolidityType.uint8]: JSBI.BigInt('0xff'),
  [SolidityType.uint256]: JSBI.BigInt('0xffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff')
}
