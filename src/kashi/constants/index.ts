import { ChainId } from '../../sdk'
import { METADIUM_CONTRACTS, METATESTNET_CONTRACTS } from '../../constants-metadium'

export const BORING_HELPER_ADDRESS = {
    [ChainId.METADIUM]: METADIUM_CONTRACTS.BoringHelperV1,
    [ChainId.META_TESTNET]: METATESTNET_CONTRACTS.BoringHelperV1
}


