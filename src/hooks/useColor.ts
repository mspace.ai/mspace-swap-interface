import { Token, ChainId } from '../sdk'
import Vibrant from 'node-vibrant'
import { shade } from 'polished'
import { useLayoutEffect, useState } from 'react'
import uriToHttp from 'utils/uriToHttp'
import { hex } from 'wcag-contrast'
import { SWAP_HOME_PAGE } from "../constants-metadium"

async function getColorFromToken(token: Token, chainId?: ChainId): Promise<string | null> {
    let path: string;
    if (chainId && [ChainId.METADIUM, ChainId.META_TESTNET].includes(chainId)) {
      path = `${SWAP_HOME_PAGE}/images/assets/${token.address}/logo.png`
    } else {
      path = `https://raw.githubusercontent.com/trustwallet/assets/master/blockchains/ethereum/assets/${token.address}/logo.png`
    }

    return Vibrant.from(path)
        .getPalette()
        .then(palette => {
            if (palette?.Vibrant) {
                let detectedHex = palette.Vibrant.hex
                let AAscore = hex(detectedHex, '#FFF')
                while (AAscore < 3) {
                    detectedHex = shade(0.005, detectedHex)
                    AAscore = hex(detectedHex, '#FFF')
                }
                return detectedHex
            }
            return null
        })
        .catch(() => null)
}

async function getColorFromUriPath(uri: string): Promise<string | null> {
    const formattedPath = uriToHttp(uri)[0]

    return Vibrant.from(formattedPath)
        .getPalette()
        .then(palette => {
            if (palette?.Vibrant) {
                return palette.Vibrant.hex
            }
            return null
        })
        .catch(() => null)
}

export function useColor(token?: Token, chainId?: ChainId) {
    const [color, setColor] = useState('#0094ec')

    useLayoutEffect(() => {
        let stale = false

        if (token) {
            getColorFromToken(token, chainId).then(tokenColor => {
                if (!stale && tokenColor !== null) {
                    setColor(tokenColor)
                }
            })
        }

        return () => {
            stale = true
            setColor('#0094ec')
        }
    }, [token])

    return color
}

export function useListColor(listImageUri?: string) {
    const [color, setColor] = useState('#0094ec')

    useLayoutEffect(() => {
        let stale = false

        if (listImageUri) {
            getColorFromUriPath(listImageUri).then(color => {
                if (!stale && color !== null) {
                    setColor(color)
                }
            })
        }

        return () => {
            stale = true
            setColor('#0094ec')
        }
    }, [listImageUri])

    return color
}
