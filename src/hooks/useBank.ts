import { ethers } from 'ethers'
import { useCallback, useEffect, useState } from 'react'
import Fraction from '../entities/Fraction'
import { useActiveWeb3React } from './useActiveWeb3React'
import { useBankContract, useMSpaceContract } from '../hooks/useContract'
import { useTransactionAdder } from '../state/transactions/hooks'
import { BalanceProps } from './useTokenBalance'

const { BigNumber } = ethers

const useBank = () => {
    const { account } = useActiveWeb3React()
    const addTransaction = useTransactionAdder()
    const mspaceContract = useMSpaceContract(true) // withSigner
    const bankContract = useBankContract(true) // withSigner

    const [allowance, setAllowance] = useState('0')

    const fetchAllowance = useCallback(async () => {
        if (account) {
            try {
                const allowance = await mspaceContract?.allowance(account, bankContract?.address)
                const formatted = Fraction.from(BigNumber.from(allowance), BigNumber.from(10).pow(18)).toString()
                setAllowance(formatted)
            } catch (error) {
                setAllowance('0')
                throw error
            }
        }
    }, [account, bankContract, mspaceContract])

    useEffect(() => {
        if (account && bankContract && mspaceContract) {
            fetchAllowance()
        }
        const refreshInterval = setInterval(fetchAllowance, 10000)
        return () => clearInterval(refreshInterval)
    }, [account, bankContract, fetchAllowance, mspaceContract])

    const approve = useCallback(async () => {
        try {
            const tx = await mspaceContract?.approve(bankContract?.address, ethers.constants.MaxUint256.toString())
            return addTransaction(tx, { summary: 'Approve' })
        } catch (e) {
            return e
        }
    }, [addTransaction, bankContract, mspaceContract])

    const enter = useCallback(
        // todo: this should be updated with BigNumber as opposed to string
        async (amount: BalanceProps | undefined) => {
            if (amount?.value) {
                try {
                    const tx = await bankContract?.enter(amount?.value)
                    return addTransaction(tx, { summary: 'Enter Bank' })
                } catch (e) {
                    return e
                }
            }
        },
        [addTransaction, bankContract]
    )

    const leave = useCallback(
        // todo: this should be updated with BigNumber as opposed to string
        async (amount: BalanceProps | undefined) => {
            if (amount?.value) {
                try {
                    const tx = await bankContract?.leave(amount?.value)
                    return addTransaction(tx, { summary: 'Leave Bank' })
                } catch (e) {
                    return e
                }
            }
        },
        [addTransaction, bankContract]
    )

    return { allowance, approve, enter, leave }
}

export default useBank
