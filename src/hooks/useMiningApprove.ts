import { Contract, ethers } from 'ethers'
import { useCallback } from 'react'
import ERC20_ABI from '../constants/abis/erc20.json'
import { useContract, useMiningContract } from './useContract'
import { useTransactionAdder } from '../state/transactions/hooks'
import { isAddress } from '../utils'

const useApprove = (lpAddress: string) => {
    //const { account } = useActiveWeb3React()
    const addTransaction = useTransactionAdder()
    const miningContract = useMiningContract()
    const lpAddressChecksum = isAddress(lpAddress)
    const lpContract = useContract(lpAddressChecksum ? lpAddressChecksum : undefined, ERC20_ABI, true) // withSigner = true

    const approve = async (lpContract: Contract | null, miningContract: Contract | null) => {
        return lpContract?.approve(miningContract?.address, ethers.constants.MaxUint256.toString())
    }

    const handleApprove = useCallback(async () => {
        try {
            const tx = await approve(lpContract, miningContract)
            return addTransaction(tx, { summary: 'Approve' })
        } catch (e) {
            return e
        }
    }, [addTransaction, lpContract, miningContract])

    return { onApprove: handleApprove }
}

export default useApprove
