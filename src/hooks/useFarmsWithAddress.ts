import { exchangeClientMap, exchangeClientDefault, miningClientMap, miningClientDefault } from 'apollo/client'
import { liquidityPositionSubsetQuery, pairSubsetQuery, poolsQuery } from 'apollo/queries'
import { useCallback, useEffect, useState } from 'react'

import { BigNumber } from '@ethersproject/bignumber'
import Fraction from '../entities/Fraction'
import { POOL_DENY as MAINNET_POOL_DENY } from '../constants'
import { METATESTNET_POOL_DENY } from '../constants-metadium'
import concat from 'lodash/concat'
import { getAverageBlockTime } from 'apollo/getAverageBlockTime'
import orderBy from 'lodash/orderBy'
import range from 'lodash/range'
import { mspace } from '../subgraph'
import { useActiveWeb3React } from 'hooks/useActiveWeb3React'
import { useBoringHelperContract, useMiningContract } from 'hooks/useContract'
import { ChainId, ChainIdDefault, MINING_ADDRESS } from 'sdk'

// Todo: Rewrite in terms of web3 as opposed to subgraph
const useFarms = (address: string) => {
    const [farms, setFarms] = useState<any | undefined>()
    const { account, chainId } = useActiveWeb3React()
    const boringHelperContract = useBoringHelperContract()
    const mining = miningClientMap[chainId ?? ChainIdDefault] ?? miningClientDefault
    console.log("chainId ?? ChainIdDefault", chainId ?? ChainIdDefault);
    const exchange = exchangeClientMap[chainId ?? ChainIdDefault] ?? exchangeClientDefault
    const POOL_DENY = chainId == ChainId.META_TESTNET ? METATESTNET_POOL_DENY : MAINNET_POOL_DENY
    const miningContract = useMiningContract();

    const fetchAllFarms = useCallback(async () => {
      console.log("chainId ?? ChainIdDefault", chainId ?? ChainIdDefault);
        const results = await Promise.all([
            mining.query({
                // results[0]
                query: poolsQuery
            }),
            exchange.query({
                // results[1]
                query: liquidityPositionSubsetQuery,
                variables: { user: MINING_ADDRESS[chainId ?? ChainIdDefault].toLowerCase() }
            }),
            getAverageBlockTime(chainId), // results[2]
            mspace.priceUSD({chainId: chainId}), // results[3]
        ])
        const pools = results[0]?.data.pools
        const pairAddresses = pools
            .map((pool: any) => {
                return pool.pair
            })
            .sort()
        const pairsQuery = await exchange.query({
            query: pairSubsetQuery,
            variables: { pairAddresses }
        })

        const liquidityPositions = results[1]?.data.liquidityPositions
        const averageBlockTime = results[2]
        const metaPrice = results[3]
        const mspacePerBlock = miningContract ? (await miningContract?.mspacePerBlock().catch((err: any) => 0)):0;

        const pairs = pairsQuery?.data.pairs

        const farms = pools
            .filter((pool: any) => {
                return (
                    !POOL_DENY.includes(pool?.id) &&
                    (pairs.find((pair: any) => pair?.id === pool?.pair))
                )
            })
            .map((pool: any) => {
                const pair = pairs.find((pair: any) => pair.id === pool.pair)
                const liquidityPosition = liquidityPositions.find(
                    (liquidityPosition: any) => liquidityPosition.pair.id === pair.id
                )
                const blocksPerHour = 3600 / Number(averageBlockTime)
                const balance = Number(pool.balance / 1e18)
                const totalSupply = pair.totalSupply > 0 ? pair.totalSupply : 0.1
                const reserveUSD = pair.reserveUSD > 0 ? pair.reserveUSD : 0.1
                const balanceUSD = (balance / Number(totalSupply)) * Number(reserveUSD)
                
                const rewardPerBlock =
                    ((pool.allocPoint / pool.owner.totalAllocPoint) * mspacePerBlock) / 1e18
                const roiPerBlock = (rewardPerBlock * metaPrice) / balanceUSD
                const roiPerHour = roiPerBlock * blocksPerHour
                const roiPerDay = roiPerHour * 24
                const roiPerMonth = roiPerDay * 30
                const roiPerYear = roiPerMonth * 12

                return {
                    ...pool,
                    type: 'MLP',
                    symbol: pair.token0.symbol + '-' + pair.token1.symbol,
                    name: pair.token0.name + ' ' + pair.token1.name,
                    pid: Number(pool.id),
                    pairAddress: pair.id,
                    lpBalance: pool.balance,
                    liquidityPair: pair,
                    roiPerBlock,
                    roiPerHour,
                    roiPerDay,
                    roiPerMonth,
                    roiPerYear,
                    rewardPerThousand: 1 * roiPerDay * (1000 / metaPrice),
                    tvl: liquidityPosition?.liquidityTokenBalance
                        ? (pair.reserveUSD / pair.totalSupply) * liquidityPosition.liquidityTokenBalance
                        : 0.1
                }
                
            })

        //console.log('farms:', farms)
        const sorted = orderBy(farms, ['pid'], ['desc'])

        const pids = sorted.map(pool => {
            return pool.pid
        })

        if (address) {
            const userFarmDetails = await boringHelperContract?.pollPools(address, pids)
            const userFarms = userFarmDetails
                .filter((farm: any) => {
                    return farm.balance.gt(BigNumber.from(0)) || farm.pending.gt(BigNumber.from(0))
                })
                .map((farm: any) => {
                    const pid = farm.pid.toNumber()
                    const farmDetails = sorted.find((pair: any) => pair.pid === pid)

                    let deposited = Fraction.from(farm.balance, BigNumber.from(10).pow(18)).toString(18)
                    let depositedUSD =
                        farmDetails.lpBalance && Number(farmDetails.lpBalance / 1e18) > 0
                            ? (Number(deposited) * Number(farmDetails.tvl)) / (farmDetails.lpBalance / 1e18)
                            : 0
                    
                    const pending = Fraction.from(farm.pending, BigNumber.from(10).pow(18)).toString(18)

                    return {
                        ...farmDetails,
                        type: farmDetails.type,
                        depositedLP: deposited,
                        depositedUSD: depositedUSD,
                        pendingMSpace: pending
                    }
                })
            setFarms({ farms: sorted, userFarms: userFarms })
            //console.log('userFarms:', userFarms)
        } else {
            setFarms({ farms: sorted, userFarms: [] })
        }
    }, [address, boringHelperContract])

    useEffect(() => {
        fetchAllFarms()
    }, [fetchAllFarms])

    return farms
}

export default useFarms
