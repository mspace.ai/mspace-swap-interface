import { ethers } from 'ethers'
import { useMiningContract } from 'hooks/useContract'
import { useCallback } from 'react'
import { useTransactionAdder } from '../state/transactions/hooks'

const useMining = () => {
    const addTransaction = useTransactionAdder()
    const miningContract = useMiningContract() // withSigner

    // Deposit
    const deposit = useCallback(
        async (pid: number, amount: string, name: string, decimals = 18) => {
            try {
                const tx = await miningContract?.deposit(pid, ethers.utils.parseUnits(amount, decimals))
                return addTransaction(tx, { summary: `Deposit ${name}` })
            } catch (e) {
                console.error(e)
                return e
            }
        },
        [addTransaction, miningContract]
    )

    // Withdraw
    const withdraw = useCallback(
        async (pid: number, amount: string, name: string, decimals = 18) => {
            try {
                const tx = await miningContract?.withdraw(pid, ethers.utils.parseUnits(amount, decimals))
                return addTransaction(tx, { summary: `Withdraw ${name}` })
            } catch (e) {
                console.error(e)
                return e
            }
        },
        [addTransaction, miningContract]
    )

    const harvest = useCallback(
        async (pid: number, name: string) => {
            try {
                const tx = await miningContract?.harvest(pid)
                return addTransaction(tx, { summary: `Harvest ${name}` })
            } catch (e) {
                console.error(e)
                return e
            }
        },
        [addTransaction, miningContract]
    )

    return { deposit, withdraw, harvest }
}

export default useMining
