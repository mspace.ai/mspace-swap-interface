import { BigNumber } from '@ethersproject/bignumber'
import { useActiveWeb3React } from 'hooks/useActiveWeb3React'
import { useMiningContract } from 'hooks/useContract'
import { useCallback, useEffect, useState } from 'react'
import { useBlockNumber } from 'state/application/hooks'
import Fraction from '../entities/Fraction'

const usePending = (pid: number) => {
    const [balance, setBalance] = useState<string>('0')
    const { account } = useActiveWeb3React()

    const miningContract = useMiningContract()
    const currentBlockNumber = useBlockNumber()

    const fetchPending = useCallback(async () => {
        try {
          const pending = await miningContract?.pendingMSpace(pid, account)
          const formatted = Fraction.from(BigNumber.from(pending), BigNumber.from(10).pow(18)).toString()
          setBalance(formatted)
        } catch (_ignore) {
          setBalance(Fraction.from(BigNumber.from(0), BigNumber.from(10).pow(18)).toString())
        }
    }, [account, miningContract, pid])

    useEffect(() => {
        if (account && miningContract && String(pid)) {
            // pid = 0 is evaluated as false
            fetchPending()
        }
    }, [account, currentBlockNumber, fetchPending, miningContract, pid])

    return balance
}

export default usePending
