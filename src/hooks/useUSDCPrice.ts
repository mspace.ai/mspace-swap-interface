import { JSBI, Price, currencyEquals, Currency, ChainId, WMETA, Token, PairInfo } from '../sdk'
import { useMemo } from 'react'
import { USDC } from '../constants'
import { DEFAULT_CHAIN_ID } from '../constants-metadium'
import { PairState, usePairs } from '../data/Reserves'
import { useActiveWeb3React } from '../hooks/useActiveWeb3React'
import { wrappedCurrency } from '../utils/wrappedCurrency'

/**
 * Returns the price in USDC of the input currency
 * @param currency currency to compute the USDC price of
 */
export default function useUSDCPrice(
    currencyPairs: {
        tokens: [Token | undefined, Token | undefined],
        pairAddress: string | undefined
    }[],
    chainId?: ChainId, currency?: Currency
): Price | undefined {
    const wrapped = wrappedCurrency(currency, chainId)
    const usePairsResult = usePairs(currencyPairs)
    const [
        [metaPairState, metaPair], 
        [usdcPairState, usdcPair], 
        [usdcMetaPairState, usdcMetaPair]
    ] = usePairsResult && usePairsResult.length == 3 ? usePairsResult:[
        [PairState.LOADING, null],
        [PairState.LOADING, null],
        [PairState.LOADING, null]
    ]

    return useMemo(() => {
        if (!currency || !wrapped || !chainId) {
            return undefined
        }
        // handle wmeta/meta
        if (wrapped.equals(WMETA[chainId])) {
            if (usdcPair) {
                const price = usdcPair.priceOf(WMETA[chainId])
                return new Price(currency, USDC[chainId], price.denominator, price.numerator)
            } else {
                return undefined
            }
        }
        // handle usdc
        if (wrapped.equals(USDC[chainId])) {
            return new Price(USDC[chainId], USDC[chainId], '1', '1')
        }

        const metaPairMETAAmount = metaPair?.reserveOf(WMETA[chainId])
        const metaPairMETAUSDCValue: JSBI =
        metaPairMETAAmount && usdcMetaPair
                ? usdcMetaPair.priceOf(WMETA[chainId]).quote(metaPairMETAAmount).raw
                : JSBI.BigInt(0)

        // all other tokens
        // first try the usdc pair
        if (
            usdcPairState === PairState.EXISTS &&
            usdcPair &&
            usdcPair.reserveOf(USDC[chainId]).greaterThan(metaPairMETAUSDCValue)
        ) {
            const price = usdcPair.priceOf(wrapped)
            return new Price(currency, USDC[chainId], price.denominator, price.numerator)
        }
        if (metaPairState === PairState.EXISTS && metaPair && usdcMetaPairState === PairState.EXISTS && usdcMetaPair) {
            if (usdcMetaPair.reserveOf(USDC[chainId]).greaterThan('0') && metaPair.reserveOf(WMETA[chainId]).greaterThan('0')) {
                const metaUsdcPrice = usdcMetaPair.priceOf(USDC[chainId])
                const currencyMetaPrice = metaPair.priceOf(WMETA[chainId])
                const usdcPrice = metaUsdcPrice.multiply(currencyMetaPrice).invert()
                return new Price(currency, USDC[chainId], usdcPrice.denominator, usdcPrice.numerator)
            }
        }
        return undefined
    }, [
        chainId, currency, wrapped,
        metaPair, metaPairState, 
        usdcMetaPair, usdcMetaPairState, 
        usdcPair, usdcPairState
    ])
}
