import { Contract } from '@ethersproject/contracts'
import { 
    ChainId, WMETA,
    BANK_ADDRESS,
    FACTORY_ADDRESS,
    EXTRACTOR_ADDRESS,
    MINING_ADDRESS,
    ROUTER_ADDRESS,
    MSPACE_ADDRESS,
    TIMELOCK_ADDRESS
} from '../sdk'
import {
    BORING_HELPER_ADDRESS
} from 'kashi'
import { useMemo } from 'react'
import {
    ARGENT_WALLET_DETECTOR_ABI,
    ARGENT_WALLET_DETECTOR_MAINNET_ADDRESS
} from '../constants/abis/argent-wallet-detector'
import BORING_HELPER_ABI from '../constants/abis/boring-helper.json'
import ENS_PUBLIC_RESOLVER_ABI from '../constants/abis/ens-public-resolver.json'
import ENS_ABI from '../constants/abis/ens-registrar.json'
import { ERC20_BYTES32_ABI } from '../constants/abis/erc20'
import ERC20_ABI from '../constants/abis/erc20.json'
import WMETA_ABI from '../constants/abis/wmeta.json'
// import { MULTICALL_ABI, MULTICALL_NETWORKS } from '../constants/multicall'
import { MULTICALL2_ABI, MULTICALL2_NETWORKS } from '../constants/multicall2'
import BANK_ABI from '../constants/abis/bank.json'
import FACTORY_ABI from '../constants/abis/factory.json'
import PAIR_ABI from '../constants/abis/pair.json'
import EXTRACTOR_ABI from '../constants/abis/extractor.json'
import MINING_ABI from '../constants/abis/mining.json'
import ROUTER_ABI from '../constants/abis/router.json'
import MSPACE_ABI from '../constants/abis/mspace.json'
import TIMELOCK_ABI from '../constants/abis/timelock.json'
import { getContract } from '../utils'
import { useActiveWeb3React } from './useActiveWeb3React'

// returns null on errors
export function useContract(address: string | undefined, ABI: any, withSignerIfPossible = true): Contract | null {
    const { library, account } = useActiveWeb3React()

    return useMemo(() => {
        if (!address || !ABI || !library) return null
        try {
            return getContract(address, ABI, library, withSignerIfPossible && account ? account : undefined)
        } catch (error) {
            console.error('Failed to get contract', error)
            return null
        }
    }, [address, ABI, library, withSignerIfPossible, account])
}

export function useTokenContract(tokenAddress?: string, withSignerIfPossible?: boolean): Contract | null {
    return useContract(tokenAddress, ERC20_ABI, withSignerIfPossible)
}

export function useWMETAContract(withSignerIfPossible?: boolean): Contract | null {
    const { chainId } = useActiveWeb3React()
    return useContract(chainId ? WMETA[chainId].address : undefined, WMETA_ABI, withSignerIfPossible)
}

// TODO remove this?
export function useArgentWalletDetectorContract(): Contract | null {
    const { chainId } = useActiveWeb3React()
    return useContract(
        chainId === ChainId.METADIUM ? ARGENT_WALLET_DETECTOR_MAINNET_ADDRESS : undefined,
        ARGENT_WALLET_DETECTOR_ABI,
        false
    )
}

// TODO what for?
export function useENSRegistrarContract(withSignerIfPossible?: boolean): Contract | null {
    const { chainId } = useActiveWeb3React()
    let address: string | undefined
    if (chainId) {
        switch (chainId) {
            case ChainId.METADIUM:
                address = '0x8eAEde2c64AdDC6cDB3bFC4aF657E64C21B950bF'
                break
            case ChainId.META_TESTNET:
                address = '0x8eAEde2c64AdDC6cDB3bFC4aF657E64C21B950bF'
                break
        }
    }
    return useContract(address, ENS_ABI, withSignerIfPossible)
}

// TODO remove this
export function useENSResolverContract(address: string | undefined, withSignerIfPossible?: boolean): Contract | null {
    return useContract(address, ENS_PUBLIC_RESOLVER_ABI, withSignerIfPossible)
}

export function useBytes32TokenContract(tokenAddress?: string, withSignerIfPossible?: boolean): Contract | null {
    return useContract(tokenAddress, ERC20_BYTES32_ABI, withSignerIfPossible)
}

export function usePairContract(pairAddress?: string, withSignerIfPossible?: boolean): Contract | null {
    return useContract(pairAddress, PAIR_ABI, withSignerIfPossible)
}

export function useBoringHelperContract(): Contract | null {
    const { chainId } = useActiveWeb3React()
    return useContract(chainId && BORING_HELPER_ADDRESS[chainId], BORING_HELPER_ABI, false)
}

export function useMulticallContract(): Contract | null {
    const { chainId } = useActiveWeb3React()
    // return useContract(chainId && MULTICALL_NETWORKS[chainId], MULTICALL_ABI, false)
    return useContract(chainId && MULTICALL2_NETWORKS[chainId], MULTICALL2_ABI, false)
}

export function useMSpaceContract(withSignerIfPossible = true): Contract | null {
    const { chainId } = useActiveWeb3React()
    return useContract(chainId && MSPACE_ADDRESS[chainId], MSPACE_ABI, withSignerIfPossible)
}

export function useMiningContract(withSignerIfPossible?: boolean): Contract | null {
    const { chainId } = useActiveWeb3React()
    return useContract(chainId && MINING_ADDRESS[chainId], MINING_ABI, withSignerIfPossible)
}

export function useFactoryContract(): Contract | null {
    const { chainId } = useActiveWeb3React()
    return useContract(chainId && FACTORY_ADDRESS[chainId], FACTORY_ABI, false)
}

export function useFactoryContractByChainId(chainId: ChainId): Contract | null {
    return useContract(chainId && FACTORY_ADDRESS[chainId], FACTORY_ABI, false)
}

export function useRouterContract(): Contract | null {
    const { chainId } = useActiveWeb3React()
    return useContract(chainId && ROUTER_ADDRESS[chainId], ROUTER_ABI, false)
}

export function useBankContract(withSignerIfPossible?: boolean): Contract | null {
    const { chainId } = useActiveWeb3React()
    return useContract(chainId && BANK_ADDRESS[chainId], BANK_ABI, withSignerIfPossible)
}

export function useExtractorContract(): Contract | null {
    const { chainId } = useActiveWeb3React()
    return useContract(chainId && EXTRACTOR_ADDRESS[chainId], EXTRACTOR_ABI, false)
}

export function useTimelockContract(): Contract | null {
    const { chainId } = useActiveWeb3React()
    return useContract(chainId && TIMELOCK_ADDRESS[chainId], TIMELOCK_ABI, false)
}
