import { useExtractorContract } from './useContract'
import { useCallback } from 'react'
import { useTransactionAdder } from '../state/transactions/hooks'

const useExtractor = () => {
    const addTransaction = useTransactionAdder()
    const extractorContract = useExtractorContract()

    // Serve
    const serve = useCallback(
        async (token0: string, token1: string) => {
            try {
                const tx = await extractorContract?.methods.convert(token0, token1)
                return addTransaction(tx, { summary: 'Serve' })
            } catch (error) {
                return error
            }
        },
        [addTransaction, extractorContract]
    )

    // TODO: Serve all?

    return { serve }
}

export default useExtractor
