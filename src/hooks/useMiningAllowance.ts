import { BigNumber } from '@ethersproject/bignumber'
import { Contract } from 'ethers'
import { useCallback, useEffect, useState } from 'react'
import ERC20_ABI from '../constants/abis/erc20.json'
import { useActiveWeb3React } from './useActiveWeb3React'
import { useContract, useMiningContract } from './useContract'
import { isAddress } from '../utils'

const useAllowance = (lpAddress: string) => {
    const [allowance, setAllowance] = useState(BigNumber.from(0))
    const { account } = useActiveWeb3React()
    const miningContract = useMiningContract()
    const lpAddressChecksum = isAddress(lpAddress)
    const lpContract = useContract(lpAddressChecksum ? lpAddressChecksum : undefined, ERC20_ABI, false)

    const getAllowance = async (
        contract: Contract | null,
        owner: string | null | undefined,
        spender: string | undefined
    ): Promise<string> => {
        try {
            return await contract?.allowance(owner, spender)
        } catch (e) {
            return '0'
        }
    }

    const fetchAllowance = useCallback(async () => {
        const allowance = await getAllowance(lpContract, account, miningContract?.address)
        setAllowance(BigNumber.from(allowance))
    }, [account, lpContract, miningContract?.address])

    useEffect(() => {
        if (account && miningContract && lpContract) {
            fetchAllowance()
        }
        const refreshInterval = setInterval(fetchAllowance, 10000)
        return () => clearInterval(refreshInterval)
    }, [account, miningContract, lpContract, fetchAllowance])

    return allowance
}

export default useAllowance
