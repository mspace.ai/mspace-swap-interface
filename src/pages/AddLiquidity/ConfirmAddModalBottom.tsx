import { CurrencyAmount, Fraction, Percent, Currency } from '../../sdk'
import React, { useContext } from 'react'
import { Text } from 'rebass'
import { ButtonPrimary } from '../../components/ButtonLegacy'
import CurrencyLogo from '../../components/CurrencyLogo'
import { RowBetween, RowFixed } from '../../components/Row'
import { useActiveWeb3React } from '../../hooks/useActiveWeb3React'
import { Field } from '../../state/mint/actions'
import { TYPE } from '../../theme'
import { t } from '@lingui/macro'
import { useLingui } from '@lingui/react'
import { ThemeContext } from 'styled-components'

export function ConfirmAddModalBottom({
    noLiquidity,
    price,
    currencies,
    parsedAmounts,
    poolTokenPercentage,
    onAdd
}: {
    noLiquidity?: boolean
    price?: Fraction
    currencies: { [field in Field]?: Currency }
    parsedAmounts: { [field in Field]?: CurrencyAmount }
    poolTokenPercentage?: Percent
    onAdd: () => void
}) {
    const { i18n } = useLingui()
    const { chainId } = useActiveWeb3React()
    const theme = useContext(ThemeContext)

    return (
        <>
            <div style={{ padding: "0 20px" }}>
                <RowBetween>
                    <TYPE.body className="text-sm">{i18n._(t`${currencies[Field.CURRENCY_A]?.getSymbol(chainId)} Deposited`)}</TYPE.body>
                    <RowFixed>
                        <CurrencyLogo currency={currencies[Field.CURRENCY_A]} style={{ marginRight: '8px' }} />
                        <TYPE.body className="text-sm" color={theme.text3}>{parsedAmounts[Field.CURRENCY_A]?.toSignificant(6)}</TYPE.body>
                    </RowFixed>
                </RowBetween>
                <RowBetween style={{ marginTop: 12 }}>
                    <TYPE.body className="text-sm">{i18n._(t`${currencies[Field.CURRENCY_B]?.getSymbol(chainId)} Deposited`)}</TYPE.body>
                    <RowFixed>
                        <CurrencyLogo currency={currencies[Field.CURRENCY_B]} style={{ marginRight: '8px' }} />
                        <TYPE.body className="text-sm" color={theme.text3}>{parsedAmounts[Field.CURRENCY_B]?.toSignificant(6)}</TYPE.body>
                    </RowFixed>
                </RowBetween>
                <RowBetween className="items-start" style={{ marginTop: 17 }}>
                    <TYPE.body className="text-sm" color={theme.text3}>{i18n._(t`Rates`)}</TYPE.body>
                    <div className="flex flex-col items-end">
                        <TYPE.body className="text-sm" color={theme.text3}>
                            {`1 ${currencies[Field.CURRENCY_A]?.getSymbol(chainId)} = ${price?.toSignificant(4)} ${currencies[
                                Field.CURRENCY_B
                            ]?.getSymbol(chainId)}`}
                        </TYPE.body>
                        <TYPE.body className="text-sm" color={theme.text3}>
                            {`1 ${currencies[Field.CURRENCY_B]?.getSymbol(chainId)} = ${price
                                ?.invert()
                                .toSignificant(4)} ${currencies[Field.CURRENCY_A]?.getSymbol(chainId)}`}
                        </TYPE.body>
                    </div>
                </RowBetween>
                <RowBetween className="mt-6">
                    <TYPE.body className="text-sm">{i18n._(t`Share of Pool:`)}</TYPE.body>
                    <TYPE.body className="text-sm" color={theme.text3}>{noLiquidity ? '100' : poolTokenPercentage?.toSignificant(4)}%</TYPE.body>
                </RowBetween>
            </div>
            <ButtonPrimary style={{ margin: '24px 0 0 0' }} onClick={onAdd}>
                <Text fontWeight={900} fontSize={18}>
                    {noLiquidity ? i18n._(t`Create Pool & Supply`) : i18n._(t`Confirm Supply`)}
                </Text>
            </ButtonPrimary>
        </>
    )
}
