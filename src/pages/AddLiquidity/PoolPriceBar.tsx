import { Percent, Price, Currency } from '../../sdk'
import React, { useContext } from 'react'
import { Text } from 'rebass'
import { ThemeContext } from 'styled-components'
import { AutoColumn } from '../../components/Column'
import { AutoRow } from '../../components/Row'
import { ONE_BIPS } from '../../constants'
import { useActiveWeb3React } from '../../hooks/useActiveWeb3React'
import { Field } from '../../state/mint/actions'
import { TYPE } from '../../theme'
import { t } from '@lingui/macro'
import { useLingui } from '@lingui/react'

export function PoolPriceBar({
    currencies,
    noLiquidity,
    poolTokenPercentage,
    price
}: {
    currencies: { [field in Field]?: Currency }
    noLiquidity?: boolean
    poolTokenPercentage?: Percent
    price?: Price
}) {
    const { i18n } = useLingui()
    const { chainId } = useActiveWeb3React()
    const theme = useContext(ThemeContext)
    return (
        <>
            <div className="text-lg text-white font-bold">{i18n._(t`Prices and pool share`)}</div>
            <AutoColumn gap="sm" className="pb-3">
                <AutoRow justify="space-around" gap="4px">
                    <AutoColumn justify="center">
                        <TYPE.black className="font-black text-xl">{price?.toSignificant(6) ?? '-'}</TYPE.black>
                        <Text fontWeight={400} fontSize={14} color={theme.text3}>
                            {currencies[Field.CURRENCY_B]?.getSymbol(chainId)} / {currencies[
                                Field.CURRENCY_A
                            ]?.getSymbol(chainId)}
                        </Text>
                    </AutoColumn>
                    <AutoColumn justify="center">
                        <TYPE.black className="font-black text-xl">{price?.invert()?.toSignificant(6) ?? '-'}</TYPE.black>
                        <Text fontWeight={400} fontSize={14} color={theme.text3}>
                            {currencies[Field.CURRENCY_A]?.getSymbol(chainId)} / {currencies[
                                Field.CURRENCY_B
                            ]?.getSymbol(chainId)}
                        </Text>
                    </AutoColumn>
                    <AutoColumn justify="center">
                        <TYPE.black className="font-black text-xl">
                            {noLiquidity && price
                                ? '100'
                                : (poolTokenPercentage?.lessThan(ONE_BIPS) ? '<0.01' : poolTokenPercentage?.toFixed(2)) ??
                                '0'}
                            %
                        </TYPE.black>
                        <Text fontWeight={400} fontSize={14} color={theme.text3}>
                            {i18n._(t`Share of Pool`)}
                        </Text>
                    </AutoColumn>
                </AutoRow>
            </AutoColumn>
        </>
    )
}
