import React from 'react'
import { t } from '@lingui/macro'
import { useLingui } from '@lingui/react'
import Mining from '../../../assets/mspace/mining.svg'
import { convertToInternationalCurrencySystem } from 'utils'

export default function Total({ farms }: any) {
    const { i18n } = useLingui()
    const totalTvl = convertToInternationalCurrencySystem((farms || []).reduce((a: any, b: any) => a + b.tvl, 0))
    return (
        <div className="flex justify-between items-center bg-dark-800 mb-4 rounded-lg p-2 text-white">
            <div className="py-2 px-3">
                <p className="text-h5 mb-1">{i18n._(t`Mining`)}</p>
                <p>{i18n._(t`Total TVL`)}: ${totalTvl}</p>
            </div>
            <div>
                <img src={Mining} alt="Mining" className="max-h-20 md:max-h-28" />
            </div>
        </div>
    )
}
