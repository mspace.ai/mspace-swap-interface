import React from 'react'
import styled from 'styled-components'
import TokenLogo from './TokenLogo'

const TokenWrapper = styled.div<{ sizeraw: number; margin?: boolean }>`
    position: relative;
    display: flex;
    flex-direction: row;
    margin-right: ${({ sizeraw, margin }) => margin && (sizeraw / 3 + 8).toString() + 'px'};
`

const HigherLogo = styled(TokenLogo)<{ higherRadius?: string }>`
    z-index: 2;
    // background-color: white;
    border-radius: ${({ higherRadius }) => (higherRadius ? higherRadius : '50%')};
`

const CoveredLogo = styled(TokenLogo)`
    position: absolute;
    left: ${({ sizeraw }) => (sizeraw / 1.2).toString() + 'px'};
    // background-color: white;
    border-radius: 50%;
`

export default function DoubleTokenLogo({ s0, s1, size = 24, margin = false, higherRadius }: any) {
    return (
        <TokenWrapper sizeraw={size} margin={margin}>
            <HigherLogo symbol={s0} size={size.toString() + 'px'} sizeraw={size} higherRadius={higherRadius} />
            <CoveredLogo symbol={s1} size={size.toString() + 'px'} sizeraw={size} />
        </TokenWrapper>
    )
}
