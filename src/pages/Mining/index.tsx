import { useFuse, useSortableData } from 'hooks'
import React, { useState } from 'react'
import { ChevronDown, ChevronUp } from 'react-feather'
import styled from 'styled-components'
import useFarms from 'hooks/useFarms'
import usePendingMSpace from '../../hooks/usePendingMSpace'
import { RowBetween } from '../../components/Row'
import { formattedNum, formattedPercent } from '../../utils'
import InputGroup from './InputGroup'
import { Helmet } from 'react-helmet'
import { t } from '@lingui/macro'
import { useLingui } from '@lingui/react'
import { DoubleLogo, Paper, Search, Total } from './components'
import { ButtonPrimaryNormal } from 'components/ButtonLegacy'
import { useHistory } from 'react-router-dom'

export const FixedHeightRow = styled(RowBetween)`
    height: 24px;
`

export default function Mining(): JSX.Element {
    const { i18n } = useLingui()
    const query = useFarms()
    const farms = query?.farms
    const userFarms = query?.userFarms

    // Search Setup
    const options = { keys: ['symbol', 'name', 'pairAddress'], threshold: 0.4 }
    const { result: resultFarms, search: searchFarms } = useFuse({
        data: farms && farms.length > 0 ? farms : [],
        options
    })
    const { result: resultUserFarms, search: searchUserFarms } = useFuse({
        data: userFarms && userFarms.length > 0 ? userFarms : [],
        options
    })
    const [term, setTerm] = useState<string>('')
    const search = (t: string) => {
      setTerm(t)
      searchFarms(t)
      searchUserFarms(t)
    }
    const flattenFarmsSearchResults = resultFarms.map((a: { item: any }) => (a.item ? a.item : a))
    const flattenUserFarmsSearchResults = resultUserFarms.map((a: { item: any }) => (a.item ? a.item : a))
    // Sorting Setup
    const { items: farmItems, requestSort: requestFarmsSort, sortConfig: sortFarmsConfig } = useSortableData(flattenFarmsSearchResults)
    const { items: userFarmItems, requestSort: requestUserFarmsSort, sortConfig: sortUserFarmsConfig } = useSortableData(flattenUserFarmsSearchResults)

    return (
        <>
            <Helmet>
                <title>{i18n._(t`Mining`)}</title>
                <meta name="description" content="Farm MSP by staking LP (Liquidity Provider) tokens" />
            </Helmet>
            <div className="container max-w-2xl mx-auto">
                <Total farms={farms} />
                <div className="flex flex-col items-center bg-dark-900 rounded-lg">
                    <div className="flex w-full justify-between items-center flex-col md:flex-row bg-dark-800 rounded-t-lg px-5 py-4 mb-4">
                        <div className="mr-12 whitespace-nowrap text-base text-secondary hidden md:block">{i18n._(t`MSP Mining Farms`)}</div>
                        <Search search={search} term={term} />
                    </div>

                    {/* UserFarms */}
                    {userFarmItems && userFarmItems.length > 0 && (
                        <>
                            <div className="grid-cols-3 pb-4 px-8 text-gray-600 grid w-full text-sm">
                                <div 
                                    className="flex items-center cursor-pointer hover:text-white"
                                    onClick={() => requestUserFarmsSort('symbol')}
                                >
                                    <div>{i18n._(t`Your Farms`)}</div>
                                    {sortUserFarmsConfig &&
                                        sortUserFarmsConfig.key === 'symbol' &&
                                        ((sortUserFarmsConfig.direction === 'ascending' && <ChevronUp size={12} />) ||
                                            (sortUserFarmsConfig.direction === 'descending' && <ChevronDown size={12} />))}
                                </div>
                                <div className="hover:text-white cursor-pointer" onClick={() => requestUserFarmsSort('depositedUSD')}>
                                    <div className="flex items-center justify-end">
                                        <div>{i18n._(t`Deposited`)}</div>
                                        {sortUserFarmsConfig &&
                                            sortUserFarmsConfig.key === 'depositedUSD' &&
                                            ((sortUserFarmsConfig.direction === 'ascending' && <ChevronUp size={12} />) ||
                                                (sortUserFarmsConfig.direction === 'descending' && <ChevronDown size={12} />))}
                                    </div>
                                </div>
                                <div className="hover:text-white cursor-pointer" onClick={() => requestUserFarmsSort('pending')}>
                                    <div className="flex items-center justify-end">
                                        <div>{i18n._(t`Claim`)}</div>
                                        {sortUserFarmsConfig &&
                                            sortUserFarmsConfig.key === 'pending' &&
                                            ((sortUserFarmsConfig.direction === 'ascending' && <ChevronUp size={12} />) ||
                                                (sortUserFarmsConfig.direction === 'descending' && <ChevronDown size={12} />))}
                                    </div>
                                </div>
                            </div>
                            <div className="flex-col space-y-2 w-full px-6 pb-6 rounded-b-lg">
                                {userFarmItems.map((farm: any, i: number) => {
                                    return <UserBalance key={farm.pairAddress + '_' + i} farm={farm} />
                                })}
                            </div>
                        </>
                    )}
                    {/* All Farms */}
                    <div className="grid-cols-3 pb-4 px-8 text-gray-600 grid w-full text-sm">
                        <div
                            className="flex items-center cursor-pointer hover:text-white"
                            onClick={() => requestFarmsSort('symbol')}
                        >
                            <div className="whitespace-nowrap">{i18n._(t`Mining Farms`)}</div>
                            {sortFarmsConfig &&
                                sortFarmsConfig.key === 'symbol' &&
                                ((sortFarmsConfig.direction === 'ascending' && <ChevronUp size={12} />) ||
                                    (sortFarmsConfig.direction === 'descending' && <ChevronDown size={12} />))}
                        </div>
                        <div className="hover:text-white cursor-pointer" onClick={() => requestFarmsSort('tvl')}>
                            <div className="flex items-center justify-end">
                                <div>{i18n._(t`TVL`)}</div>
                                {sortFarmsConfig &&
                                    sortFarmsConfig.key === 'tvl' &&
                                    ((sortFarmsConfig.direction === 'ascending' && <ChevronUp size={12} />) ||
                                        (sortFarmsConfig.direction === 'descending' && <ChevronDown size={12} />))}
                            </div>
                        </div>
                        <div className="hover:text-white cursor-pointer" onClick={() => requestFarmsSort('roiPerYear')}>
                            <div className="flex items-center justify-end">
                                <div>{i18n._(t`APR`)}</div>
                                {sortFarmsConfig &&
                                    sortFarmsConfig.key === 'roiPerYear' &&
                                    ((sortFarmsConfig.direction === 'ascending' && <ChevronUp size={12} />) ||
                                        (sortFarmsConfig.direction === 'descending' && <ChevronDown size={12} />))}
                            </div>
                        </div>
                    </div>
                    <div className="flex-col space-y-2 w-full px-6 pb-6 rounded-b-lg">
                        {farmItems && farmItems.length > 0 ? (
                            farmItems
                                .filter((farm: any) => {
                                    return userFarmItems === undefined 
                                        || userFarmItems.length === 0
                                        || !userFarmItems.map((f: any) => f.pid).includes(farm.pid)
                                }).map((farm: any, i: number) => {
                                    return <TokenBalance key={farm.address + '_' + i} farm={farm} />
                                })
                        ) : (
                            <div className="w-full text-center py-6">{i18n._(t`No Results`)}</div>
                        )}
                    </div>
                </div>
            </div>
        </>
    )
}

const TokenBalance = ({ farm }: any) => {
    const [expand, setExpand] = useState<boolean>(false)
    
    return (
        <>
            {farm.type === 'MLP' && (
                <Paper>
                    <div 
                        className="grid grid-cols-3 py-4 px-4 select-none text-sm bg-dark-800 rounded-lg"
                        onClick={() => setExpand(!expand)}
                    >
                        <div className="flex items-center">
                            <div className="mr-4">
                                <DoubleLogo
                                    s0={farm.liquidityPair.token0.symbol}
                                    s1={farm.liquidityPair.token1.symbol}
                                    size={32}
                                    margin={true}
                                />
                            </div>
                            <div className="md:text-base text-gray-300">
                                {farm && farm.symbol}
                            </div>
                        </div>
                        <div className="flex items-center justify-end">
                            <div className="text-right md:text-base text-gray-300">
                                {formattedNum(farm.tvl, true)}<br />
                                <span className="text-gray-500">{formattedNum(farm.liquidityTokenBalance)} MP</span>
                            </div>
                        </div>
                        <div className="flex justify-end items-center">
                            <div className="text-lg text-white">
                                {farm.tvl !== 0 ? farm.roiPerYear > 10000 ? '10,000%+' : formattedPercent(farm.roiPerYear * 100) : 'Infinite'}
                            </div>
                        </div>
                    </div>
                    {expand && (
                        <InputGroup
                            pid={farm.pid}
                            pairAddress={farm.pairAddress}
                            pairSymbol={farm.symbol}
                            token0Address={farm.liquidityPair.token0.id}
                            token1Address={farm.liquidityPair.token1.id}
                            type={'MLP'}
                        />
                    )}
                </Paper>
            )}
        </>
    )
}

const UserBalance = ({ farm }: any) => {
    const [expand, setExpand] = useState<boolean>(false)

    return (
        <>
            {farm.type === 'MLP' && (
                <Paper>
                    <div className="grid grid-cols-3 py-4 px-4 select-none text-sm bg-dark-800 rounded-lg" onClick={() => setExpand(!expand)}>
                        <div className="flex items-center">
                            <div className="mr-4">
                                <DoubleLogo
                                    s0={farm.liquidityPair.token0.symbol}
                                    s1={farm.liquidityPair.token1.symbol}
                                    size={32}
                                    margin={true}
                                />
                            </div>
                            <div className="hidden sm:block md:text-base text-gray-300">
                                {farm && farm.symbol}
                            </div>
                        </div>
                        <div className="flex justify-end items-center">
                            <div className="text-right md:text-base text-gray-300">
                                {formattedNum(farm.depositedUSD, true)}<br />
                                <span className="text-gray-500">{formattedNum(farm.depositedLP, false)} MP</span>
                            </div>
                        </div>
                        <div className="flex justify-end items-center">
                            <div>
                                <div className="text-right">{formattedNum(farm.pending)} </div>
                                <div className="text-secondary text-right">MSP</div>
                            </div>
                        </div>
                    </div>
                    {expand && (
                        <InputGroup
                            pid={farm.pid}
                            pairAddress={farm.pairAddress}
                            pairSymbol={farm.symbol}
                            token0Address={farm.liquidityPair.token0.id}
                            token1Address={farm.liquidityPair.token1.id}
                            type={'MLP'}
                        />
                    )}
                </Paper>
            )}
        </>
    )
}
