import { JSBI, Pair, Token, ChainIdDefault } from '../../sdk'
import React, { useContext, useMemo, useEffect, useState, useCallback } from 'react'
import { Link, useHistory } from 'react-router-dom'
import styled, { ThemeContext } from 'styled-components'
import { ButtonPrimary } from '../../components/ButtonLegacy'
import Card from '../../components/Card'
import { SwapPoolTabs } from '../../components/NavigationTabs'
import FullPositionCard from '../../components/PositionCard'
import { Dots } from '../../components/swap/styleds'
import { ANALYTICS_URL, BIG_INT_ZERO } from '../../constants'
import { usePairs } from '../../data/Reserves'
import { useActiveWeb3React } from '../../hooks/useActiveWeb3React'
import { useStakingInfo } from '../../state/stake/hooks'
import { toV2LiquidityToken } from '../../state/user/hooks'
import { generateTrackedTokenPairs } from '../../utils/generateTrackedTokenPairs'
import { useTokenBalancesWithLoadingIndicator } from '../../state/wallet/hooks'
import { TYPE } from '../../theme'
import Alert from '../../components/Alert'
import { Helmet } from 'react-helmet'
import Button from '../../components/Button'
import { t, Trans } from '@lingui/macro'
import { useLingui } from '@lingui/react'
import { getPairAddresses } from 'apollo/getPairAddresses'
import { useAllTokens } from 'hooks/Tokens'
import { AppState } from '../../state'
import { useSelector } from 'react-redux'
import IconChart from 'assets/mspace/blue_chart.svg'

const EmptyProposals = styled.div`
    border: 1px solid ${({ theme }) => theme.text4};
    padding: 16px 12px;
    border-radius: ${({ theme }) => theme.borderRadius};
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
`

type TokenPairWithLiquidityToken = {
    tokens: [Token, Token],
    liquidityToken: Token | undefined
}

export default function Pool() {
    const { i18n } = useLingui()
    const theme = useContext(ThemeContext)
    const history = useHistory()
    const { account, chainId } = useActiveWeb3React()
    const [tokenPairsWithLiquidityTokens, setTokenPairsWithLiquidityTokens] = useState<TokenPairWithLiquidityToken[]>([]);

    const allTokens = useAllTokens()
    const savedSerializedPairs = useSelector<AppState, AppState['user']['pairs']>(({ user: { pairs } }) => pairs)

    // fetch the user's balances of all tracked V2 LP tokens
    const generatePairs = useCallback(async () => {
        const trackedTokenPairs = generateTrackedTokenPairs(chainId, allTokens, savedSerializedPairs)
        const results = await getPairAddresses(trackedTokenPairs.map(([token0, token1]) => [token0.address, token1.address]), chainId)
        const pairAddresses = results.map(result => result.pairAddress)
        // console.log("fetchPairAddress", pairAddresses);

        const data: TokenPairWithLiquidityToken[] = trackedTokenPairs.map(([tokenA, tokenB], i) => ({
            tokens: [tokenA, tokenB],
            liquidityToken: toV2LiquidityToken(
                [tokenA, tokenB],
                pairAddresses[i]
            )
        }))
        setTokenPairsWithLiquidityTokens(data)
    }, [account, chainId])
    useEffect(() => {
        generatePairs()
    }, [generatePairs])

    const liquidityTokens = useMemo(() => tokenPairsWithLiquidityTokens.map(tpwlt => tpwlt.liquidityToken), [
        tokenPairsWithLiquidityTokens
    ])
    const [v2PairsBalances, fetchingV2PairBalances] = useTokenBalancesWithLoadingIndicator(
        account ?? undefined,
        liquidityTokens
    )

    // fetch the reserves for all V2 pools in which the user has a balance
    const liquidityTokensWithBalances = useMemo(
        () =>
            tokenPairsWithLiquidityTokens.filter(({ liquidityToken }) =>
                liquidityToken !== undefined && v2PairsBalances[liquidityToken.address]?.greaterThan('0')
            ),
        [tokenPairsWithLiquidityTokens, v2PairsBalances]
    )

    const v2Pairs = usePairs(liquidityTokensWithBalances.map(({
        tokens: [tokenA, tokenB],
        liquidityToken
    }) => {
        return {
            tokens: [tokenA, tokenB],
            pairAddress: liquidityToken?.address
        }
    }))
    const v2IsLoading =
        fetchingV2PairBalances ||
        v2Pairs?.length < liquidityTokensWithBalances.length ||
        v2Pairs?.some(V2Pair => !V2Pair)

    const allV2PairsWithLiquidity = v2Pairs.map(([, pair]) => pair).filter((v2Pair): v2Pair is Pair => Boolean(v2Pair))

    // show liquidity even if its deposited in rewards contract
    // TODO query pair address
    const stakingInfo = useStakingInfo()
    const stakingInfosWithBalance = stakingInfo?.filter(pool => JSBI.greaterThan(pool.stakedAmount.raw, BIG_INT_ZERO))
    const stakingPairs = usePairs(stakingInfosWithBalance?.map(stakingInfo => {
        return {
            tokens: stakingInfo.tokens,
            pairAddress: stakingInfo.pairAddress
        }
    }))

    // remove any pairs that also are included in pairs with stake in mining pool
    const v2PairsWithoutStakedAmount = allV2PairsWithLiquidity.filter(v2Pair => {
        return (
            stakingPairs
                ?.map(stakingPair => stakingPair[1])
                .filter(stakingPair => stakingPair?.liquidityToken.address === v2Pair.liquidityToken.address).length ===
            0
        )
    })

    return (
        <>
            <Helmet>
                <title>{i18n._(t`Pool`)}</title>
            </Helmet>
            <div className="w-full">
                {/* <ExchangeHeader /> */}
                <div id="pool-page">
                    <SwapPoolTabs active={'pool'} />
                    <div className="relative flex flex-col items-center pb-6">
                        <div className="absolute" style={{ height: '100%', width: '100%', backgroundColor: '#000', opacity: 0.2, zIndex: 0 }}>
                        </div>
                        <div className="max-w-xl z-10">
                            <Alert className="pt-8 px-0"
                                title={i18n._(t`Liquidity Provider Rewards`)}
                                message={t`Liquidity providers earn a 0.25% fee on all trades proportional to their share of
                        the pool. Fees are added to the pool, accrue in real time and can be claimed by
                        withdrawing your liquidity`}
                                type="information"
                            />
                            <div className="grid grid-cols-1 sm:grid-cols-2 gap-2 mt-6 px-6 md:px-0">
                                <ButtonPrimary
                                    id="join-pool-button"
                                    style={{ height: 45, lineHeight: '45px', borderRadius: 5 }}
                                    onClick={() => history.push('/add/META')}>
                                    {i18n._(t`Add Liquidity`)}
                                </ButtonPrimary>
                                <Button
                                    id="create-pool-button"
                                    className="bg-black"
                                    onClick={() => history.push('/create/META')}
                                    size='small'
                                    style={{ height: 45, borderRadius: 5 }}
                                >
                                    {i18n._(t`Create a pair`)}
                                </Button>
                            </div>
                        </div>
                    </div>
                    <div className="flex flex-col items-center">
                        <div className="max-w-xl w-full px-6 md:px-0 mb-24 md:mb-0">
                            <div className="flex justify-center items-center my-8">
                                <div className="text-base text-white">
                                    {i18n._(t`My Liquidity Positions`)}
                                </div>
                                <div>
                                    <a className="flex items-center ml-4"
                                        style={{ textAlign: 'center', color: '#50caff' }}
                                        href={`${ANALYTICS_URL[chainId ?? ChainIdDefault]}/users` + (account ? `/${account}` : '')}
                                        rel="noreferrer"
                                        target="_blank"
                                    >
                                        {i18n._(t`Account analytics and accrued fees`)}
                                        <img src={IconChart} style={{ width: 30, height: 24, marginLeft: 8 }} />
                                    </a>
                                </div>
                            </div>
                            <div className="grid grid-flow-row gap-3">
                                {!account ? (
                                    <div style={{ padding: '40px 0' }}>
                                        <Card>
                                            <TYPE.body color={theme.text3} textAlign="center">
                                                {i18n._(t`Connect to a wallet to view your liquidity`)}
                                            </TYPE.body>
                                        </Card>
                                    </div>
                                ) : v2IsLoading ? (
                                    <EmptyProposals>
                                        <TYPE.body color={theme.text3} textAlign="center">
                                            <Dots>{i18n._(t`Loading`)}</Dots>
                                        </TYPE.body>
                                    </EmptyProposals>
                                ) : allV2PairsWithLiquidity?.length > 0 || stakingPairs?.length > 0 ? (
                                    <>
                                        {v2PairsWithoutStakedAmount.map(v2Pair => (
                                            <FullPositionCard key={v2Pair.liquidityToken.address} pair={v2Pair} />
                                        ))}
                                        {stakingPairs.map(
                                            (stakingPair, i) =>
                                                stakingPair[1] && ( // skip pairs that arent loaded
                                                    <FullPositionCard
                                                        key={stakingInfosWithBalance[i].stakingRewardAddress}
                                                        pair={stakingPair[1]}
                                                        stakedBalance={stakingInfosWithBalance[i].stakedAmount}
                                                    />
                                                )
                                        )}
                                    </>
                                ) : (
                                    <Card className="mb-6">
                                        <TYPE.body color={theme.text3} textAlign="center">
                                            {i18n._(t`No liquidity found`)}
                                        </TYPE.body>
                                    </Card>
                                )}

                                <div className='flex justify-center'>
                                    <div className="text-base font-normal text-white">
                                        <Trans>
                                            Don&apos;t see a pool you joined?{' '}
                                            <Link id="import-pool-link" to="/find" style={{ color: '#50caff' }}>
                                                Import it.
                                            </Link>
                                        </Trans>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}
