import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import { useFuse, useSortableData } from 'hooks'
import React, { useState, useCallback } from 'react'
import { ChevronDown, ChevronUp } from 'react-feather'
import styled from 'styled-components'
import useFarms from 'hooks/useFarms'
import { RowBetween } from '../../../components/Row'
import { formattedNum, formattedPercent } from '../../../utils'
import { Card, CardHeader, DoubleLogo, Paper, Search } from '../components'
import InputGroup from './InputGroup'
import { Helmet } from 'react-helmet'
import { t } from '@lingui/macro'
import { useLingui } from '@lingui/react'
import { ButtonPrimary, ButtonPrimaryNormal } from 'components/ButtonLegacy'
import Slider from "react-slick";
import Canvas from '../../../components/Canvas'
import { useHistory } from 'react-router-dom'
import LeftSlider from '../../../assets/mspace/left_slider.svg'
import RightSlider from '../../../assets/mspace/right_slider.svg'

export const FixedHeightRow = styled(RowBetween)`
    height: 24px;
`

function PrevArrow(props: any) {
    const { className, style, onClick } = props;
    return (
        <img src={LeftSlider} className={className} alt="success" style={{ ...style, width: 45, height: 45, objectFit: 'contain', left: -50 }} onClick={onClick} />
    );
}

function NextArrow(props: any) {
    const { className, style, onClick } = props;
    return (
        <img src={RightSlider} className={className} alt="success" style={{ ...style, width: 45, height: 45, objectFit: 'contain', right: -50 }} onClick={onClick} />
    );
}

export default function Yield(): JSX.Element {
    const { i18n } = useLingui()
    const query = useFarms()
    const farms = query?.farms
    const userFarms = query?.userFarms
    const history = useHistory()

    // Search Setup
    const options = { keys: ['symbol', 'name', 'pairAddress'], threshold: 0.4 }
    const { result, search, term } = useFuse({
        data: farms && farms.length > 0 ? farms : [],
        options
    })
    const flattenSearchResults = result.map((a: { item: any }) => (a.item ? a.item : a))
    // Sorting Setup
    const { items, requestSort, sortConfig } = useSortableData(flattenSearchResults)

    const settings = {
        speed: 500,
        slidesToShow: 3,
        slidesToScroll: 3,
        nextArrow: <NextArrow />,
        prevArrow: <PrevArrow />,
        dotsClass: "slick-dots slick-thumb",
        responsive: [
            {
                breakpoint: 1366,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 3,
                    infinite: true,
                    dots: true
                }
            },
            {
                breakpoint: 600,
                settings: {
                    dots: false,
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    arrows: false,
                    centerMode: true,
                    infinite: false
                }
            }
        ],
        customPaging: (pagi: any, i: any) => {
            return <div className="dot"></div>
        }
    }

    return (
        <>
            <Helmet>
                <title>{i18n._(t`Reward`)}</title>
                <meta name="description" content="Farm MSP by staking LP (Liquidity Provider) tokens" />
            </Helmet>
            <div style={{ position: "absolute", top: 76, left: 0, width: "100vw", height: 250 }} className="bg-black bg-opacity-20 md:block hidden" />
            <div className="container max-w-5xl mx-auto">
                <div className="flex flex-col">
                    <div className="flex flex-col bg-black bg-opacity-20 md:bg-transparent">
                        <div className="text-center md:my-7 my-4 md:text-xl text-base text-white">Total value Locked in <b>MetaSpace</b></div>
                        <div className="flex md:flex-row flex-col justify-between mb-3 md:mb-7">
                            <div className="flex flex-row md:text-xl text-base text-white font-bold mx-4">
                                <div style={{ color: '#0090cf' }} className="mr-5 flex-1 md:flex-0">Total Reward</div>
                                <div className="mr-1.5">$ 423,283</div>
                                <div className="text-sm leading-7">/day</div>
                            </div>
                            <div className="md:hidden mx-4 my-2" style={{ backgroundColor: '#4a4a4a', height: 1 }} />
                            <div className="flex flex-row md:text-xl text-base text-white font-bold mx-4">
                                <div style={{ color: '#0090cf' }} className="mr-5 flex-1 md:flex-0">Reward Rate</div>
                                <div className="mr-1.5">1,000 MSP</div>
                                <div className="text-sm leading-7">/day</div>
                            </div>
                            <div className="md:hidden mx-4 my-2" style={{ backgroundColor: '#4a4a4a', height: 1 }} />
                            <div className="flex flex-row md:text-xl text-base text-white font-bold mx-4">
                                <div style={{ color: '#0090cf' }} className="mr-5 flex-1 md:flex-0">Fees 24hr</div>
                                <div className="mr-1.5">$ 899,010</div>
                                <div className="text-sm leading-7">/day</div>
                            </div>
                        </div>

                        <div className="hidden md:flex" style={{ height: 1, opacity: 0.4, backgroundColor: '#fff' }}>
                        </div>
                    </div>

                    <div className="text-base md:text-lg font-bold text-white mt-6 md:mt-12 mx-4 md:mx-0"> {i18n._(t`My Reward Cards`)}</div>
                    <div className="mt-2.5 mb-4 md:mb-10">
                        <Slider {...settings}>
                            <div className="px-2">
                                <div className="relative">
                                    <div className="absolute w-full h-full">
                                        <Canvas case={2} />
                                    </div>
                                    <div className="p-10 z-10 flex flex-col">
                                        <div className="flex flex-row items-center z-10">
                                            <div style={{ width: 32, height: 32, borderRadius: 16, backgroundColor: "red" }} />
                                            <div style={{ width: 32, height: 32, borderRadius: 16, backgroundColor: "red", marginLeft: 2, marginRight: 10 }} />
                                            <div className="font-black text-base md:text-2xl text-white">DAI - ETH</div>
                                        </div>
                                        <div className="mt-5 md:mt-10" style={{ fontSize: 16, color: '#9e9e9e', zIndex: 10 }}>My rate</div>
                                        <div className="text-white text-base z-10">
                                            <span className="font-black text-base md:text-2xl">00000 MSP</span> /day
                                        </div>
                                        <div className="mt-2.5 md:mt-5" style={{ fontSize: 16, color: '#9e9e9e', zIndex: 10 }}>My rate</div>
                                        <div className="text-white text-base z-10" style={{ marginBottom: 30 }}>
                                            <span className="font-black text-base md:text-2xl">$ 710,152</span> /day
                                        </div>
                                        <ButtonPrimary height={45} color="default" onClick={() => history.push('/add/META')}>
                                            {i18n._(t`Manage`)}
                                        </ButtonPrimary>
                                    </div>
                                </div>
                            </div>
                            <div className="px-2">
                                <div className="relative">
                                    <div className="absolute w-full h-full">
                                        <Canvas case={2} />
                                    </div>
                                    <div className="p-10 z-10 flex flex-col">
                                        <div className="flex flex-row items-center z-10">
                                            <div style={{ width: 32, height: 32, borderRadius: 16, backgroundColor: "red" }} />
                                            <div style={{ width: 32, height: 32, borderRadius: 16, backgroundColor: "red", marginLeft: 2, marginRight: 10 }} />
                                            <div className="font-black text-base md:text-2xl text-white">DAI - ETH</div>
                                        </div>
                                        <div className="mt-5 md:mt-10" style={{ fontSize: 16, color: '#9e9e9e', zIndex: 10 }}>My rate</div>
                                        <div className="text-white text-base z-10">
                                            <span className="font-black text-base md:text-2xl">00000 MSP</span> /day
                                        </div>
                                        <div className="mt-2.5 md:mt-5" style={{ fontSize: 16, color: '#9e9e9e', zIndex: 10 }}>My rate</div>
                                        <div className="text-white text-base z-10" style={{ marginBottom: 30 }}>
                                            <span className="font-black text-base md:text-2xl">$ 710,152</span> /day
                                        </div>
                                        <ButtonPrimaryNormal height={45} color="default" onClick={() => history.push('/add/META')}>
                                            {i18n._(t`Add Liquidity`)}
                                        </ButtonPrimaryNormal>
                                    </div>
                                </div>
                            </div>
                            <div className="px-2">
                                <div className="relative">
                                    <div className="absolute w-full h-full">
                                        <Canvas case={2} />
                                    </div>
                                    <div className="p-10 z-10 flex flex-col">
                                        <div className="flex flex-row items-center z-10">
                                            <div style={{ width: 32, height: 32, borderRadius: 16, backgroundColor: "red" }} />
                                            <div style={{ width: 32, height: 32, borderRadius: 16, backgroundColor: "red", marginLeft: 2, marginRight: 10 }} />
                                            <div className="font-black text-base md:text-2xl text-white">DAI - ETH</div>
                                        </div>
                                        <div className="mt-5 md:mt-10" style={{ fontSize: 16, color: '#9e9e9e', zIndex: 10 }}>My rate</div>
                                        <div className="text-white text-base z-10">
                                            <span className="font-black text-base md:text-2xl">00000 MSP</span> /day
                                        </div>
                                        <div className="mt-2.5 md:mt-5" style={{ fontSize: 16, color: '#9e9e9e', zIndex: 10 }}>My rate</div>
                                        <div className="text-white text-base z-10" style={{ marginBottom: 30 }}>
                                            <span className="font-black text-base md:text-2xl">$ 710,152</span> /day
                                        </div>
                                        <ButtonPrimaryNormal height={45} color="default" onClick={() => history.push('/add/META')}>
                                            {i18n._(t`Deposit MLP`)}
                                        </ButtonPrimaryNormal>
                                    </div>
                                </div>
                            </div>
                            <div className="px-2">
                                <div className="relative">
                                    <div className="absolute w-full h-full">
                                        <Canvas case={2} />
                                    </div>
                                    <div className="p-10 z-10 flex flex-col">
                                        <div className="flex flex-row items-center z-10">
                                            <div style={{ width: 32, height: 32, borderRadius: 16, backgroundColor: "red" }} />
                                            <div style={{ width: 32, height: 32, borderRadius: 16, backgroundColor: "red", marginLeft: 2, marginRight: 10 }} />
                                            <div className="font-black text-base md:text-2xl text-white">DAI - ETH</div>
                                        </div>
                                        <div className="mt-5 md:mt-10" style={{ fontSize: 16, color: '#9e9e9e', zIndex: 10 }}>My rate</div>
                                        <div className="text-white text-base z-10">
                                            <span className="font-black text-base md:text-2xl">00000 MSP</span> /day
                                        </div>
                                        <div className="mt-2.5 md:mt-5" style={{ fontSize: 16, color: '#9e9e9e', zIndex: 10 }}>My rate</div>
                                        <div className="text-white text-base z-10" style={{ marginBottom: 30 }}>
                                            <span className="font-black text-base md:text-2xl">$ 710,152</span> /day
                                        </div>
                                        <ButtonPrimary height={45} color="default" onClick={() => history.push('/add/META')}>
                                            {i18n._(t`Manage`)}
                                        </ButtonPrimary>
                                    </div>
                                </div>
                            </div>
                            <div className="px-2">
                                <div className="relative">
                                    <div className="absolute w-full h-full">
                                        <Canvas case={2} />
                                    </div>
                                    <div className="p-10 z-10 flex flex-col">
                                        <div className="flex flex-row items-center z-10">
                                            <div style={{ width: 32, height: 32, borderRadius: 16, backgroundColor: "red" }} />
                                            <div style={{ width: 32, height: 32, borderRadius: 16, backgroundColor: "red", marginLeft: 2, marginRight: 10 }} />
                                            <div className="font-black text-base md:text-2xl text-white">DAI - ETH</div>
                                        </div>
                                        <div className="mt-5 md:mt-10" style={{ fontSize: 16, color: '#9e9e9e', zIndex: 10 }}>My rate</div>
                                        <div className="text-white text-base z-10">
                                            <span className="font-black text-base md:text-2xl">00000 MSP</span> /day
                                        </div>
                                        <div className="mt-2.5 md:mt-5" style={{ fontSize: 16, color: '#9e9e9e', zIndex: 10 }}>My rate</div>
                                        <div className="text-white text-base z-10" style={{ marginBottom: 30 }}>
                                            <span className="font-black text-base md:text-2xl">$ 710,152</span> /day
                                        </div>
                                        <ButtonPrimary height={45} color="default" onClick={() => history.push('/add/META')}>
                                            {i18n._(t`Manage`)}
                                        </ButtonPrimary>
                                    </div>
                                </div>
                            </div>
                        </Slider>
                    </div>
                </div>

                <div className="flex items-center mb-8 mx-4 md:mx-0">
                    <div className="flex w-full justify-between flex-col md:flex-row">
                        <div className="flex items-center" style={{ height: 48 }}>
                            <div className="mr-5 whitespace-nowrap text-base md:text-xl text-white font-bold">{i18n._(t`Participating Farms`)}</div>
                        </div>
                        <Search search={search} term={term} />
                    </div>
                </div>
                {/* UserFarms */}
                {userFarms && userFarms.length > 0 && (
                    <>
                        <div className="pb-4">
                            <div className="grid grid-cols-3 pb-4 px-4 text-sm  text-secondary">
                                <div className="flex items-center">
                                    <div>{i18n._(t`Your Yields`)}</div>
                                </div>
                                <div className="flex items-center justify-end">
                                    <div>{i18n._(t`Deposited`)}</div>
                                </div>
                                <div className="flex items-center justify-end">
                                    <div>{i18n._(t`Claim`)}</div>
                                </div>
                            </div>
                            <div className="flex-col space-y-2">
                                {userFarms.map((farm: any, i: number) => {
                                    return <UserBalance key={farm.address + '_' + i} farm={farm} />
                                })}
                            </div>
                        </div>
                    </>
                )}
                {/* All Farms */}
                <div className="grid-cols-5 pb-4 px-4 text-sm text-secondary hidden md:grid">
                    <div
                        className="flex items-center cursor-pointer hover:text-secondary"
                        onClick={() => requestSort('symbol')}
                    >
                        <div>{i18n._(t`Pool`)}</div>
                        {sortConfig &&
                            sortConfig.key === 'symbol' &&
                            ((sortConfig.direction === 'ascending' && <ChevronUp size={12} />) ||
                                (sortConfig.direction === 'descending' && <ChevronDown size={12} />))}
                    </div>
                    <div className="hover:text-secondary cursor-pointer" onClick={() => requestSort('tvl')}>
                        <div className="flex items-center justify-end">
                            <div>{i18n._(t`Total Liquidity`)}</div>
                            {sortConfig &&
                                sortConfig.key === 'tvl' &&
                                ((sortConfig.direction === 'ascending' && <ChevronUp size={12} />) ||
                                    (sortConfig.direction === 'descending' && <ChevronDown size={12} />))}
                        </div>
                    </div>
                    <div className="hover:text-secondary cursor-pointer" onClick={() => requestSort('roiPerYear')}>
                        <div className="flex items-center justify-end">
                            <div>{i18n._(t`Reward`)}</div>
                            {sortConfig &&
                                sortConfig.key === 'roiPerYear' &&
                                ((sortConfig.direction === 'ascending' && <ChevronUp size={12} />) ||
                                    (sortConfig.direction === 'descending' && <ChevronDown size={12} />))}
                        </div>
                    </div>
                    <div className="hover:text-secondary cursor-pointer" onClick={() => requestSort('tvl')}>
                        <div className="flex items-center justify-end">
                            <div>{i18n._(t`Pool Rate`)}</div>
                            {sortConfig &&
                                sortConfig.key === 'tvl' &&
                                ((sortConfig.direction === 'ascending' && <ChevronUp size={12} />) ||
                                    (sortConfig.direction === 'descending' && <ChevronDown size={12} />))}
                        </div>
                    </div>
                    <div className="hover:text-secondary cursor-pointer">
                    </div>
                </div>
                <div className="flex-col space-y-2 mb-8">
                    {items && items.length > 0 ? (
                        items.map((farm: any, i: number) => {
                            return <TokenBalance key={farm.address + '_' + i} farm={farm} />
                        })
                    ) : (
                        <div className="w-full text-center py-6">{i18n._(t`No Results`)}</div>
                    )}
                </div>
            </div>
        </>
    )
}

const TokenBalance = ({ farm }: any) => {
    const [expand, setExpand] = useState<boolean>(false)
    const { i18n } = useLingui()
    const history = useHistory()
    const handleDeposit = (currencyIdA: String, currencyIdB: String) => {
        history.push(`/liquidity-mining/${currencyIdA}/${currencyIdB}`)
    }

    return (
        <>
            {farm.type === 'MLP' && (
                <Paper>
                    <div
                        className="grid grid-cols-2 md:grid-cols-5 py-4 px-4 select-none text-sm bg-black"
                    // onClick={() => setExpand(!expand)}
                    >
                        <div className="flex items-center">
                            <div className="mr-4">
                                <DoubleLogo
                                    s0={farm.liquidityPair.token0.symbol}
                                    s1={farm.liquidityPair.token1.symbol}
                                    size={32}
                                    margin={true}
                                />
                            </div>
                            <div className="text-base md:text-lg text-white font-black">
                                {farm && farm.liquidityPair.token0.symbol + '-' + farm.liquidityPair.token1.symbol}
                            </div>
                        </div>
                        <div className="flex justify-end items-center hidden md:flex">
                            <div>
                                <div className="text-right text-lg text-white font-black">{formattedNum(farm.tvl, true)} </div>
                                {/* <div className="text-secondary text-right">
                                    {formattedNum(farm.lpBalance / 1e18, false)} MLP
                                </div> */}
                            </div>
                        </div>
                        <div className="flex justify-end items-center hidden md:flex">
                            <div className="text-right font-black text-lg text-white">
                                {farm.roiPerYear > 10000 ? '10000%+' : formattedPercent(farm.roiPerYear * 100)}
                            </div>
                        </div>
                        <div className="flex justify-end items-center hidden md:flex">
                            <div className="text-right font-black text-lg text-white">
                                {farm.roiPerYear > 10000 ? '10000%+' : formattedPercent(farm.roiPerYear * 100)}
                            </div>
                        </div>
                        <div className="flex justify-end md:justify-center items-center">
                            <ButtonPrimaryNormal height={30} style={{ width: 100 }} color="default" onClick={() => handleDeposit(farm.liquidityPair.token0.id, farm.liquidityPair.token1.id)}>
                                {i18n._(t`Deposit`)}
                            </ButtonPrimaryNormal>
                        </div>
                    </div>
                    {expand && (
                        <InputGroup
                            pid={farm.pid}
                            pairAddress={farm.pairAddress}
                            pairSymbol={farm.symbol}
                            token0Address={farm.liquidityPair.token0.id}
                            token1Address={farm.liquidityPair.token1.id}
                            type={'MLP'}
                        />
                    )}
                </Paper>
            )}
        </>
    )
}

const UserBalance = ({ farm }: any) => {
    const [expand, setExpand] = useState<boolean>(false)

    return (
        <>
            {farm.type === 'MLP' && (
                <Paper>
                    <div
                        className="grid grid-cols-3 py-4 px-4 cursor-pointer select-none rounded text-sm"
                        onClick={() => setExpand(!expand)}
                    >
                        <div className="flex items-center">
                            <div className="mr-4">
                                <DoubleLogo
                                    s0={farm.liquidityPair.token0.symbol}
                                    s1={farm.liquidityPair.token1.symbol}
                                    size={26}
                                    margin={true}
                                />
                            </div>
                            <div className="hidden sm:block">
                                {farm && farm.liquidityPair.token0.symbol + '-' + farm.liquidityPair.token1.symbol}
                            </div>
                        </div>
                        <div className="flex justify-center items-center">
                            <div>
                                <div className="text-right">{formattedNum(farm.depositedUSD, true)} </div>
                                <div className="text-secondary text-right">
                                    {formattedNum(farm.depositedLP, false)} MLP
                                </div>
                            </div>
                        </div>
                        <div className="flex justify-center items-center">
                            <div>
                                <div className="text-right">{formattedNum(farm.pendingMSpace)} </div>
                                <div className="text-secondary text-right">MSP</div>
                            </div>
                        </div>
                    </div>
                    {expand && (
                        <InputGroup
                            pid={farm.pid}
                            pairAddress={farm.pairAddress}
                            pairSymbol={farm.symbol}
                            token0Address={farm.liquidityPair.token0.id}
                            token1Address={farm.liquidityPair.token1.id}
                            type={'MLP'}
                        />
                    )}
                </Paper>
            )}
        </>
    )
}
