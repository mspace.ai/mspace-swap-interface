import React, { useCallback, useContext, useState } from 'react'
import { Trans, t } from '@lingui/macro'
import { Helmet } from 'react-helmet'
import { RouteComponentProps } from 'react-router-dom'
import { ThemeContext } from 'styled-components'
import { useActiveWeb3React } from '../../../hooks/useActiveWeb3React'
import { useCurrency } from '../../../hooks/Tokens'
import { useLingui } from '@lingui/react'
import { ButtonPrimary } from 'components/ButtonLegacy'

export default function LiquidityMining({
    match: {
        params: { currencyIdA, currencyIdB }
    },
    history
}: RouteComponentProps<{ currencyIdA?: string; currencyIdB?: string }>) {
    const { i18n } = useLingui()
    const { account, chainId, library } = useActiveWeb3React()
    const theme = useContext(ThemeContext)

    const currencyA = useCurrency(currencyIdA, chainId)
    const currencyB = useCurrency(currencyIdB, chainId)

    return (
        <>
            <Helmet>
                <title>{i18n._(t`Liquidity Mining`)}</title>
            </Helmet>
            <div className="w-full max-w-5xl z-10">
                <div className="flex flex-row mt-20">
                    <div className="flex flex-row items-center">
                        <div style={{ width: 32, height: 32, borderRadius: 16, backgroundColor: "red" }} />
                        <div style={{ width: 32, height: 32, borderRadius: 16, backgroundColor: "red", marginLeft: 2, marginRight: 10 }} />
                        <div className="font-black text-2xl text-white">DAI - ETH</div>
                        <div style={{ marginLeft: 20 }} className="text-2xl text-white">Liquidity Mining</div>
                        <div style={{ backgroundColor: 'rgba(0, 0, 0, 0.51)', padding: '6px 15px', marginLeft: 20 }} className="rounded-full font-sm">9999</div>
                    </div>
                </div>
                <div className="grid grid-cols-5 text-base text-secondary mt-6">
                    <div className="flex">
                        <div>{i18n._(t`Total Tokens Locked`)}</div>
                    </div>
                    <div className="flex justify-center">
                        <div>{i18n._(t`TVL`)}</div>
                    </div>
                    <div className="flex justify-center">
                        <div>{i18n._(t`Volume 24h`)}</div>
                    </div>
                    <div className="flex justify-center">
                        <div>{i18n._(t`Fee 24h`)}</div>
                    </div>
                    <div className="flex justify-center">
                        <div>{i18n._(t`Rewards + Fee`)}</div>
                    </div>
                </div>
                <div className="grid grid-cols-5 text-base text-secondary mt-2.5">
                    <div className="flex flex-row font-black text-2xl">
                        <div style={{ width: 60 }}>DAI</div>
                        <div>46.22m</div>
                    </div>
                    <div className="flex flex-row font-black text-2xl text-white justify-center">
                        <div>$ 152,013,067</div>
                    </div>
                    <div className="flex flex-row font-black text-2xl text-white justify-center">
                        <div>$ 13,61m</div>
                    </div>
                    <div className="flex flex-row font-black text-2xl text-white justify-center">
                        <div>$ 710152</div>
                    </div>
                    <div className="flex flex-row font-black text-2xl justify-center">
                        <div>50.08%</div>
                    </div>
                </div>
                <div className="grid grid-cols-5 text-base text-secondary mt-2.5">
                    <div className="flex flex-row font-black text-2xl">
                        <div style={{ width: 60 }}>ETH</div>
                        <div>68,97m</div>
                    </div>
                    <div className="flex flex-row font-bold text-2xl text-white justify-center items-center">
                        <div style={{
                            width: 0, height: 0, borderLeft: '10px solid transparent', borderRight: '10px solid transparent', borderBottom: '18px solid #c7de02', marginTop: 2
                        }} />
                        <div style={{ color: '#c7de02', marginLeft: 10 }}>3.80%</div>
                    </div>
                    <div className="flex flex-row font-bold text-2xl text-white justify-center items-center">
                        <div style={{
                            width: 0, height: 0, borderLeft: '10px solid transparent', borderRight: '10px solid transparent', borderBottom: '18px solid #ff3300', marginTop: 2
                        }} />
                        <div style={{ color: '#ff3300', marginLeft: 10 }}>29.0%</div>
                    </div>
                </div>

                <div style={{ marginTop: 60, padding: 30, marginBottom: 30 }} className="bg-opacity-30 bg-black text-center">
                    <div className="text-xl font-black" style={{ color: '#50caff' }}>First! Get TokenA-TokenB Liquidity tokens</div>
                    <div style={{ marginTop: 20 }} className="text-xl text-white font-medium">Once you’ve added liquidity to the Token A-Token B Pool<br />you can stake your liquidity tokens on this page.</div>
                </div>

                <div className="flex flex-row justify-center">
                    <ButtonPrimary height={45} color="default" onClick={() => history.push('/add/META')} style={{ maxWidth: 460, flex: 1 }}>
                        {i18n._(t`Add Token A-Token B liquidity`)}
                    </ButtonPrimary>
                </div>
            </div>
        </>
    )
}
