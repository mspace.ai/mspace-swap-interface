import { ApprovalState, useApproveCallbackFromTrade } from '../../hooks/useApproveCallback'
import { ArrowWrapper, BottomGrouping, SwapCallbackError, Wrapper } from '../../components/swap/styleds'
import { AutoRow, RowBetween } from '../../components/Row'
import { ButtonConfirmed, ButtonError, ButtonLight, ButtonPrimary } from '../../components/ButtonLegacy'
import Card, { DarkCard, GreyCard } from '../../components/CardLegacy'
import { CurrencyAmount, JSBI, Token, Trade, ChainId, PairInfo } from '../../sdk'
import Column, { AutoColumn } from '../../components/Column'
import { LinkStyledButton, TYPE } from '../../theme'
import React, { useCallback, useContext, useEffect, useMemo, useState } from 'react'
import { computeTradePriceBreakdown, warningSeverity } from '../../utils/prices'
import { useAllTokens, useCurrency } from '../../hooks/Tokens'
import {
    useDefaultsFromURLSearch,
    useDerivedSwapInfo,
    useSwapActionHandlers,
    useSwapState
} from '../../state/swap/hooks'
import { useExpertModeManager, useUserSingleHopOnly, useUserSlippageTolerance } from '../../state/user/hooks'
import { useNetworkModalToggle, useToggleSettingsMenu, useWalletModalToggle } from '../../state/application/hooks'
import useWrapCallback, { WrapType } from '../../hooks/useWrapCallback'

import AddressInputPanel from '../../components/AddressInputPanel'
import AdvancedSwapDetailsDropdown from '../../components/swap/AdvancedSwapDetailsDropdown'
import { ArrowDown } from 'react-feather'
import { ClickableText } from '../Pool/styleds'
import ConfirmSwapModal from '../../components/swap/ConfirmSwapModal'
import CurrencyInputPanel from '../../components/CurrencyInputPanel'
import { Field } from '../../state/swap/actions'
import { Helmet } from 'react-helmet'
import { INITIAL_ALLOWED_SLIPPAGE } from '../../constants'
import { Link } from 'react-router-dom'
import Loader from '../../components/Loader'
import Lottie from 'lottie-react'
import PolygonLogo from '../../assets/images/matic-logo.png'
import ProgressSteps from '../../components/ProgressSteps'
import ReactGA from 'react-ga'
import SwapHeader from '../../components/ExchangeHeader'
import { SwapPoolTabs } from '../../components/NavigationTabs'
import { Text } from 'rebass'
import { ThemeContext } from 'styled-components'
import TokenWarningModal from '../../components/TokenWarningModal'
import TradePrice from '../../components/swap/TradePrice'
import UnsupportedCurrencyFooter from 'components/swap/UnsupportedCurrencyFooter'
import confirmPriceImpactWithoutFee from '../../components/swap/confirmPriceImpactWithoutFee'
import { isTradeBetter } from 'utils/trades'
import { maxAmountSpend } from '../../utils/maxAmountSpend'
import { wrappedCurrency } from '../../utils/wrappedCurrency'
import { generateAllRoutePairs } from '../../hooks/Trades'
import swapArrowsAnimationData from '../../assets/animation/swap-arrows.json'
import { t, Trans } from '@lingui/macro'
import { useActiveWeb3React } from '../../hooks/useActiveWeb3React'
import useENSAddress from '../../hooks/useENSAddress'
import { useIsTransactionUnsupported } from 'hooks/Trades'
import { useLingui } from '@lingui/react'
import { useSwapCallback } from '../../hooks/useSwapCallback'
//import MisoBanner from '../../assets/images/miso-banner.jpg'
import SakeBanner from '../../assets/images/sake-banner.jpg'
import SakeLogo from '../../assets/images/sake-square.png'
import SakeBottle from '../../assets/images/sake-half.png'
//import MisoLogo from '../../assets/images/miso-logo.png'
import IconDown from '../../assets/mspace/icon_down.svg'
import Setting from '../../assets/mspace/icon_setting.svg'
import Canvas from '../../components/Canvas'
import { getPairAddresses } from 'apollo/getPairAddresses'

export default function Swap() {
    const { i18n } = useLingui()

    const { account, chainId } = useActiveWeb3React()

    const toggleNetworkModal = useNetworkModalToggle()

    const loadedUrlParams = useDefaultsFromURLSearch()

    // token warning stuff
    const [loadedInputCurrency, loadedOutputCurrency] = [
        useCurrency(loadedUrlParams?.inputCurrencyId, chainId),
        useCurrency(loadedUrlParams?.outputCurrencyId, chainId)
    ]
    const [dismissTokenWarning, setDismissTokenWarning] = useState<boolean>(false)
    const urlLoadedTokens: Token[] = useMemo(
        () => [loadedInputCurrency, loadedOutputCurrency]?.filter((c): c is Token => c instanceof Token) ?? [],
        [loadedInputCurrency, loadedOutputCurrency]
    )
    const handleConfirmTokenWarning = useCallback(() => {
        setDismissTokenWarning(true)
    }, [])

    // dismiss warning if all imported tokens are in active lists
    const defaultTokens = useAllTokens()
    const importTokensNotInDefault =
        urlLoadedTokens &&
        urlLoadedTokens.filter((token: Token) => {
            return !Boolean(token.address in defaultTokens)
        })

    const theme = useContext(ThemeContext)

    // toggle wallet when disconnected
    const toggleWalletModal = useWalletModalToggle()

    // for expert mode
    const toggleSettings = useToggleSettingsMenu()
    const [isExpertMode] = useExpertModeManager()

    // get custom setting values for user
    const [allowedSlippage] = useUserSlippageTolerance()

    // swap state
    const swapState = useSwapState()
    const { 
        independentField,
        typedValue,
        [Field.INPUT]: { currencyId: inputCurrencyId },
        [Field.OUTPUT]: { currencyId: outputCurrencyId },
        recipient
    } = swapState
    const inputCurrency = useCurrency(inputCurrencyId, chainId)
    const outputCurrency = useCurrency(outputCurrencyId, chainId)

    const [pairs, setPairs] = useState<PairInfo[]>([])

    const [tokenA, tokenB] = chainId !== undefined 
        && inputCurrency !== null 
        && outputCurrency !== null ? 
            [wrappedCurrency(inputCurrency, chainId), wrappedCurrency(outputCurrency, chainId)]
            : [undefined, undefined]

    const allPairCombinations: [Token, Token][] = useMemo(() => generateAllRoutePairs(tokenA, tokenB, chainId), [
        tokenA,
        tokenB,
        chainId
    ])
    
    const generatePairs = useCallback(async () => {
        const results = await getPairAddresses(allPairCombinations.map(([token0, token1]) => [token0.address, token1.address]), chainId)
        const validPairs: PairInfo[] = []
        results.map((result, i) => {
            if (result.pairAddress !== undefined) {
                validPairs.push({
                    tokens: allPairCombinations[i],
                    pairAddress: result.pairAddress
                })
            }
        })
        setPairs(validPairs)
    }, [
        allPairCombinations
    ]);
    useEffect(() => {
        generatePairs()
    }, [generatePairs])

    const { v2Trade, currencyBalances, parsedAmount, currencies, inputError: swapInputError } = useDerivedSwapInfo(swapState, pairs)
    const { wrapType, execute: onWrap, inputError: wrapInputError } = useWrapCallback(
        currencies[Field.INPUT],
        currencies[Field.OUTPUT],
        typedValue
    )
    const showWrap: boolean = wrapType !== WrapType.NOT_APPLICABLE
    const { address: recipientAddress } = useENSAddress(recipient)

    const trade = showWrap ? undefined : v2Trade

    const parsedAmounts = showWrap
        ? {
            [Field.INPUT]: parsedAmount,
            [Field.OUTPUT]: parsedAmount
        }
        : {
            [Field.INPUT]: independentField === Field.INPUT ? parsedAmount : trade?.inputAmount,
            [Field.OUTPUT]: independentField === Field.OUTPUT ? parsedAmount : trade?.outputAmount
        }

    const { onSwitchTokens, onCurrencySelection, onUserInput, onChangeRecipient } = useSwapActionHandlers()
    const isValid = !swapInputError
    const dependentField: Field = independentField === Field.INPUT ? Field.OUTPUT : Field.INPUT

    const handleTypeInput = useCallback(
        (value: string) => {
            onUserInput(Field.INPUT, value)
        },
        [onUserInput]
    )
    const handleTypeOutput = useCallback(
        (value: string) => {
            onUserInput(Field.OUTPUT, value)
        },
        [onUserInput]
    )

    // modal and loading
    const [{ showConfirm, tradeToConfirm, swapErrorMessage, attemptingTxn, txHash }, setSwapState] = useState<{
        showConfirm: boolean
        tradeToConfirm: Trade | undefined
        attemptingTxn: boolean
        swapErrorMessage: string | undefined
        txHash: string | undefined
    }>({
        showConfirm: false,
        tradeToConfirm: undefined,
        attemptingTxn: false,
        swapErrorMessage: undefined,
        txHash: undefined
    })

    const formattedAmounts = {
        [independentField]: typedValue,
        [dependentField]: showWrap
            ? parsedAmounts[independentField]?.toExact() ?? ''
            : parsedAmounts[dependentField]?.toSignificant(6) ?? ''
    }

    const route = trade?.route
    const userHasSpecifiedInputOutput = Boolean(
        currencies[Field.INPUT] &&
        currencies[Field.OUTPUT] &&
        parsedAmounts[independentField]?.greaterThan(JSBI.BigInt(0))
    )
    const noRoute = !route

    // check whether the user has approved the router on the input token
    const [approval, approveCallback] = useApproveCallbackFromTrade(trade, allowedSlippage)

    // check if user has gone through approval process, used to show two step buttons, reset on token change
    const [approvalSubmitted, setApprovalSubmitted] = useState<boolean>(false)

    // mark when a user has submitted an approval, reset onTokenSelection for input field
    useEffect(() => {
        if (approval === ApprovalState.PENDING) {
            setApprovalSubmitted(true)
        }
    }, [approval, approvalSubmitted])

    const maxAmountInput: CurrencyAmount | undefined = maxAmountSpend(currencyBalances[Field.INPUT])
    const atMaxAmountInput = Boolean(maxAmountInput && parsedAmounts[Field.INPUT]?.equalTo(maxAmountInput))

    // the callback to execute the swap
    const { callback: swapCallback, error: swapCallbackError } = useSwapCallback(trade, allowedSlippage, recipient)

    const { priceImpactWithoutFee } = computeTradePriceBreakdown(trade)

    const [singleHopOnly] = useUserSingleHopOnly()

    const handleSwap = useCallback(() => {
        if (priceImpactWithoutFee && !confirmPriceImpactWithoutFee(priceImpactWithoutFee)) {
            return
        }
        if (!swapCallback) {
            return
        }
        setSwapState({
            attemptingTxn: true,
            tradeToConfirm,
            showConfirm,
            swapErrorMessage: undefined,
            txHash: undefined
        })
        swapCallback()
            .then(hash => {
                setSwapState({
                    attemptingTxn: false,
                    tradeToConfirm,
                    showConfirm,
                    swapErrorMessage: undefined,
                    txHash: hash
                })

                // ReactGA.event({
                //     category: 'Swap',
                //     action:
                //         recipient === null
                //             ? 'Swap w/o Send'
                //             : (recipientAddress ?? recipient) === account
                //                 ? 'Swap w/o Send + recipient'
                //                 : 'Swap w/ Send',
                //     label: [
                //         trade?.inputAmount?.currency?.getSymbol(chainId),
                //         trade?.outputAmount?.currency?.getSymbol(chainId)
                //     ].join('/')
                // })

                // ReactGA.event({
                //     category: 'Routing',
                //     action: singleHopOnly ? 'Swap with multihop disabled' : 'Swap with multihop enabled'
                // })
            })
            .catch(error => {
                setSwapState({
                    attemptingTxn: false,
                    tradeToConfirm,
                    showConfirm,
                    swapErrorMessage: error.message,
                    txHash: undefined
                })
            })
    }, [
        priceImpactWithoutFee,
        swapCallback,
        tradeToConfirm,
        showConfirm,
        recipient,
        recipientAddress,
        account,
        trade,
        chainId,
        singleHopOnly
    ])

    // errors
    const [showInverted, setShowInverted] = useState<boolean>(false)

    // warnings on slippage
    const priceImpactSeverity = warningSeverity(priceImpactWithoutFee)

    // show approve flow when: no error on inputs, not approved or pending, or approved in current session
    // never show if price impact is above threshold in non expert mode
    const showApproveFlow =
        !swapInputError &&
        (approval === ApprovalState.NOT_APPROVED ||
            approval === ApprovalState.PENDING ||
            (approvalSubmitted && approval === ApprovalState.APPROVED)) &&
        !(priceImpactSeverity > 3 && !isExpertMode)

    const handleConfirmDismiss = useCallback(() => {
        setSwapState({ showConfirm: false, tradeToConfirm, attemptingTxn, swapErrorMessage, txHash })
        // if there was a tx hash, we want to clear the input
        if (txHash) {
            onUserInput(Field.INPUT, '')
        }
    }, [attemptingTxn, onUserInput, swapErrorMessage, tradeToConfirm, txHash])

    const handleAcceptChanges = useCallback(() => {
        setSwapState({ tradeToConfirm: trade, swapErrorMessage, txHash, attemptingTxn, showConfirm })
    }, [attemptingTxn, showConfirm, swapErrorMessage, trade, txHash])

    const handleInputSelect = useCallback(
        inputCurrency => {
            setApprovalSubmitted(false) // reset 2 step UI for approvals
            onCurrencySelection(Field.INPUT, inputCurrency)
        },
        [onCurrencySelection]
    )

    const handleMaxInput = useCallback(() => {
        maxAmountInput && onUserInput(Field.INPUT, maxAmountInput.toExact())
    }, [maxAmountInput, onUserInput])

    const handleOutputSelect = useCallback(outputCurrency => onCurrencySelection(Field.OUTPUT, outputCurrency), [
        onCurrencySelection
    ])

    const swapIsUnsupported = useIsTransactionUnsupported(currencies?.INPUT, currencies?.OUTPUT)

    const [animateSwapArrows, setAnimateSwapArrows] = useState<boolean>(false)

    return (
        <>
            <Helmet>
                <title>{i18n._(t`Swap`)}</title>
                <meta
                    name="description"
                    content="MSP allows for swapping of ERC20 compatible tokens across multiple networks"
                />
            </Helmet>
            <TokenWarningModal
                isOpen={importTokensNotInDefault.length > 0 && !dismissTokenWarning}
                tokens={importTokensNotInDefault}
                onConfirm={handleConfirmTokenWarning}
            />

            <SwapPoolTabs active={'swap'} />
            <div className="w-full max-w-md relative bg-black bg-opacity-60 rounded">
                <div className="w-full px-4 pt-5">
                    {/* <SwapHeader input={currencies[Field.INPUT]} output={currencies[Field.OUTPUT]} /> */}
                    <Wrapper id="swap-page">
                        <ConfirmSwapModal
                            isOpen={showConfirm}
                            trade={trade}
                            originalTrade={tradeToConfirm}
                            onAcceptChanges={handleAcceptChanges}
                            attemptingTxn={attemptingTxn}
                            txHash={txHash}
                            recipient={recipient}
                            allowedSlippage={allowedSlippage}
                            onConfirm={handleSwap}
                            swapErrorMessage={swapErrorMessage}
                            onDismiss={handleConfirmDismiss}
                        />
                        <AutoColumn gap={'md'}>
                            <CurrencyInputPanel
                                label={
                                    independentField === Field.OUTPUT && !showWrap && trade
                                        ? i18n._(t`From`)
                                        : i18n._(t`From`)
                                }
                                value={formattedAmounts[Field.INPUT]}
                                showMaxButton={!atMaxAmountInput}
                                currency={currencies[Field.INPUT]}
                                onUserInput={handleTypeInput}
                                onMax={handleMaxInput}
                                onCurrencySelect={handleInputSelect}
                                otherCurrency={currencies[Field.OUTPUT]}
                                id="swap-currency-input"
                            />
                            <AutoColumn justify="space-between">
                                <AutoRow
                                    justify={isExpertMode ? 'space-between' : 'center'}
                                    style={{ padding: '0 1rem' }}
                                >
                                    <button
                                        className="z-10"
                                        onClick={() => {
                                            setApprovalSubmitted(false) // reset 2 step UI for approvals
                                            onSwitchTokens()
                                        }}
                                    >
                                        <img src={IconDown} style={{ width: 26, height: 26 }} />
                                        {/* <div
                                        className="bg-dark-800 hover:bg-dark-700 rounded-full p-3"
                                        onMouseEnter={() => setAnimateSwapArrows(true)}
                                        onMouseLeave={() => setAnimateSwapArrows(false)}
                                    >
                                        <Lottie
                                            animationData={swapArrowsAnimationData}
                                            autoplay={animateSwapArrows}
                                            loop={false}
                                            style={{ width: 32, height: 32 }}

                                        // className="text-secondary fill-current"
                                        />
                                    </div> */}
                                    </button>
                                    {/* <ArrowWrapper clickable>
                                      <ArrowDown
                                        size="16"
                                        onClick={() => {
                                            setApprovalSubmitted(false) // reset 2 step UI for approvals
                                            onSwitchTokens()
                                        }}
                                        color={
                                            currencies[Field.INPUT] && currencies[Field.OUTPUT]
                                                ? theme.primary1
                                                : theme.text2
                                        }
                                    />
                                </ArrowWrapper> */}
                                    {recipient === null && !showWrap && isExpertMode ? (
                                        <LinkStyledButton id="add-recipient-button" onClick={() => onChangeRecipient('')}>
                                            + Add a send (optional)
                                        </LinkStyledButton>
                                    ) : null}
                                </AutoRow>
                            </AutoColumn>

                            <CurrencyInputPanel
                                value={formattedAmounts[Field.OUTPUT]}
                                onUserInput={handleTypeOutput}
                                label={
                                    independentField === Field.INPUT && !showWrap && trade
                                        ? i18n._(t`To Estimated`)
                                        : i18n._(t`To`)
                                }
                                showMaxButton={false}
                                currency={currencies[Field.OUTPUT]}
                                onCurrencySelect={handleOutputSelect}
                                otherCurrency={currencies[Field.INPUT]}
                                id="swap-currency-output"
                            />

                            {recipient !== null && !showWrap ? (
                                <>
                                    <AutoRow justify="space-between" style={{ padding: '0 1rem' }}>
                                        <ArrowWrapper clickable={false}>
                                            <ArrowDown size="16" color={theme.text2} />
                                        </ArrowWrapper>
                                        <LinkStyledButton
                                            id="remove-recipient-button"
                                            onClick={() => onChangeRecipient(null)}
                                        >
                                            - {i18n._(t`Remove send`)}
                                        </LinkStyledButton>
                                    </AutoRow>
                                    <AddressInputPanel id="recipient" value={recipient} onChange={onChangeRecipient} />
                                </>
                            ) : null}

                            {showWrap ? null : (
                                <Card padding={showWrap ? '.25rem 1rem 0 1rem' : '0px'} borderRadius={'20px'}>
                                    <AutoColumn gap="8px" style={{ padding: '0' }}>
                                        {Boolean(trade) && (
                                            <RowBetween align="center">
                                                <Text fontWeight={400} fontSize={14} color={theme.text2}>
                                                    {i18n._(t`Price`)}
                                                </Text>
                                                <TradePrice
                                                    price={trade?.executionPrice}
                                                    showInverted={showInverted}
                                                    setShowInverted={setShowInverted}
                                                />
                                            </RowBetween>
                                        )}
                                        {allowedSlippage !== INITIAL_ALLOWED_SLIPPAGE && (
                                            <RowBetween align="center">
                                                <ClickableText
                                                    fontWeight={500}
                                                    fontSize={14}
                                                    color={theme.text2}
                                                    onClick={toggleSettings}
                                                >
                                                    {i18n._(t`Slippage Tolerance`)}
                                                </ClickableText>
                                                <div className="flex flex-row justify-center items-center cursor-pointer" onClick={toggleSettings}>
                                                    <ClickableText
                                                        fontWeight={500}
                                                        fontSize={14}
                                                        color={theme.text2}
                                                    >
                                                        {allowedSlippage / 100}%
                                                    </ClickableText>
                                                    <img src={Setting} alt="setting" style={{ width: 17, height: 17, marginLeft: 8 }} />
                                                </div>
                                            </RowBetween>
                                        )}
                                    </AutoColumn>
                                </Card>
                            )}
                        </AutoColumn>
                        <BottomGrouping>
                            {swapIsUnsupported ? (
                                <ButtonPrimary disabled={true}>
                                    <TYPE.main mb="4px">{i18n._(t`Unsupported Asset`)}</TYPE.main>
                                </ButtonPrimary>
                            ) : !account ? (
                                <ButtonLight onClick={toggleWalletModal} className="font-black text-white"
                                    style={{
                                        backgroundImage: 'linear-gradient(165deg, #4ec8fb -54%, #500c5d 103%)',
                                        border: 'none', borderRadius: 0, fontSize: 18
                                    }}>{i18n._(t`Connect Wallet`)}</ButtonLight>
                            ) : showWrap ? (
                                <ButtonPrimary disabled={Boolean(wrapInputError)} onClick={onWrap}>
                                    {wrapInputError ??
                                        (wrapType === WrapType.WRAP
                                            ? i18n._(t`Wrap`)
                                            : wrapType === WrapType.UNWRAP
                                                ? i18n._(t`Unwrap`)
                                                : null)}
                                </ButtonPrimary>
                            ) : noRoute && userHasSpecifiedInputOutput ? (
                                <GreyCard style={{ textAlign: 'center' }}>
                                    <TYPE.main mb="4px">
                                        txt: {i18n._(t`Insufficient liquidity for this trade`)}
                                        noRoute: {String(noRoute)}
                                        userHasSpecifiedInputOutput: {String(userHasSpecifiedInputOutput)}
                                    </TYPE.main>
                                    {singleHopOnly && (
                                        <TYPE.main mb="4px">{i18n._(t`Try enabling multi-hop trades`)}</TYPE.main>
                                    )}
                                </GreyCard>
                            ) : showApproveFlow ? (
                                <RowBetween>
                                    <ButtonConfirmed
                                        onClick={approveCallback}
                                        disabled={approval !== ApprovalState.NOT_APPROVED || approvalSubmitted}
                                        width="48%"
                                        altDisabledStyle={approval === ApprovalState.PENDING} // show solid button while waiting
                                        confirmed={approval === ApprovalState.APPROVED}
                                    >
                                        {approval === ApprovalState.PENDING ? (
                                            <AutoRow gap="6px" justify="center">
                                                Approving <Loader stroke="white" />
                                            </AutoRow>
                                        ) : approvalSubmitted && approval === ApprovalState.APPROVED ? (
                                            i18n._(t`Approved`)
                                        ) : (
                                            // i18n._(t`Approve ${currencies[Field.INPUT]?.getSymbol(chainId)}`)
                                            i18n._(t`Approved`)
                                        )}
                                    </ButtonConfirmed>
                                    <ButtonError
                                        onClick={() => {
                                            if (isExpertMode) {
                                                handleSwap()
                                            } else {
                                                setSwapState({
                                                    tradeToConfirm: trade,
                                                    attemptingTxn: false,
                                                    swapErrorMessage: undefined,
                                                    showConfirm: true,
                                                    txHash: undefined
                                                })
                                            }
                                        }}
                                        width="48%"
                                        id="swap-button"
                                        disabled={
                                            !isValid ||
                                            approval !== ApprovalState.APPROVED ||
                                            (priceImpactSeverity > 3 && !isExpertMode)
                                        }
                                        error={isValid && priceImpactSeverity > 2}
                                    >
                                        <Text fontSize={16} fontWeight={900}>
                                            {priceImpactSeverity > 3 && !isExpertMode
                                                ? i18n._(t`Price Impact High`)
                                                : priceImpactSeverity > 2
                                                    ? i18n._(t`Swap Anyway`)
                                                    : i18n._(t`Swap`)}
                                        </Text>
                                    </ButtonError>
                                </RowBetween>
                            ) : (
                                <ButtonError
                                    onClick={() => {
                                        if (isExpertMode) {
                                            handleSwap()
                                        } else {
                                            setSwapState({
                                                tradeToConfirm: trade,
                                                attemptingTxn: false,
                                                swapErrorMessage: undefined,
                                                showConfirm: true,
                                                txHash: undefined
                                            })
                                        }
                                    }}
                                    id="swap-button"
                                    disabled={!isValid || (priceImpactSeverity > 3 && !isExpertMode) || !!swapCallbackError}
                                    error={isValid && priceImpactSeverity > 2 && !swapCallbackError}
                                >
                                    <Text fontWeight={500}>
                                        {swapInputError
                                            ? swapInputError
                                            : priceImpactSeverity > 3 && !isExpertMode
                                                ? i18n._(t`Price Impact Too High`)
                                                : priceImpactSeverity > 2
                                                    ? i18n._(t`Swap Anyway`)
                                                    : i18n._(t`Swap`)}
                                    </Text>
                                </ButtonError>
                            )}
                            {showApproveFlow && (
                                <Column style={{ marginTop: '1rem' }}>
                                    <ProgressSteps steps={[approval === ApprovalState.APPROVED]} />
                                </Column>
                            )}
                            {isExpertMode && swapErrorMessage ? <SwapCallbackError error={swapErrorMessage} /> : null}
                        </BottomGrouping>
                    </Wrapper>
                </div>
                <div className="px-4 pb-6">
                    {!swapIsUnsupported ? (
                        <AdvancedSwapDetailsDropdown trade={trade} />
                    ) : (
                        <UnsupportedCurrencyFooter
                            show={swapIsUnsupported}
                            currencies={[currencies.INPUT, currencies.OUTPUT]}
                        />
                    )}
                </div>
            </div>
        </>
    )
}
