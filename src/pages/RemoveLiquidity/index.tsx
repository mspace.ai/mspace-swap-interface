import { ApprovalState, useApproveCallback } from '../../hooks/useApproveCallback'
// import { ArrowDown, Plus } from 'react-feather'
import { ButtonConfirmed, ButtonError, ButtonLight, ButtonPrimary } from '../../components/ButtonLegacy'
import { META, Percent, currencyEquals, WMETA, Currency, ChainId } from '../../sdk'
import React, { useCallback, useContext, useMemo, useState, useEffect } from 'react'
import Row, { AutoRow, RowBetween, RowFixed } from '../../components/Row'
import { Trans, t } from '@lingui/macro'
import TransactionConfirmationModal, { ConfirmationModalContent } from '../../components/TransactionConfirmationModal'
import { calculateGasMargin, calculateSlippageAmount, getRouterAddress, getRouterContract } from '../../utils'
import { useBurnActionHandlers, useBurnState, useDerivedBurnInfo } from '../../state/burn/hooks'

// import AdvancedLiquidityDetailsDropdown from '../../components/Liquidity/AdvancedLiquidityDetailsDropdown'
// import { Alert } from '../../components'
import { AutoColumn } from '../../components/Column'
import { BigNumber } from '@ethersproject/bignumber'
import { Contract } from '@ethersproject/contracts'
import CurrencyLogo from '../../components/CurrencyLogo'
import { Dots } from '../../components/swap/styleds'
import DoubleCurrencyLogo from '../../components/DoubleLogo'
import { Field } from '../../state/burn/actions'
// import Header from '../../components/ExchangeHeader'
import { Helmet } from 'react-helmet'
// import LiquidityHeader from '../../components/Liquidity/LiquidityHeader'
// import LiquidityPrice from '../../components/Liquidity/LiquidityPrice'
import { MinimalPositionCard } from '../../components/PositionCard'
import { NavLink } from '../../components/Link'
import PercentInputPanel from '../../components/PercentInputPanel'
// import ReactGA from 'react-ga'
import RemoveLiquidityReceiveDetails from '../../components/Liquidity/RemoveLiquidityReceiveDetails'
import { RouteComponentProps } from 'react-router'
import { TYPE } from '../../theme'
import { Text } from 'rebass'
import { ThemeContext } from 'styled-components'
import { TransactionResponse } from '@ethersproject/providers'
import { Wrapper } from '../Pool/styleds'
import { currencyId } from '../../utils/currencyId'
import { useActiveWeb3React } from '../../hooks/useActiveWeb3React'
import { useCurrency } from '../../hooks/Tokens'
import useDebouncedChangeHandler from '../../utils/useDebouncedChangeHandler'
import useIsArgentWallet from '../../hooks/useIsArgentWallet'
import { useLingui } from '@lingui/react'
import { usePairContract } from '../../hooks/useContract'
import { useTransactionAdder } from '../../state/transactions/hooks'
import useTransactionDeadline from '../../hooks/useTransactionDeadline'
import { useUserSlippageTolerance } from '../../state/user/hooks'
import { useWalletModalToggle } from '../../state/application/hooks'
import { wrappedCurrency } from '../../utils/wrappedCurrency'
import IconBack from '../../assets/mspace/icon_back.svg'
import IconDown from '../../assets/mspace/icon_down.svg'
import { getPairAddresses } from 'apollo/getPairAddresses'

export default function RemoveLiquidity({
    history,
    match: {
        params: { currencyIdA, currencyIdB }
    }
}: RouteComponentProps<{ currencyIdA: string; currencyIdB: string }>) {
    const { i18n } = useLingui()
    const { account, chainId, library } = useActiveWeb3React()

    const [currencyA, currencyB] = [useCurrency(currencyIdA, chainId) ?? undefined, useCurrency(currencyIdB, chainId) ?? undefined]
    
    const [tokenA, tokenB] = useMemo(() => [wrappedCurrency(currencyA, chainId), wrappedCurrency(currencyB, chainId)], [
        currencyA,
        currencyB,
        chainId
    ])

    const theme = useContext(ThemeContext)

    // toggle wallet when disconnected
    const toggleWalletModal = useWalletModalToggle()

    // burn state
    // const { price } = useDerivedMintInfo(currencyA ?? undefined, currencyB ?? undefined)
    const { independentField, typedValue } = useBurnState()

    const [pairAddress, setPairAddress] = useState<string|undefined>(undefined)
    const fetchPairAddress = useCallback(async () => {
      const results = await getPairAddresses([[
          wrappedCurrency(currencyA ?? undefined, chainId)?.address, 
          wrappedCurrency(currencyB ?? undefined, chainId)?.address
      ]], chainId)
      setPairAddress(results[0].pairAddress)
    }, [chainId, currencyA, currencyB])
    useEffect(()=>{fetchPairAddress()}, [fetchPairAddress])

    const { pair, parsedAmounts, error } = useDerivedBurnInfo(currencyA ?? undefined, currencyB ?? undefined, pairAddress)
    const { onUserInput: _onUserInput } = useBurnActionHandlers()
    const isValid = !error

    // modal and loading
    const [showConfirm, setShowConfirm] = useState<boolean>(false)
    const [attemptingTxn, setAttemptingTxn] = useState(false) // clicked confirm

    // txn values
    const [txHash, setTxHash] = useState<string>('')
    const deadline = useTransactionDeadline()
    const [allowedSlippage] = useUserSlippageTolerance()

    const formattedAmounts = {
        [Field.LIQUIDITY_PERCENT]: parsedAmounts[Field.LIQUIDITY_PERCENT].equalTo('')
            ? ''
            : parsedAmounts[Field.LIQUIDITY_PERCENT].greaterThan('100')
                ? '100'
                : parsedAmounts[Field.LIQUIDITY_PERCENT].equalTo('0')
                    ? '0'
                    : parsedAmounts[Field.LIQUIDITY_PERCENT].lessThan(new Percent('1', '100'))
                        ? '<1'
                        : parsedAmounts[Field.LIQUIDITY_PERCENT].toFixed(0),
        [Field.LIQUIDITY]:
            independentField === Field.LIQUIDITY ? typedValue : parsedAmounts[Field.LIQUIDITY]?.toSignificant(6) ?? '',
        [Field.CURRENCY_A]:
            independentField === Field.CURRENCY_A
                ? typedValue
                : parsedAmounts[Field.CURRENCY_A]?.toSignificant(6) ?? '',
        [Field.CURRENCY_B]:
            independentField === Field.CURRENCY_B ? typedValue : parsedAmounts[Field.CURRENCY_B]?.toSignificant(6) ?? ''
    }

    const atMaxAmount = parsedAmounts[Field.LIQUIDITY_PERCENT]?.equalTo(new Percent('1'))

    // pair contract
    const pairContract: Contract | null = usePairContract(pair?.liquidityToken?.address)

    // allowance handling
    const [signatureData, setSignatureData] = useState<{ v: number; r: string; s: string; deadline: number } | null>(
        null
    )
    const [approval, approveCallback] = useApproveCallback(parsedAmounts[Field.LIQUIDITY], getRouterAddress(chainId), true)

    const isArgentWallet = useIsArgentWallet()

    async function onAttemptToApprove() {
        if (!pairContract || !pair || !library || !deadline) throw new Error('missing dependencies')
        const liquidityAmount = parsedAmounts[Field.LIQUIDITY]
        if (!liquidityAmount) throw new Error('missing liquidity amount')

        if (isArgentWallet) {
            return approveCallback()
        }

        return approveCallback()
        
    }

    // wrapped onUserInput to clear signatures
    const onUserInput = useCallback(
        (field: Field, typedValue: string) => {
            setSignatureData(null)
            return _onUserInput(field, typedValue)
        },
        [_onUserInput]
    )

    const onLiquidityPercentInput = useCallback(
        (typedValue: string): void => onUserInput(Field.LIQUIDITY_PERCENT, typedValue),
        [onUserInput]
    )
    const onLiquidityInput = useCallback((typedValue: string): void => onUserInput(Field.LIQUIDITY, typedValue), [
        onUserInput
    ])
    const onCurrencyAInput = useCallback((typedValue: string): void => onUserInput(Field.CURRENCY_A, typedValue), [
        onUserInput
    ])
    const onCurrencyBInput = useCallback((typedValue: string): void => onUserInput(Field.CURRENCY_B, typedValue), [
        onUserInput
    ])

    // tx sending
    const addTransaction = useTransactionAdder()
    async function onRemove() {
        if (!chainId || !library || !account || !deadline) throw new Error('missing dependencies')
        const { [Field.CURRENCY_A]: currencyAmountA, [Field.CURRENCY_B]: currencyAmountB } = parsedAmounts
        if (!currencyAmountA || !currencyAmountB) {
            throw new Error('missing currency amounts')
        }
        const router = getRouterContract(chainId, library, account)

        const amountsMin = {
            [Field.CURRENCY_A]: calculateSlippageAmount(currencyAmountA, allowedSlippage)[0],
            [Field.CURRENCY_B]: calculateSlippageAmount(currencyAmountB, allowedSlippage)[0]
        }

        if (!currencyA || !currencyB) throw new Error('missing tokens')
        const liquidityAmount = parsedAmounts[Field.LIQUIDITY]
        if (!liquidityAmount) throw new Error('missing liquidity amount')

        const currencyBIsMETA = currencyB === META
        const oneCurrencyIsMETA = currencyA === META || currencyBIsMETA

        if (!tokenA || !tokenB) throw new Error('could not wrap')

        let methodNames: string[], args: Array<string | string[] | number | boolean>
        // we have approval, use normal remove liquidity
        if (approval === ApprovalState.APPROVED) {
            // removeLiquidityMETA
            if (oneCurrencyIsMETA) {
                methodNames = ['removeLiquidityMETA', 'removeLiquidityMETASupportingFeeOnTransferTokens']
                args = [
                    currencyBIsMETA ? tokenA.address : tokenB.address,
                    liquidityAmount.raw.toString(),
                    amountsMin[currencyBIsMETA ? Field.CURRENCY_A : Field.CURRENCY_B].toString(),
                    amountsMin[currencyBIsMETA ? Field.CURRENCY_B : Field.CURRENCY_A].toString(),
                    account,
                    deadline.toHexString()
                ]
            }
            // removeLiquidity
            else {
                methodNames = ['removeLiquidity']
                args = [
                    tokenA.address,
                    tokenB.address,
                    liquidityAmount.raw.toString(),
                    amountsMin[Field.CURRENCY_A].toString(),
                    amountsMin[Field.CURRENCY_B].toString(),
                    account,
                    deadline.toHexString()
                ]
            }
        }
        // we have a signataure, use permit versions of remove liquidity
        else if (signatureData !== null) {
            // removeLiquidityMETAWithPermit
            if (oneCurrencyIsMETA) {
                methodNames = [
                    'removeLiquidityMETAWithPermit',
                    'removeLiquidityMETAWithPermitSupportingFeeOnTransferTokens'
                ]
                args = [
                    currencyBIsMETA ? tokenA.address : tokenB.address,
                    liquidityAmount.raw.toString(),
                    amountsMin[currencyBIsMETA ? Field.CURRENCY_A : Field.CURRENCY_B].toString(),
                    amountsMin[currencyBIsMETA ? Field.CURRENCY_B : Field.CURRENCY_A].toString(),
                    account,
                    signatureData.deadline,
                    false,
                    signatureData.v,
                    signatureData.r,
                    signatureData.s
                ]
            }
            // removeLiquidityMETAWithPermit
            else {
                methodNames = ['removeLiquidityWithPermit']
                args = [
                    tokenA.address,
                    tokenB.address,
                    liquidityAmount.raw.toString(),
                    amountsMin[Field.CURRENCY_A].toString(),
                    amountsMin[Field.CURRENCY_B].toString(),
                    account,
                    signatureData.deadline,
                    false,
                    signatureData.v,
                    signatureData.r,
                    signatureData.s
                ]
            }
        } else {
            throw new Error('Attempting to confirm without approval or a signature. Please contact support.')
        }

        const safeGasEstimates: (BigNumber | undefined)[] = await Promise.all(
            methodNames.map(methodName =>
                router.estimateGas[methodName](...args)
                    .then(calculateGasMargin)
                    .catch(error => {
                        console.error(`estimateGas failed`, methodName, args, error)
                        return undefined
                    })
            )
        )

        const indexOfSuccessfulEstimation = safeGasEstimates.findIndex(safeGasEstimate =>
            BigNumber.isBigNumber(safeGasEstimate)
        )

        // all estimations failed...
        if (indexOfSuccessfulEstimation === -1) {
            console.error('This transaction would fail. Please contact support.')
        } else {
            const methodName = methodNames[indexOfSuccessfulEstimation]
            const safeGasEstimate = safeGasEstimates[indexOfSuccessfulEstimation]

            setAttemptingTxn(true)
            await router[methodName](...args, {
                gasLimit: safeGasEstimate
            })
                .then((response: TransactionResponse) => {
                    setAttemptingTxn(false)

                    addTransaction(response, {
                        summary:
                            'Remove ' +
                            parsedAmounts[Field.CURRENCY_A]?.toSignificant(3) +
                            ' ' +
                            currencyA?.getSymbol(chainId) +
                            ' and ' +
                            parsedAmounts[Field.CURRENCY_B]?.toSignificant(3) +
                            ' ' +
                            currencyB?.getSymbol(chainId)
                    })

                    setTxHash(response.hash)

                    // ReactGA.event({
                    //     category: 'Liquidity',
                    //     action: 'Remove',
                    //     label: [currencyA?.getSymbol(chainId), currencyB?.getSymbol(chainId)].join('/')
                    // })
                })
                .catch((error: Error) => {
                    setAttemptingTxn(false)
                    // we only care if the error is something _other_ than the user rejected the tx
                    console.error(error)
                })
        }
    }

    function modalHeader() {
        return (
            <AutoColumn style={{ marginTop: '20px', backgroundColor: '#191919', padding: 20, marginBottom: 24 }}>
                <RowBetween align="center">
                    <Text fontSize={24} fontWeight={900} color={theme.white}>
                        {parsedAmounts[Field.CURRENCY_A]?.toSignificant(6)}
                    </Text>
                    <RowFixed>
                        <CurrencyLogo currency={currencyA} size={'30px'} />
                        <Text fontSize={16} fontWeight={700} style={{ marginLeft: '6px' }} color={theme.white}>
                            {currencyA?.getSymbol(chainId)}
                        </Text>
                    </RowFixed>
                </RowBetween>
                {/* <RowFixed>
                    <Plus size="16" color={theme.text2} />
                </RowFixed> */}
                <RowBetween align="center">
                    <Text fontSize={24} fontWeight={900} color={theme.white}>
                        {parsedAmounts[Field.CURRENCY_B]?.toSignificant(6)}
                    </Text>
                    <RowFixed>
                        <CurrencyLogo currency={currencyB} size={'30px'} />
                        <Text fontSize={16} fontWeight={700} style={{ marginLeft: '6px' }} color={theme.white}>
                            {currencyB?.getSymbol(chainId)}
                        </Text>
                    </RowFixed>
                </RowBetween>

                <TYPE.italic fontSize={12} color={theme.text3} className="text-gray-500 not-italic" textAlign="left" padding={'10px 0 0 0'}>
                    {t`Output is estimated. If the price changes by more than ${allowedSlippage /
                        100}% your transaction will revert.`}
                </TYPE.italic>
            </AutoColumn>
        )
    }

    function modalBottom() {
        return (
            <>
                <RowBetween>
                    <Text color={theme.white} fontWeight={400} fontSize={14}>
                        {i18n._(t`${currencyA?.getSymbol(chainId)}/${currencyB?.getSymbol(chainId)} LP Burned`)}
                    </Text>
                    <RowFixed>
                        <DoubleCurrencyLogo size={30} currency0={currencyA} currency1={currencyB} margin={false} />
                        <Text fontWeight={400} fontSize={14} style={{ marginLeft: 6 }} color={theme.text3}>
                            {parsedAmounts[Field.LIQUIDITY]?.toSignificant(6)}
                        </Text>
                    </RowFixed>
                </RowBetween>
                {pair && (
                    <>
                        <Text color={theme.white} fontWeight={700} fontSize={18} style={{ marginBottom: 4, marginTop: 60 }}>
                            {i18n._(t`Price`)}
                        </Text>
                        <Text fontWeight={500} fontSize={14} color={theme.white}>
                            1 {currencyA?.getSymbol(chainId)} ={' '}
                            <span className="font-bold">{tokenA ? pair.priceOf(tokenA).toSignificant(6) : '-'} {currencyB?.getSymbol(chainId)}</span>
                        </Text>
                        <div />
                        <Text fontWeight={500} fontSize={14} color={theme.white}>
                            1 {currencyB?.getSymbol(chainId)} ={' '}
                            <span className="font-bold">{tokenB ? pair.priceOf(tokenB).toSignificant(6) : '-'} {currencyA?.getSymbol(chainId)}</span>
                        </Text>
                    </>
                )}
                <ButtonPrimary
                    disabled={!(approval === ApprovalState.APPROVED || signatureData !== null)}
                    onClick={onRemove} style={{ marginTop: 24 }}
                >
                    <Text fontWeight={500} fontSize={18}>
                        {i18n._(t`Confirm`)}
                    </Text>
                </ButtonPrimary>
            </>
        )
    }

    const pendingText = t`Removing ${parsedAmounts[Field.CURRENCY_A]?.toSignificant(6)} ${currencyA?.getSymbol(
        chainId
    )} and ${parsedAmounts[Field.CURRENCY_B]?.toSignificant(6)} ${currencyB?.getSymbol(chainId)}`

    const liquidityPercentChangeCallback = useCallback(
        (value: number) => {
            onUserInput(Field.LIQUIDITY_PERCENT, value.toString())
        },
        [onUserInput]
    )

    const oneCurrencyIsMETA = currencyA === META || currencyB === META
    const oneCurrencyIsWMETA = Boolean(
        chainId &&
        ((currencyA && currencyEquals(WMETA[chainId], currencyA)) ||
            (currencyB && currencyEquals(WMETA[chainId], currencyB)))
    )

    // const handleSelectCurrencyA = useCallback(
    //     (currency: Currency) => {
    //         if (currencyIdB && currencyId(currency) === currencyIdB) {
    //             history.push(`/remove/${currencyId(currency)}/${currencyIdA}`)
    //         } else {
    //             history.push(`/remove/${currencyId(currency)}/${currencyIdB}`)
    //         }
    //     },
    //     [currencyIdA, currencyIdB, history]
    // )
    // const handleSelectCurrencyB = useCallback(
    //     (currency: Currency) => {
    //         if (currencyIdA && currencyId(currency) === currencyIdA) {
    //             history.push(`/remove/${currencyIdB}/${currencyId(currency)}`)
    //         } else {
    //             history.push(`/remove/${currencyIdA}/${currencyId(currency)}`)
    //         }
    //     },
    //     [currencyIdA, currencyIdB, history]
    // )

    const handleDismissConfirmation = useCallback(() => {
        setShowConfirm(false)
        setSignatureData(null) // important that we clear signature data to avoid bad sigs
        // if there was a tx hash, we want to clear the input
        if (txHash) {
            onUserInput(Field.LIQUIDITY_PERCENT, '0')
        }
        setTxHash('')
    }, [onUserInput, txHash])

    const [innerLiquidityPercentage, setInnerLiquidityPercentage] = useDebouncedChangeHandler(
        Number.parseInt(parsedAmounts[Field.LIQUIDITY_PERCENT].toFixed(0)),
        liquidityPercentChangeCallback
    )

    return (
        <>
            <Helmet>
                <title>{i18n._(t`Remove Liquidity`)}</title>
            </Helmet>

            {/* <div className="w-full max-w-2xl mb-5 px-4"> */}
                {/* <NavLink
                    className="text-center text-secondary hover:text-high-emphesis text-base font-medium"
                    to={'/pool'}
                >
                    {i18n._(t`View Your Liquidity Positions >`)}
                </NavLink> */}
                {/* <button
                    style={{
                        backgroundColor: 'rgba(167, 85, 221, 0.25)',
                        border: '1px solid #A755DD',
                        borderRadius: 20,
                        padding: '5px 40px'
                        fontSize: 14,
                    }}
                >
                    FARM THE {currencies[Field.CURRENCY_A]?.getSymbol(chainId)}-
                    {currencies[Field.CURRENCY_B]?.getSymbol(chainId)} POOL
                </button> */}
            {/* </div> */}
            <div className="w-full max-w-md relative bg-black bg-opacity-60 rounded">
                {/* <Header input={currencyA} output={currencyB} /> */}
                <div className="w-full px-4 pt-5 pb-7">
                    <Wrapper>
                        <TransactionConfirmationModal
                            isOpen={showConfirm}
                            onDismiss={handleDismissConfirmation}
                            attemptingTxn={attemptingTxn}
                            hash={txHash ? txHash : ''}
                            content={() => (
                                <ConfirmationModalContent
                                    title={i18n._(t`You Will Receive`)}
                                    onDismiss={handleDismissConfirmation}
                                    topContent={modalHeader}
                                    bottomContent={modalBottom}
                                />
                            )}
                            pendingText={pendingText}
                        />
                        <AutoColumn gap="md">
                            <RowBetween className="mb-3">
                                <NavLink to={'/pool'} className="p-0">
                                    <img src={IconBack} style={{ width: 32, height: 32 }} />
                                </NavLink>
                                <div className="text-center text-white text-xl font-bold">
                                    {i18n._(t`Remove Liquidity`)}
                                </div>
                                <div style={{ width: 32, height: 32 }} className="opacity-0" />
                            </RowBetween>
                            {/* <Alert
                            showIcon={false}
                            message={
                                <>
                                    <Trans>
                                        <b>Tip:</b> Removing pool tokens converts your position back into underlying
                                        tokens at the current rate, proportional to your share of the pool. Accrued fees
                                        are included in the amounts you receive.
                                    </Trans>
                                </>
                            }
                            type="information"
                        /> */}
                            {/* <LiquidityHeader input={currencyA} output={currencyB} /> */}
                            <PercentInputPanel
                                value={formattedAmounts[Field.LIQUIDITY_PERCENT]}
                                onUserInput={onLiquidityPercentInput}
                                id="liquidity-percent"
                            />
                            <div className="flex items-center justify-center pt-4 pb-2">
                                <button>
                                    <img src={IconDown} style={{ width: 26, height: 26 }} />
                                </button>
                            </div>
                            <RemoveLiquidityReceiveDetails
                                currencyA={currencyA}
                                amountA={formattedAmounts[Field.CURRENCY_A] || '-'}
                                currencyB={currencyB}
                                amountB={formattedAmounts[Field.CURRENCY_B] || '-'}
                                hasWMETA={oneCurrencyIsWMETA}
                                hasMETA={oneCurrencyIsMETA}
                                id="liquidit-receive"
                            />
                            {currencyA && currencyB && pair && (
                                // <LiquidityPrice input={currencyA} output={currencyB} price={price} />
                                <div>
                                    <Text color={theme.white} fontWeight={700} fontSize={18} style={{ marginBottom: 4, marginTop: 30 }}>
                                        {i18n._(t`Price`)}
                                    </Text>
                                    <Text fontWeight={500} fontSize={14} color={theme.white}>
                                        1 {currencyA?.getSymbol(chainId)} ={' '}
                                        <span className="font-bold">{tokenA ? pair.priceOf(tokenA).toSignificant(6) : '-'} {currencyB?.getSymbol(chainId)}</span>
                                    </Text>
                                    <div />
                                    <Text fontWeight={500} fontSize={14} color={theme.white}>
                                        1 {currencyB?.getSymbol(chainId)} ={' '}
                                        <span className="font-bold">{tokenB ? pair.priceOf(tokenB).toSignificant(6) : '-'} {currencyA?.getSymbol(chainId)}</span>
                                    </Text>
                                </div>
                            )}
                            <div style={{ position: 'relative' }}>
                                {!account ? (
                                    <ButtonLight onClick={toggleWalletModal}>{i18n._(t`Connect Wallet`)}</ButtonLight>
                                ) : (
                                    <div className="flex flex-col space-y-3 md:space-y-0">
                                        <ButtonConfirmed
                                            onClick={onAttemptToApprove}
                                            confirmed={approval === ApprovalState.APPROVED || signatureData !== null}
                                            disabled={approval !== ApprovalState.NOT_APPROVED || signatureData !== null}
                                            mb="0.5rem"
                                            fontWeight={900}
                                            fontSize={16}
                                            height={45}
                                        >
                                            {approval === ApprovalState.PENDING ? (
                                                <Dots>{i18n._(t`Approving`)}</Dots>
                                            ) : approval === ApprovalState.APPROVED || signatureData !== null ? (
                                                i18n._(t`Approved`)
                                            ) : (
                                                i18n._(t`Approve`)
                                            )}
                                        </ButtonConfirmed>
                                        <ButtonError
                                            onClick={() => {
                                                setShowConfirm(true)
                                            }}
                                            disabled={
                                                !isValid || (signatureData === null && approval !== ApprovalState.APPROVED)
                                            }
                                            error={
                                                !isValid &&
                                                !!parsedAmounts[Field.CURRENCY_A] &&
                                                !!parsedAmounts[Field.CURRENCY_B]
                                            }
                                            height={45}
                                        >
                                            <Text fontSize={16} fontWeight={900}>
                                                {error || i18n._(t`Confirm Withdrawal`)}
                                            </Text>
                                        </ButtonError>
                                    </div>
                                )}
                            </div>
                        </AutoColumn>
                    </Wrapper>
                </div>
            </div>

            {/* <div className="w-full max-w-2xl flex flex-col mt-4">
                <AdvancedLiquidityDetailsDropdown show={Boolean(typedValue && parseInt(typedValue))} />
            </div> */}
            {pair ? (
                <div className="w-full max-w-md flex flex-col px-8">
                    <MinimalPositionCard showUnwrapped={oneCurrencyIsWMETA} pair={pair} />
                </div>
            ) : null}
        </>
    )
}
