import React from 'react'
import { useActiveWeb3React } from '../../hooks/useActiveWeb3React'
import { Helmet } from 'react-helmet'
import InfoCard from './InfoCard'
import APRCard from './APRCard'
import StakeCard from './StakeCard'
import BalanceCard from './BalanceCard'
import { ChainId, ChainIdDefault } from '../../sdk'
import { MSP, XMSP } from '../../constants'
import useTokenBalance from '../../hooks/useTokenBalance'

const mockData = {
    mspaceEarnings: 345.27898,
    weightedApr: 15.34
}

export default function Bank() {
    const { account, chainId } = useActiveWeb3React()

    const mspaceBalance = useTokenBalance(MSP[chainId ?? ChainIdDefault]?.address ?? '')
    const xmspaceBalance = useTokenBalance(XMSP[chainId ?? ChainIdDefault]?.address ?? '')

    return (
        <>
            <Helmet>
                <title>xMSP | MSP</title>
            </Helmet>
            <div className="flex flex-col max-w-2xl items-center min-h-fitContent">
                <InfoCard />
                <div className="flex flex-col w-full mt-5">
                    <div className="mb-4">
                        <APRCard />
                    </div>
                    <div>
                        <StakeCard mspaceBalance={mspaceBalance} xmspaceBalance={xmspaceBalance} />
                    </div>
                    <div className="hidden md:block">
                        <BalanceCard
                            mspaceEarnings={mockData.mspaceEarnings}
                            xmspaceBalance={xmspaceBalance}
                            mspaceBalance={mspaceBalance}
                            weightedApr={mockData.weightedApr}
                            chainId={chainId ?? ChainIdDefault}
                        />
                    </div>
                </div>
                <div className="flex justify-center w-full">
                    <div className="md:hidden flex justify-center w-full">
                        <BalanceCard
                            mspaceEarnings={mockData.mspaceEarnings}
                            xmspaceBalance={xmspaceBalance}
                            mspaceBalance={mspaceBalance}
                            weightedApr={mockData.weightedApr}
                            chainId={chainId ?? ChainIdDefault}
                        />
                    </div>
                </div>
            </div>
        </>
    )
}
