import React, { useEffect, useState } from 'react'
import { bank, exchange, mspace } from '../../subgraph'
import { t } from '@lingui/macro'
import { useLingui } from '@lingui/react'
import { useActiveWeb3React } from '../../hooks/useActiveWeb3React'
import { ANALYTICS_URL } from '../../constants'
import { ChainIdDefault } from '../../sdk'

export default function APRCard() {
    const { i18n } = useLingui()
    const [Apr, setApr] = useState<any>()
    const { account, chainId } = useActiveWeb3React()
    useEffect(() => {
        const fetchData = async () => {
            const results = await Promise.all([
                bank.info({chainId: chainId}),
                exchange.dayData({chainId: chainId}),
                mspace.priceUSD({chainId: chainId})
            ])
            const APR =
                (((results[1][1].volumeUSD * 0.05) / results[0].totalSupply) * 365) / (results[0].ratio * results[2])

            setApr(APR)
        }
        fetchData()
    }, [chainId])
    return (
        <div className="flex w-full justify-between items-center h-24 p-4 md:pl-5 md:pr-7 rounded bg-light-blue text-white">
            <div className="flex flex-col">
                <div className="flex flex-nowrap justify-center items-center mb-4 md:mb-2">
                    <p className="whitespace-nowrap text-caption2 md:text-lg md:leading-5 font-bold text-high-emphesis">
                        {i18n._(t`Staking APR`)}{' '}
                    </p>
                </div>
                <div className="flex">
                    <a
                    // TODO change this?
                        href={`${ANALYTICS_URL[chainId ?? ChainIdDefault]}/bank`}
                        target="_blank"
                        rel="noreferrer noopener"
                        className={`
                        py-1 px-4 md:py-1.5 md:px-7 rounded
                        text-xs md:text-sm font-medium text-dark-900
                        bg-light-yellow hover:bg-opacity-90`}
                    >
                        {i18n._(t`View Stats`)}
                    </a>
                </div>
            </div>
            <div className="flex flex-col">
                <p className="text-right font-bold text-lg md:text-h4 mb-1">
                    {`${Apr ? Apr.toFixed(2) + '%' : i18n._(t`Loading...`)}`}
                </p>
                <p className="text-right w-32 md:w-64 md:text-base">
                    {i18n._(t`Yesterday's APR`)}
                </p>
            </div>
        </div>
    )
}
