import React from 'react'
import { t } from '@lingui/macro'
import Bank from '../../assets/mspace/bank.svg'
import { useLingui } from '@lingui/react'

export default function InfoCard() {
    const { i18n } = useLingui()
    return (
        <div className="flex w-full">
            <div className="flex flex-col w-full mt-auto">
                <div className="flex">
                    <div className="text-body font-bold md:text-h5 text-high-emphesis self-end mb-3 md:mb-7">
                        {i18n._(t`Deposit MSP at Bank`)}
                    </div>
                </div>
                <div className="text-gray-300 text-sm leading-5 md:text-caption pr-3 md:pr-0">
                    {t`Bank earns 0.05% fee for every swap on Mspace protocol. The buyback of MSP takes every 24 hours. xMSP as the staked MSP compounds continuously. When xMSP is staked, all the originally deposited MSP and any additional from fees are returned to the owner.`}
                </div>
            </div>
            <div className="hidden md:flex justify-center items-start ml-6">
                <img src={Bank} alt={'xMSP sign'} />
            </div>
        </div>
    )
}
