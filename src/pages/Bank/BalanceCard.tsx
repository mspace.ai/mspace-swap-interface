import { BalanceProps } from '../../hooks/useTokenBalance'
import React from 'react'
import MSP from '../../assets/mspace/MSP.svg'
import xMSP from '../../assets/mspace/xMSP.svg'
import { fixedFromBalance } from '../../utils'
import { t } from '@lingui/macro'
import { useActiveWeb3React } from '../../hooks/useActiveWeb3React'
import { useLingui } from '@lingui/react'
import { ANALYTICS_URL } from '../../constants'
import { ChainId, ChainIdDefault } from '../../sdk'

interface BalanceCardProps {
    mspaceEarnings?: number
    xmspaceBalance: BalanceProps
    mspaceBalance: BalanceProps
    weightedApr?: number,
    chainId?: ChainId,
}

export default function BalanceCard({
    xmspaceBalance,
    mspaceBalance,
    mspaceEarnings = 0,
    weightedApr = 0,
    chainId,
}: BalanceCardProps) {
    const { i18n } = useLingui()
    const { account } = useActiveWeb3React()
    return (
        <div className="flex flex-col w-full bg-dark-800 rounded px-4 md:px-8 mt-4 py-5 text-white">
            <div className="flex flex-wrap">
                <div className="flex flex-grow justify-between">
                    <p className="text-lg font-bold">
                        {i18n._(t`Balance`)}
                    </p>
                    <div className="flex items-center">
                        <img className="w-10 md:w-16 -ml-1 mr-1 md:mr-2 -mb-1.5" src={xMSP} alt="xMSP" />
                        <div className="flex flex-col justify-center">
                            <p className="text-caption2 md:text-lg">
                                {fixedFromBalance(xmspaceBalance.value)}
                            </p>
                            <p>xMSP</p>
                        </div>
                    </div>
                    <div className="flex items-center ml-8 md:ml-0">
                        <img className="w-10 md:w-16 -ml-1 mr-1 md:mr-2 -mb-1.5" src={MSP} alt="MSP" />
                        <div className="flex flex-col justify-center">
                            <p className="text-caption2 md:text-lg">
                                {fixedFromBalance(mspaceBalance.value)}
                            </p>
                            <p>MSP</p>
                        </div>
                    </div>
                    <p></p>
                </div>
                {account && (
                    <div className="flex flex-col w-full mt-6 mb-4 md:mb-0">
                        <a
                            href={`${ANALYTICS_URL[chainId ?? ChainIdDefault]}/users/${account}`}
                            target="_blank"    
                            rel="noreferrer noopener"
                            className={`
                                flex flex-grow justify-center items-center
                                h-14 rounded
                                bg-dark-700
                                focus:outline-none focus:ring hover:bg-opacity-80
                                text-caption2 font-bold cursor-pointer
                            `}
                        >
                            {i18n._(t`Your Bank Stats`)}
                        </a>
                    </div>
                )}
            </div>
        </div>
    )
}
