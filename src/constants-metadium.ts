export const METATESTNET_CONTRACTS = {
  Timelock: "0xACCcaA546A19C821a9dCd4133bD80161c1646b94",
  BoringHelperV1: "0xF26750552e929cd29561317719Bf45d9Cd296820",

  MSpace: "0x8a7C8755ff2d52F41346841176bEb54D4A3ad934",
  Mining: "0x60550807c387A7050D0c3B2c06E8d7b14211D14d",
  Bank: "0xF74475AdD8CB29794D8105dc7E4ece99bFC38575",
  Extractor: "0xc0DA7eB11913767d1D5Cbe7a5DC64Ff31b00433a",
  Factory: "0x044a1a5B0Dd5a337CC815C314170657225D15d52",
  Router02: "0x3FDFA448F66cC28ff29689823d7f8180141a27FE",

  Multicall2: "0x21ff38B0D0CB707c2ac7D437145074c9F6aaE103",
}

export const METATESTNET_TOKENS = {
  WETH: "0x2de8F3f0e07414e32790c257b009F76ecb11cFDf",
  WMETA: "0x6c9411fbb5c9f4cc9c2805e05a1a1390ee145b02",
  USDC: "0xb18D248866F03b88bEA4a256FC5593D7a68bb64B",
  DAI: "0x2251984A9b0ec156349d546d51Ebb7124Dc79588",
  WBTC: "0x8691B0EB0Ee188BA0d28A41a0aDd403F4e44FD47",

  MSP: METATESTNET_CONTRACTS.MSpace,
  xMSP: METATESTNET_CONTRACTS.Bank,
}

export const METADIUM_CONTRACTS = {
  Timelock: "0x7A2515Bf31196f61718029C7f8DADFEcEc9920fc",
  BoringHelperV1: "0x7E342bd6F7bC0fc9d76e5ef6167120a1151c879d",

  MSpace: "0x885c993fE9Dd57F6ACcB384692395D3cDD2b6F77",
  Mining: "0xb8692569c8129Ff24624552ec790d17b6B725104",
  Bank: "0x41c2C7C4a7dC266ae4F16558CB54b904CDCA279d",
  Extractor: "0x27BE7d67B2d935E7ABCC93414D154F2492094bc9",
  Factory: "0xc889864D24BD8c68cC4E7b4E3E1dFDc2455B83a3",
  Router02: "0xF3259973C529467bD48A18dD66d7706118b0e1fD",

  Multicall2: "0xA9B65EAC7F60aE72A263255ca7F95D8E335D73f2",
}

export const METADIUM_TOKENS = {
  WETH: "0x56538e02076d2978fDFbeae76128fCCC06ac102B",
  WMETA: "0x3dCB7c0b536022Eb3D33919A1493a5B7789E97D0",
  USDC: "0xe2600B19f17fa97f0d671EC2c513C99dB19f1971",
  DAI: "0xd148E880A106049dF1f72c2Ba92B4c7130AD6F20",
  WBTC: "0xb7A558AF782251f51152E290988bcD2AfFd1C204",

  MSP: METADIUM_CONTRACTS.MSpace,
  xMSP: METADIUM_CONTRACTS.Bank,
}

const MetatestnetTokens = [
  {
    "symbol": "mETH", "name": "Ethereum on Meta", "decimals": 18, "chainId": 12, 
    "logoURI": "https://swap.gocheetah.co/images/tokens/eth-square.jpg",
    "address": METATESTNET_TOKENS.WETH,
  },
  {
    "symbol": "mUSDC", "name": "USD Coin on Meta", "decimals": 6, "chainId": 12,
    "logoURI": "https://swap.gocheetah.co/images/tokens/usdc-square.jpg",
    "address": METATESTNET_TOKENS.USDC,
  },
  {
    "symbol": "mWBTC", "name": "BTC on Meta", "decimals": 8, "chainId": 12,
    "logoURI": "https://swap.gocheetah.co/images/tokens/btc-square.jpg",
    "address": METATESTNET_TOKENS.WBTC,
  },
  {
    "symbol": "mDAI", "name": "Dai Stablecoin on Meta", "decimals": 18, "chainId": 12,
    "logoURI": "https://swap.gocheetah.co/images/tokens/dai-square.jpg",
    "address": METATESTNET_TOKENS.DAI,
  },
  {
    "symbol": "MSP", "name": "MSP Token", "decimals": 18, "chainId": 12,
    "logoURI": "https://swap.gocheetah.co/images/tokens/msp.jpg",
    "address": METATESTNET_CONTRACTS.MSpace,
  },
  // {
  //   "symbol": "xMSP", "name": "Bank", "decimals": 18, "chainId": 12,
  //   "logoURI": "https://swap.gocheetah.co/images/tokens/xmsp.jpg",
  //   "address": METATESTNET_CONTRACTS.Bank,
  // },
]

const MetadiumTokens = [
  {
    "symbol": "mETH", "name": "Ethereum on Meta", "decimals": 18, "chainId": 11, 
    "logoURI": "https://swap.gocheetah.co/images/tokens/eth-square.jpg",
    "address": METADIUM_TOKENS.WETH,
  },
  {
    "symbol": "mUSDC", "name": "USD Coin on Meta", "decimals": 6, "chainId": 11,
    "logoURI": "https://swap.gocheetah.co/images/tokens/usdc-square.jpg",
    "address": METADIUM_TOKENS.USDC,
  },
  {
    "symbol": "mWBTC", "name": "BTC on Meta", "decimals": 8, "chainId": 11,
    "logoURI": "https://swap.gocheetah.co/images/tokens/btc-square.jpg",
    "address": METADIUM_TOKENS.WBTC,
  },
  {
    "symbol": "mDAI", "name": "Dai Stablecoin on Meta", "decimals": 18, "chainId": 11,
    "logoURI": "https://swap.gocheetah.co/images/tokens/dai-square.jpg",
    "address": METADIUM_TOKENS.DAI,
  },
  {
    "symbol": "MSP", "name": "MSP Token", "decimals": 18, "chainId": 11,
    "logoURI": "https://swap.gocheetah.co/images/tokens/msp.jpg",
    "address": METADIUM_CONTRACTS.MSpace,
  },
  // {
  //   "symbol": "xMSP", "name": "Bank", "decimals": 18, "chainId": 11,
  //   "logoURI": "https://swap.gocheetah.co/images/tokens/xmsp.svg",
  //   "address": METADIUM_CONTRACTS.Bank,
  // },
]

export const DEFAULT_TOKEN_LIST = [
  ...MetadiumTokens,
  ...MetatestnetTokens,
]

// can not use because factory not use create2 function
export const INIT_CODE_HASH_METADIUM = '0xb14745391249ed09daa2ffdff7ad783ecc5830974b3d46ed03154dd6e7ca9d99'
export const INIT_CODE_HASH_META_TESTNET = '0xb14745391249ed09daa2ffdff7ad783ecc5830974b3d46ed03154dd6e7ca9d99'

export const METATESTNET_POOL_DENY = []

export const DEFAULT_CHAIN_ID = process.env.REACT_APP_DEFAULT_CHAIN_ID ? Number.parseInt(process.env.REACT_APP_DEFAULT_CHAIN_ID) : 11;

export const SWAP_HOME_PAGE = process.env.REACT_APP_SWAP_HOME_PAGE;