import { Interface } from '@ethersproject/abi'
import IUniswapV2PairABI from '../constants/abis/pair.json'
import { Pair, TokenAmount, Currency } from '../sdk'
import { useMemo, useEffect, useState } from 'react'
import { useActiveWeb3React } from '../hooks/useActiveWeb3React'
import { useMultipleContractSingleData } from '../state/multicall/hooks'
import { wrappedCurrency } from '../utils/wrappedCurrency'
import { exchange } from '../subgraph/'

const PAIR_INTERFACE = new Interface(IUniswapV2PairABI)

export enum PairState {
    LOADING,
    NOT_EXISTS,
    EXISTS,
    INVALID
}

export function usePairs(
  currencies: {
    tokens: [Currency | undefined, Currency | undefined],
    pairAddress?: string
  }[]
): [PairState, Pair | null][] {
    const { chainId } = useActiveWeb3React()

    const tokens = useMemo(
        () =>
            currencies.map(currency => [
                wrappedCurrency(currency.tokens[0], chainId),
                wrappedCurrency(currency.tokens[1], chainId)
            ]),
        [chainId, currencies]
    )
    const pairAddresses = useMemo(
        () => currencies.map(currency => currency.pairAddress), 
        [chainId, currencies]
    );
    
    const results = useMultipleContractSingleData(pairAddresses, PAIR_INTERFACE, 'getReserves')

    return useMemo(() => {
        return results.map((result, i) => {
            const { result: reserves, loading } = result
            const tokenA = tokens[i][0]
            const tokenB = tokens[i][1]

            if (loading) return [PairState.LOADING, null]
            if (!tokenA || !tokenB || tokenA.equals(tokenB)) return [PairState.INVALID, null]
            if (!reserves) return [PairState.NOT_EXISTS, null]
            const { _reserve0: reserve0, _reserve1: reserve1 } = reserves
            if (!reserve0 || !reserve1) return [PairState.NOT_EXISTS, null]

            const [token0, token1] = tokenA.sortsBefore(tokenB) ? [tokenA, tokenB] : [tokenB, tokenA]
            return [
                PairState.EXISTS,
                new Pair(
                  new TokenAmount(token0, reserve0.toString()), 
                  new TokenAmount(token1, reserve1.toString()),
                  pairAddresses[i]
                )
            ]
        })
    }, [pairAddresses])
}

// TODO query pair address
export function usePair(tokenA?: Currency, tokenB?: Currency, pairAddress?: string): [PairState, Pair | null] {
    return usePairs([{tokens: [tokenA, tokenB], pairAddress}])[0]
}
