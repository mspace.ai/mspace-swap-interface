import { MenuFlyout, StyledMenu, StyledMenuButton } from 'components/StyledMenu'
import React, { useContext, useRef, useState } from 'react'
import { X } from 'react-feather'
import Setting from '../../assets/mspace/icon_setting.svg'
import { Text } from 'rebass'
import styled, { ThemeContext } from 'styled-components'
import { useOnClickOutside } from '../../hooks/useOnClickOutside'
import { ApplicationModal } from '../../state/application/actions'
import { useModalOpen, useToggleSettingsMenu } from '../../state/application/hooks'
import {
    useExpertModeManager,
    useUserSingleHopOnly,
    useUserSlippageTolerance,
    useUserTransactionTTL
} from '../../state/user/hooks'
import { TYPE } from '../../theme'
import { ButtonError, ButtonPrimary } from '../ButtonLegacy'
import { AutoColumn } from '../Column'
import Modal from '../Modal'
import QuestionHelper from '../QuestionHelper'
import { RowBetween, RowFixed } from '../Row'
import Toggle from '../Toggle'
import TransactionSettings from '../TransactionSettings'
import { t } from '@lingui/macro'
import { useLingui } from '@lingui/react'

// const StyledMenuIcon = styled(Setting)`
//     height: 20px;
//     width: 20px;

//     > * {
//         stroke: ${({ theme }) => theme.text2};
//     }

//     :hover {
//         opacity: 0.7;
//     }
// `

const StyledCloseIcon = styled(X)`
    height: 24px;
    width: 24px;
    :hover {
        cursor: pointer;
    }

    > * {
        stroke: ${({ theme }) => theme.text1};
    }
`

const EmojiWrapper = styled.div`
    position: absolute;
    bottom: -6px;
    right: 0px;
    font-size: 14px;
`

const Break = styled.div`
    width: 100%;
    height: 1px;
    background-color: ${({ theme }) => theme.bg3};
`

const ModalContentWrapper = styled.div`
    display: flex;
    align-items: center;
    justify-content: center;
`

const ExtendedMenuFlyout = styled(MenuFlyout)`
    min-width: 22rem;
    margin-right: -20px;

    ${({ theme }) => theme.mediaWidth.upToMedium`
        min-width: 20rem;
        margin-right: -10px;
  `};
`

export default function SettingsTab() {
    const { i18n } = useLingui()

    const node = useRef<HTMLDivElement>(null)
    const open = useModalOpen(ApplicationModal.SETTINGS)
    const toggle = useToggleSettingsMenu()

    const theme = useContext(ThemeContext)
    const [userSlippageTolerance, setUserslippageTolerance] = useUserSlippageTolerance()

    const [ttl, setTtl] = useUserTransactionTTL()

    const [expertMode, toggleExpertMode] = useExpertModeManager()

    const [singleHopOnly, setSingleHopOnly] = useUserSingleHopOnly()

    // show confirmation view before turning on
    const [showConfirmation, setShowConfirmation] = useState(false)

    // useOnClickOutside(node, open ? toggle : undefined)

    return (
        <StyledMenu ref={node}>
            <Modal isOpen={showConfirmation} onDismiss={() => setShowConfirmation(false)} maxHeight={100}>
                <ModalContentWrapper>
                    <AutoColumn gap="lg">
                        <RowBetween>
                            <div style={{ width: 24 }} />
                            <Text fontWeight={700} fontSize={18} color={theme.white}>
                                {i18n._(t`Turn On Expert Mode`)}
                            </Text>
                            <StyledCloseIcon onClick={() => setShowConfirmation(false)} />
                        </RowBetween>
                        <AutoColumn gap="lg">
                            <Text fontWeight={400} fontSize={12} color={theme.white} textAlign='center'>
                                {t`Expert mode turns off the confirm transaction prompt and allows high slippage trades
                                that often result in bad rates and lost funds.`}
                            </Text>
                            <Text fontWeight={700} fontSize={16} color={theme.white} textAlign='center'>
                                {i18n._(t`ONLY USE THIS MODE IF YOU KNOW WHAT YOU ARE DOING.`)}
                            </Text>
                            <ButtonPrimary
                                padding={'12px'}
                                onClick={() => {
                                    if (
                                        window.prompt(
                                            i18n._(t`Please type the word "confirm" to enable expert mode.`)
                                        ) === 'confirm'
                                    ) {
                                        toggleExpertMode()
                                        setShowConfirmation(false)
                                    }
                                }}
                            >
                                <Text fontSize={20} fontWeight={500} id="confirm-expert-mode">
                                    {i18n._(t`OK`)}
                                </Text>
                            </ButtonPrimary>
                        </AutoColumn>
                    </AutoColumn>
                </ModalContentWrapper>
            </Modal>
            <StyledMenuButton onClick={toggle} id="open-settings-dialog-button">
                <img src={Setting} alt="setting" className="h-6 w-auto hover:opacity-70" />
                {/* {expertMode ? (
                    <EmojiWrapper>
                        <span role="img" aria-label="wizard-icon">
                            🧙
                        </span>
                    </EmojiWrapper>
                ) : null} */}
            </StyledMenuButton>
            <Modal isOpen={open} onDismiss={toggle}>
                <ModalContentWrapper>
                    <AutoColumn gap="sm" style={{ padding: '0.5rem' }}>
                        <div className="text-lg font-bold text-white">
                            {i18n._(t`Transaction Settings`)}
                        </div>
                        <TransactionSettings
                            rawSlippage={userSlippageTolerance}
                            setRawSlippage={setUserslippageTolerance}
                            deadline={ttl}
                            setDeadline={setTtl}
                        />
                        <div className="text-base font-semibold text-high-emphesis" style={{ borderTop: 'solid 0.9px #4a4a4a', paddingTop: 16, marginTop: 16 }}>
                            {i18n._(t`Interface Settings`)}
                        </div>
                        <RowBetween className="items-start">
                            <RowFixed className="flex-col items-start pr-1">
                                <TYPE.black fontWeight={400} fontSize={14} color={theme.white}>
                                    {i18n._(t`Toggle Expert Mode`)}
                                </TYPE.black>
                                <TYPE.black fontWeight={400} fontSize={10} color={theme.text3}>
                                    {i18n._(t`Bypasses confirmation modals and allows high silppage trades. Use at your own risk.`)}
                                </TYPE.black>
                            </RowFixed>
                            <Toggle
                                id="toggle-expert-mode-button"
                                isActive={!expertMode}
                                toggle={
                                    expertMode
                                        ? () => {
                                            toggleExpertMode()
                                            setShowConfirmation(false)
                                        }
                                        : () => {
                                            toggle()
                                            setShowConfirmation(true)
                                        }
                                }
                            />
                        </RowBetween>
                        <RowBetween className="items-start">
                            <RowFixed className="flex-col items-start pr-1">
                                <TYPE.black fontWeight={400} fontSize={14} color={theme.white}>
                                    {i18n._(t`Auto Yield Farm`)}
                                </TYPE.black>
                                <TYPE.black fontWeight={400} fontSize={10} color={theme.text3}>
                                    {i18n._(t`Yield Farm automatically the liquidity you supply.`)}
                                </TYPE.black>
                            </RowFixed>
                            <Toggle
                                id="toggle-disable-multihop-button"
                                isActive={!singleHopOnly}
                                toggle={() => (singleHopOnly ? setSingleHopOnly(false) : setSingleHopOnly(true))}
                            />
                        </RowBetween>
                    </AutoColumn>
                </ModalContentWrapper>
            </Modal>
        </StyledMenu >
    )
}
