import React, { memo, useRef } from 'react'
import { useOnClickOutside } from '../../hooks/useOnClickOutside'
import { ApplicationModal } from '../../state/application/actions'
import { useModalOpen, useToggleModal } from '../../state/application/hooks'
import { useLanguageData } from '../../language/hooks'

function LanguageSwitch() {
    const node = useRef<HTMLDivElement>(null)
    const open = useModalOpen(ApplicationModal.LANGUAGE)
    const toggle = useToggleModal(ApplicationModal.LANGUAGE)
    useOnClickOutside(node, open ? toggle : undefined)
    const { language, setLanguage } = useLanguageData()

    const onClick = (key: string) => {
        setLanguage(key)
        toggle()
    }

    console.log(language)

    return <div className="flex flex-row text-sm items-center text-white">
        <span className={`font-bold cursor-pointer ${language === 'en' ? "opacity-100" : "opacity-50"}`} onClick={() => onClick('en')}>ENG</span>
        <span style={{ width: 2, height: 11, margin: "0 4px", backgroundColor: 'white' }}></span>
        <span className={`font-bold cursor-pointer ${language === 'ko' ? "opacity-100" : "opacity-50"}`} onClick={() => onClick('ko')}>KOR</span>
    </div>
}

export default memo(LanguageSwitch)
