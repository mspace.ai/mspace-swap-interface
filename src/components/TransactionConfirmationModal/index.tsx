import { ChainId } from '../../sdk'
import React, { useContext } from 'react'
import { AlertTriangle, ArrowUpCircle } from 'react-feather'
import { Text } from 'rebass'
import styled, { ThemeContext } from 'styled-components'
import Circle from '../../assets/mspace/icon_progress.png'
import IconCheck from '../../assets/mspace/icon_check.svg'
import { useActiveWeb3React } from '../../hooks/useActiveWeb3React'
import { ExternalLink } from '../../theme'
import { CloseIcon, CustomLightSpinner } from '../../theme/components'
import { getExplorerLink } from '../../utils'
import { ButtonPrimary } from '../ButtonLegacy'
import { AutoColumn, ColumnCenter } from '../Column'
import Modal from '../Modal'
import { RowBetween } from '../Row'
import Close from '../../assets/mspace/icon_close.svg'
import { useLingui } from '@lingui/react'
import { t } from '@lingui/macro'

const Wrapper = styled.div`
    width: 100%;
`
const Section = styled(AutoColumn)`
    // padding: 24px;
`

const BottomSection = styled(Section)`
    // background-color: ${({ theme }) => theme.bg2};
    border-bottom-left-radius: 20px;
    border-bottom-right-radius: 20px;
`

const ConfirmedIcon = styled(ColumnCenter)`
    padding: 60px 0;
`

function ConfirmationPendingContent({ onDismiss, pendingText }: { onDismiss: () => void; pendingText: string }) {
    const theme = useContext(ThemeContext)

    return (
        <Wrapper>
            <Section>
                <RowBetween>
                    <div />
                    <CloseIcon onClick={onDismiss} size={30} color={theme.white} />
                </RowBetween>
                <AutoColumn gap="12px" justify={'center'}>
                    <Text fontWeight={'bold'} fontSize={22} color={theme.white}>
                        Waiting For Confirmation
                    </Text>
                    <AutoColumn gap="12px" justify={'center'}>
                        <Text fontWeight={500} fontSize={18} color="#909090" textAlign="center" dangerouslySetInnerHTML={{ __html: pendingText }}>
                        </Text>
                    </AutoColumn>
                </AutoColumn>
                <ConfirmedIcon>
                    <CustomLightSpinner src={Circle} alt="loader" size={'90px'} />
                </ConfirmedIcon>
                <AutoColumn gap="12px" justify={'center'}>
                    <Text fontSize={16} color="#bd5fa6" textAlign="center" fontWeight='bold'>
                        Confirm this transaction in your wallet
                    </Text>
                </AutoColumn>
            </Section>
        </Wrapper>
    )
}

function TransactionSubmittedContent({
    onDismiss,
    chainId,
    hash
}: {
    onDismiss: () => void
    hash: string | undefined
    chainId: ChainId
}) {
    const { i18n } = useLingui()
    const theme = useContext(ThemeContext)

    return (
        <Wrapper>
            <Section>
                <RowBetween>
                    <div />
                    <CloseIcon onClick={onDismiss} size={30} color={theme.white} />
                </RowBetween>
                <AutoColumn gap="12px" justify={'center'}>
                    <Text fontWeight={'bold'} fontSize={22} color={theme.white}>
                        Transaction Submitted
                    </Text>
                    {chainId && hash && (
                        <ExternalLink href={getExplorerLink(chainId, hash, 'transaction')}>
                            <Text fontWeight={400} fontSize={16} color={'#bd5fa6'}>
                                {i18n._(t`View on explorer`)}
                            </Text>
                        </ExternalLink>
                    )}
                </AutoColumn>
                <ConfirmedIcon>
                    <img src={IconCheck} alt="success" style={{ width: 85, height: 85, objectFit: 'contain' }} />
                </ConfirmedIcon>
                <AutoColumn gap="12px" justify={'center'}>
                    <ButtonPrimary onClick={onDismiss} style={{ margin: '0' }}>
                        <Text fontWeight={900} fontSize={18}>
                            Close
                        </Text>
                    </ButtonPrimary>
                </AutoColumn>
            </Section>
        </Wrapper>
    )
}

export function ConfirmationModalContent({
    title,
    bottomContent,
    onDismiss,
    topContent
}: {
    title: string
    onDismiss: () => void
    topContent: () => React.ReactNode
    bottomContent: () => React.ReactNode
}) {
    return (
        <Wrapper>
            <Section>
                <div className="relative">
                    <Text className="font-bold text-white text-xl font-bold text-center">
                        {title}
                    </Text>
                    <div
                        className="absolute -top-1 right-0 cursor-pointer"
                        onClick={onDismiss}
                    >
                        <img src={Close} alt="close" style={{ width: 34, height: 34, objectFit: 'contain' }} />
                    </div>
                </div>
                {topContent()}
            </Section>
            <BottomSection>{bottomContent()}</BottomSection>
        </Wrapper>
    )
}

export function TransactionErrorContent({ message, onDismiss }: { message: string; onDismiss: () => void }) {
    const theme = useContext(ThemeContext)
    return (
        <Wrapper>
            <Section>
                <RowBetween>
                    <Text fontWeight={900} fontSize={20} color={theme.white}>
                        Error
                    </Text>
                    <CloseIcon onClick={onDismiss} size={30} color={theme.white} />
                </RowBetween>
                <AutoColumn style={{ marginTop: 20, padding: '2rem 0' }} gap="24px" justify="center">
                    <AlertTriangle color={theme.red1} style={{ strokeWidth: 1.5 }} size={64} />
                    <Text
                        fontWeight={500}
                        fontSize={16}
                        color={theme.red1}
                        style={{ textAlign: 'center', width: '85%' }}
                    >
                        {message}
                    </Text>
                </AutoColumn>
            </Section>
            <BottomSection gap="12px">
                <ButtonPrimary onClick={onDismiss}><span style={{ fontSize: 18 }}>Dismiss</span></ButtonPrimary>
            </BottomSection>
        </Wrapper>
    )
}

interface ConfirmationModalProps {
    isOpen: boolean
    onDismiss: () => void
    hash: string | undefined
    content: () => React.ReactNode
    attemptingTxn: boolean
    pendingText: string
}

export default function TransactionConfirmationModal({
    isOpen,
    onDismiss,
    attemptingTxn,
    hash,
    pendingText,
    content
}: ConfirmationModalProps) {
    const { chainId } = useActiveWeb3React()

    if (!chainId) return null

    // confirmation screen
    return (
        <Modal isOpen={isOpen} onDismiss={onDismiss} maxHeight={90}>
            {attemptingTxn ? (
                <ConfirmationPendingContent onDismiss={onDismiss} pendingText={pendingText} />
            ) : hash ? (
                <TransactionSubmittedContent chainId={chainId} hash={hash} onDismiss={onDismiss} />
            ) : (
                content()
            )}
        </Modal>
    )
}
