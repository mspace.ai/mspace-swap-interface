import { Currency, Pair, Token, WMETA, currencyEquals } from '../../sdk'
import { darken } from 'polished'
import React, { useCallback, useState, useMemo } from 'react'
import styled from 'styled-components'
import { ReactComponent as DropDown } from '../../assets/images/dropdown.svg'
import { t } from '@lingui/macro'
import { useActiveWeb3React } from '../../hooks/useActiveWeb3React'
import useTheme from '../../hooks/useTheme'
import { useCurrencyBalance } from '../../state/wallet/hooks'
import CurrencyLogo from '../CurrencyLogo'
import DoubleCurrencyLogo from '../DoubleLogo'
import { Input as NumericalInput } from '../NumericalInput'
import CurrencySearchModal from '../SearchModal/CurrencySearchModal'
// import Button from '../Button'
// import selectCoinAnimation from '../../assets/animation/select-coin.json'
// import Lottie from 'lottie-react'
// import { useUSDCPrice } from '../../hooks'
// import { formattedNum } from '../../utils'
import { wrappedCurrency } from '../../utils/wrappedCurrency'
import { useLingui } from '@lingui/react'
// import { getPairAddresses, PairAddressResult } from 'apollo/getPairAddresses'
import { USDC } from '../../constants'

const CurrencySelect = styled.button<{ selected: boolean }>`
    align-items: center;
    height: 100%;
    font-size: 20px;
    font-weight: 500;
    // background-color: ${({ selected, theme }) => (selected ? theme.bg1 : theme.primary1)};
    // color: ${({ selected, theme }) => (selected ? theme.text1 : theme.white)};
    // border-radius: ${({ theme }) => theme.borderRadius};
    // box-shadow: ${({ selected }) => (selected ? 'none' : '0px 6px 10px rgba(0, 0, 0, 0.075)')};
    outline: none;
    cursor: pointer;
    user-select: none;
    border: none;
    // padding: 0 0.5rem;

    :focus,
    :hover {
        // background-color: ${({ selected, theme }) => (selected ? theme.bg2 : darken(0.05, theme.primary1))};
    }
`

const StyledDropDown = styled(DropDown) <{ selected: boolean }>`
    margin: 0 0.25rem 0 0.5rem;
    height: 35%;

    path {
        stroke: ${({ selected, theme }) => (selected ? theme.text1 : theme.white)};
        stroke-width: 1.5px;
    }
`

const StyledTokenName = styled.span<{ active?: boolean }>`
//   ${({ active }) => (active ? '  margin: 0 0.25rem 0 0.75rem;' : '  margin: 0 0.25rem 0 0.25rem;')}
//   font-size:  ${({ active }) => (active ? '24px' : '12px')};
`

interface CurrencyInputPanelProps {
    value: string
    onUserInput: (value: string) => void
    onMax?: () => void
    showMaxButton: boolean
    label?: string
    onCurrencySelect?: (currency: Currency) => void
    currency?: Currency | null
    disableCurrencySelect?: boolean
    hideBalance?: boolean
    pair?: Pair | null
    hideInput?: boolean
    otherCurrency?: Currency | null
    id: string
    showCommonBases?: boolean
    customBalanceText?: string
    cornerRadiusBottomNone?: boolean
    cornerRadiusTopNone?: boolean
    containerBackground?: string
    isPoll?: boolean,
}

export default function CurrencyInputPanel({
    value,
    onUserInput,
    onMax,
    showMaxButton,
    label = 'Input',
    onCurrencySelect,
    currency,
    disableCurrencySelect = false,
    hideBalance = false,
    pair = null, // used for double token logo
    hideInput = false,
    otherCurrency,
    id,
    showCommonBases,
    customBalanceText,
    cornerRadiusBottomNone,
    cornerRadiusTopNone,
    containerBackground,
    isPoll,
}: CurrencyInputPanelProps) {
    const { i18n } = useLingui()
    const [modalOpen, setModalOpen] = useState(false)
    const { account, chainId } = useActiveWeb3React()
    const selectedCurrencyBalance = useCurrencyBalance(account ?? undefined, currency ?? undefined)
    const theme = useTheme()

    const handleDismissSearch = useCallback(() => {
        setModalOpen(false)
    }, [setModalOpen])

    const wrapped = wrappedCurrency(currency ?? undefined, chainId)
    const currencyPairs: [Token|undefined, Token|undefined][] = useMemo(
      () => [
          [
            chainId && wrapped && currencyEquals(WMETA[chainId], wrapped) ? undefined : wrappedCurrency(currency ?? undefined, chainId),
            chainId ? WMETA[chainId] : undefined
          ],
          [
            chainId && wrapped && wrapped.equals(USDC[chainId]) ? undefined : wrapped, 
            chainId ? USDC[chainId] : undefined
          ],
          [chainId ? WMETA[chainId] : undefined, chainId ? USDC[chainId] : undefined]
      ],
      [chainId, currency]
  )
    // const [pairs, setPairs] = useState<{
    //   tokens: [Token|undefined, Token|undefined], 
    //   pairAddress: string|undefined
    // }[]>([])
    // const fetchPairAddress = useCallback(async()=>{
    //     const results = await getPairAddresses(currencyPairs.map(([token0, token1]) => [token0?.address, token1?.address]), chainId)
    //     setPairs(results.map((result, i) => {return {
    //         tokens: currencyPairs[i],
    //         pairAddress: result.pairAddress
    //     }}))
    // }, [currencyPairs])
    // useEffect(() => {
    //     fetchPairAddress()
    // }, [fetchPairAddress]) 
    // const currencyUSDC = useUSDCPrice(pairs, chainId, currency ? currency : undefined)?.toFixed(18)
    // const valueUSDC = formattedNum(Number(value) * Number(currencyUSDC))

    return (
        <div id={id}>
            {label && (
                <div className="text-base text-secondary font-bold whitespace-nowrap mb-1.5">
                    {label}
                </div>
            )}
            <div className="flex space-y-0 flex-row justify-between" style={currency ? {
                borderStyle: 'solid',
                borderWidth: 2,
                borderImageSource: 'linear-gradient(99deg, #92009d 3%, #0089ff 51%, #00a482 96%)',
                backgroundColor: '#0f0a22',
                borderImageSlice: 1
            } : { backgroundColor: '#000', borderWidth: 2, borderColor: '#000' }}>
                {
                    !isPoll ? <div className="flex items-center rounded space-x-3 p-3 flex-1">
                        {!hideInput && (
                            <>
                                <NumericalInput
                                    className="token-amount-input"
                                    value={value}
                                    onUserInput={val => {
                                        onUserInput(val)
                                    }}
                                />
                            </>
                        )}
                    </div> : null
                }
                <div
                    className="flex justify-end"
                >
                    <CurrencySelect
                        selected={!!currency}
                        className="open-currency-select-button"
                        onClick={() => {
                            if (!disableCurrencySelect) {
                                setModalOpen(true)
                            }
                        }}
                    >
                        <div className="flex items-center" style={{
                            backgroundColor: currency ? '#1d1b25' : '', borderRadius: currency ? 7 : 0, padding: currency ? "4px 12px" : 0,
                            marginRight: isPoll ? 0 : 10, marginLeft: !isPoll ? 0 : 10
                        }}>
                            {pair ? (
                                <DoubleCurrencyLogo
                                    currency0={pair.token0}
                                    currency1={pair.token1}
                                    size={54}
                                    margin={true}
                                />
                            ) : currency ? (
                                <div className="flex">
                                    <CurrencyLogo currency={currency} size={'32px'} />
                                </div>
                            ) : (
                                <div />
                                // <div className="bg-dark-700 rounded" style={{ maxWidth: 24, maxHeight: 24 }}>
                                //     <div style={{ width: 24, height: 24 }}>
                                //         <Lottie animationData={selectCoinAnimation} autoplay loop />
                                //     </div>
                                // </div>
                            )}
                            {pair ? (
                                <StyledTokenName className="pair-name-container">
                                    {pair?.token0.symbol}:{pair?.token1.symbol}
                                </StyledTokenName>
                            ) : (
                                <div className="flex flex-1 flex-col items-start justify-center w-full text-left overflow-x-hidden"
                                    style={{ marginLeft: currency ? 8 : 0 }}>
                                    <div className="flex items-center">
                                        <div className="text-sm font-bold whitespace-nowrap text-white">
                                            {(currency && currency.symbol && currency.symbol.length > 20
                                                ? currency.symbol.slice(0, 4) +
                                                '...' +
                                                currency.symbol.slice(
                                                    currency.symbol.length - 5,
                                                    currency.symbol.length
                                                )
                                                : currency?.getSymbol(chainId)) || (
                                                    isPoll ?
                                                        <div className="flex items-center font-normal" style={{
                                                            backgroundColor: '#1d1b25', borderRadius: 7, padding: "4px 12px", height: 40
                                                        }}>
                                                            {i18n._(t`Select a Token`)}
                                                            <StyledDropDown selected={true} />
                                                        </div> : <div className="bg-transparent hover:bg-primary text-xs font-medium whitespace-nowrap"
                                                            style={{ padding: "7px 12px", border: 'solid 1px #808080', borderRadius: 3, color: '#808080' }}>
                                                            {i18n._(t`Select a Token`)}
                                                        </div>
                                                )}
                                        </div>
                                        {!disableCurrencySelect && currency && <StyledDropDown selected={!!currency} />}
                                    </div>
                                </div>
                            )}
                        </div>
                    </CurrencySelect>
                </div>

                {
                    isPoll ? <div className="flex items-center rounded space-x-3 p-3 flex-1">
                        {!hideInput && (
                            <>
                                <NumericalInput
                                    className="token-amount-input"
                                    value={value}
                                    onUserInput={val => {
                                        onUserInput(val)
                                    }}
                                />
                            </>
                        )}
                    </div> : null
                }
                {/* {
                    isPoll && currency && valueUSDC ? <div className="font-black text-base flex items-center px-3 text-white">
                        = {valueUSDC}
                    </div> : null
                } */}
            </div>
            {account && (
                <div className="flex flex-row justify-end items-center mt-2">
                    <div
                        onClick={onMax}
                        className="font-normal cursor-pointer text-sm text-secondary"
                    >
                        {!hideBalance && !!currency && selectedCurrencyBalance
                            ? <div>{(customBalanceText ?? i18n._(t`BALANCE`))}<span className="font-bold" style={{ marginLeft: 8 }}>{selectedCurrencyBalance?.toSignificant(6)}</span></div>
                            : ''}
                    </div>
                    {account && currency && showMaxButton && label !== 'To' && (
                        <div
                            onClick={onMax}
                            className="bg-white rounded-full text-black text-sm font-medium cursor-pointer"
                            style={{ padding: '2px 9px', marginLeft: 8 }}
                        >
                            {i18n._(t`max`)}
                        </div>
                    )}
                    {/* {chainId === ChainId.MAINNET && (
                        <div className="font-medium text-xs text-secondary">≈ {valueUSDC} USDC</div>
                    )} */}
                </div>
            )
            }
            {
                !disableCurrencySelect && onCurrencySelect && (
                    <CurrencySearchModal
                        isOpen={modalOpen}
                        onDismiss={handleDismissSearch}
                        onCurrencySelect={onCurrencySelect}
                        selectedCurrency={currency}
                        otherSelectedCurrency={otherCurrency}
                        showCommonBases={showCommonBases}
                    />
                )
            }
        </div >
    )
}
