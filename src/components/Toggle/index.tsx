import React from 'react'
import styled from 'styled-components'

const ToggleElement = styled.span<{ isActive?: boolean; isOnSwitch?: boolean }>`
    width: ${({ isActive }) =>
        isActive ? '27px' : '20px'};
    height: 27px;
    border-radius: 14px;
    background: ${({ isActive, isOnSwitch }) =>
        isActive ? (isOnSwitch ? 'white' : 'white') : 'none'};

    :hover {
        user-select: ${({ isOnSwitch }) => (isOnSwitch ? 'none' : 'initial')};
        background: ${({ isActive, isOnSwitch }) =>
            isActive ? (isOnSwitch ? 'white' : 'white') : 'none'};
    }
`

const StyledToggle = styled.button<{ isActive?: boolean; activeElement?: boolean }>`
    width: 51px;
    border-radius: 14px;
    border: none;
    background: ${({ isActive }) => isActive ? '#525252' : '#e92622'};
    display: flex;
    width: fit-content;
    cursor: pointer;
    outline: none;
    padding: 2px;
`

export interface ToggleProps {
    id?: string
    isActive: boolean
    toggle: () => void
}

export default function Toggle({ id, isActive, toggle }: ToggleProps) {
    return (
        <StyledToggle id={id} isActive={isActive} onClick={toggle}>
            <ToggleElement isActive={isActive} isOnSwitch={false}>
            </ToggleElement>
            <ToggleElement isActive={!isActive} isOnSwitch={true
            }>
            </ToggleElement>
        </StyledToggle>
    )
}
