import { Trade } from '../../sdk'
import { useActiveWeb3React } from 'hooks/useActiveWeb3React'
import React, { Fragment, memo, useContext } from 'react'
import { ChevronRight } from 'react-feather'
import { Flex } from 'rebass'
import { ThemeContext } from 'styled-components'
import { unwrappedToken } from 'utils/wrappedCurrency'
import { TYPE } from '../../theme'
import CurrencyLogo from '../CurrencyLogo'

export default memo(function SwapRoute({ trade }: { trade: Trade }) {
    const theme = useContext(ThemeContext)
    const { chainId } = useActiveWeb3React()
    return (
        <Flex flexWrap="wrap" width="100%" justifyContent="center" alignItems="center" style={{ marginTop: 12 }}>
            {trade.route.path.map((token, i, path) => {
                const isLastItem: boolean = i === path.length - 1
                const currency = unwrappedToken(token)
                return (
                    <Fragment key={i}>
                        <div className="flex items-center space-x-2">
                            <CurrencyLogo currency={currency} size={'32px'} />
                            <div className="text-sm font-bold text-high-emphesis">{currency.getSymbol(chainId)}</div>
                        </div>
                        {isLastItem ? null : <ChevronRight size={20} color={theme.white} style={{ margin: '0 5px' }} />}
                    </Fragment>
                )
            })}
        </Flex>
    )
})
