import { Trade, TradeType } from '../../sdk'
import React, { useContext, useMemo } from 'react'
import { AlertTriangle, ArrowDown } from 'react-feather'
import { Text } from 'rebass'
import { ThemeContext } from 'styled-components'
import { useActiveWeb3React } from '../../hooks/useActiveWeb3React'
import { Field } from '../../state/swap/actions'
import { TYPE } from '../../theme'
import { isAddress, shortenAddress } from '../../utils'
import { computeSlippageAdjustedAmounts, computeTradePriceBreakdown, warningSeverity } from '../../utils/prices'
import { ButtonPrimary } from '../ButtonLegacy'
import { AutoColumn } from '../Column'
import CurrencyLogo from '../CurrencyLogo'
import { RowBetween, RowFixed } from '../Row'
import { SwapShowAcceptChanges, TruncatedText } from './styleds'
import { t, Trans } from '@lingui/macro'
import { useLingui } from '@lingui/react'

export default function SwapModalHeader({
    trade,
    allowedSlippage,
    recipient,
    showAcceptChanges,
    onAcceptChanges
}: {
    trade: Trade
    allowedSlippage: number
    recipient: string | null
    showAcceptChanges: boolean
    onAcceptChanges: () => void
}) {
    const { i18n } = useLingui()
    const { chainId } = useActiveWeb3React()
    const slippageAdjustedAmounts = useMemo(() => computeSlippageAdjustedAmounts(trade, allowedSlippage), [
        trade,
        allowedSlippage
    ])
    const { priceImpactWithoutFee } = useMemo(() => computeTradePriceBreakdown(trade), [trade])
    const priceImpactSeverity = warningSeverity(priceImpactWithoutFee)

    const theme = useContext(ThemeContext)

    return (
        <AutoColumn gap={'0'} style={{ marginTop: '20px', border: 'solid 1px #575757', padding: 20 }}>
            <RowBetween align="flex-end" style={{ backgroundColor: '#191919', padding: "12px 16px" }}>
                <RowFixed gap={'0px'}>
                    <TruncatedText
                        fontSize={24}
                        fontWeight={900}
                        color={theme.white}
                    >
                        {trade.inputAmount.toSignificant(6)}
                    </TruncatedText>
                </RowFixed>
                <RowFixed gap={'0px'}>
                    <CurrencyLogo currency={trade.inputAmount.currency} size={'30px'} style={{ marginLeft: '10px', marginRight: '6px' }} />
                    <Text fontSize={16} fontWeight={700} color={theme.white}>
                        {trade.inputAmount.currency.getSymbol(chainId)}
                    </Text>
                </RowFixed>
            </RowBetween>
            {
                trade.tradeType === TradeType.EXACT_INPUT ? <RowFixed className="w-full" style={{ margin: 4 }}>
                    <div className="flex flex-col justify-center items-center w-full">
                        <div style={{ width: 12, height: 12, backgroundColor: '#575757' }}></div>
                        <div style={{
                            width: 0, height: 0, borderLeft: '12px solid transparent',
                            borderRight: '12px solid transparent', borderTop: '12px solid #575757'
                        }}>
                        </div>
                    </div>
                </RowFixed> : null
            }
            <RowBetween align="flex-end" style={{ backgroundColor: '#191919', padding: "12px 16px" }}>
                <RowFixed gap={'0px'}>
                    <TruncatedText
                        fontSize={24}
                        fontWeight={900}
                        color={theme.white}
                    >
                        {trade.outputAmount.toSignificant(6)}
                    </TruncatedText>
                </RowFixed>
                <RowFixed gap={'0px'}>
                    <CurrencyLogo currency={trade.outputAmount.currency} size={'30px'} style={{ marginLeft: '10px', marginRight: '6px' }} />
                    <Text fontSize={16} fontWeight={700} color={theme.white}>
                        {trade.outputAmount.currency.getSymbol(chainId)}
                    </Text>
                </RowFixed>
            </RowBetween>
            {
                showAcceptChanges ? (
                    <SwapShowAcceptChanges justify="flex-start" gap={'0px'}>
                        <RowBetween>
                            <RowFixed>
                                <AlertTriangle size={20} style={{ marginRight: '6px', minWidth: 24 }} color='#f30' />
                                <TYPE.main color={'#f30'} style={{ fontSize: 14 }}> {i18n._(t`Price Updated`)}</TYPE.main>
                            </RowFixed>
                            <ButtonPrimary
                                style={{
                                    padding: '7px 17px',
                                    width: 'fit-content',
                                    fontSize: 14,
                                    borderRadius: 5,
                                    fontWeight: 'bold',
                                    backgroundImage: 'linear-gradient(174deg, #4ec8fb -151%, #500c5d 168%)'
                                }}
                                onClick={onAcceptChanges}
                            >
                                {i18n._(t`Accept`)}
                            </ButtonPrimary>
                        </RowBetween>
                    </SwapShowAcceptChanges>
                ) : null
            }
            <AutoColumn justify="flex-start" gap="sm" style={{ padding: '12px 0 0 0px' }}>
                {trade.tradeType === TradeType.EXACT_INPUT ? (
                    <TYPE.italic textAlign="center" color={theme.text3} style={{ width: '100%', fontStyle: 'normal', fontSize: 14, margin: '24px 0' }}>
                        <Trans>
                            Output is estimated. You will receive at least{' '}
                            <b className="text-white">
                                {slippageAdjustedAmounts[Field.OUTPUT]?.toSignificant(6)}{' '}
                                {trade.outputAmount.currency.getSymbol(chainId)}
                            </b>{' '}
                            or the transaction will revert.
                        </Trans>
                    </TYPE.italic>
                ) : (
                    <TYPE.italic textAlign="center" color={!showAcceptChanges ? theme.white : theme.text3} style={{ width: '100%', fontStyle: 'normal', fontSize: 14, margin: showAcceptChanges ? '12px 0' : '24px 0' }}>
                        <Trans>
                            Input is estimated. You will sell at most{' '}
                            <b className="text-white">
                                {slippageAdjustedAmounts[Field.INPUT]?.toSignificant(6)}{' '}
                                {trade.inputAmount.currency.getSymbol(chainId)}
                            </b>{' '}
                            or the transaction will revert.
                        </Trans>
                    </TYPE.italic>
                )}
            </AutoColumn>
            {
                recipient !== null ? (
                    <AutoColumn justify="flex-start" gap="sm" style={{ padding: '12px 0 0 0px' }}>
                        <TYPE.main>
                            <Trans>
                                Output will be sent to{' '}
                                <b title={recipient} className="text-white">{isAddress(recipient) ? shortenAddress(recipient) : recipient}</b>
                            </Trans>
                        </TYPE.main>
                    </AutoColumn>
                ) : null
            }
        </AutoColumn >
    )
}
