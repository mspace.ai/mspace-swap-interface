import { Price } from '../../sdk'
import React, { useContext } from 'react'
import { Repeat } from 'react-feather'
import { Text } from 'rebass'
import { ThemeContext } from 'styled-components'
import { useActiveWeb3React } from '../../hooks/useActiveWeb3React'
import { StyledBalanceMaxMini } from './styleds'

interface TradePriceProps {
    price?: Price
    showInverted: boolean
    setShowInverted: (showInverted: boolean) => void
}

export default function TradePrice({ price, showInverted, setShowInverted }: TradePriceProps) {
    const { chainId } = useActiveWeb3React()
    const theme = useContext(ThemeContext)

    const formattedPrice = showInverted ? price?.toSignificant(6) : price?.invert()?.toSignificant(6)

    const show = Boolean(price?.baseCurrency && price?.quoteCurrency)
    const label = showInverted
        ? <div><span className="font-bold text-white">{price?.quoteCurrency?.getSymbol(chainId)}</span> per {price?.baseCurrency?.getSymbol(chainId)}</div>
        : <div><span className="font-bold text-white">{price?.baseCurrency?.getSymbol(chainId)}</span> per {price?.quoteCurrency?.getSymbol(chainId)}</div>

    return (
        <Text
            fontWeight={400}
            fontSize={14}
            color={theme.text3}
            style={{ justifyContent: 'center', alignItems: 'center', display: 'flex' }}
        >
            {show ? (
                <>
                    <span className="font-bold text-white">{formattedPrice ?? '-'}</span>&nbsp;{label}
                    <StyledBalanceMaxMini onClick={() => setShowInverted(!showInverted)} style={{ backgroundColor: 'transparent'}}>
                        <Repeat size={14} color={'white'}/>
                    </StyledBalanceMaxMini>
                </>
            ) : (
                '-'
            )}
        </Text>
    )
}
