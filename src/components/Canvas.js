import React, { useRef, useEffect } from 'react'
import useWindowSize from '../hooks/useWindowSize'

const Canvas = props => {
    const { width } = useWindowSize()
    const canvasRef = useRef(null)

    const draw = ctx => {
        ctx.clearRect(0, 0, ctx.canvas.width, ctx.canvas.height)
        let pointTopLeft = { x: 7, y: 4, r: 4 }
        let pointTopRight = { x: ctx.canvas.width - 4.5, y: 16.5, r: 3.5 }
        let pointBottomLeft = { x: 3.5, y: ctx.canvas.height - 14.5, r: 3.5 }
        let pointBottomRight = { x: ctx.canvas.width - 5.5, y: ctx.canvas.height - 5.5, r: 5.5 }

        if (width < 640) {
            pointTopLeft = { x: 3.5, y: 3.5, r: 3.5 }
            pointTopRight = { x: ctx.canvas.width - 3.5, y: 4, r: 3 }
            pointBottomLeft = { x: 4, y: ctx.canvas.height - 10.5, r: 3 }
            pointBottomRight = { x: ctx.canvas.width - 4.5, y: ctx.canvas.height - 4.5, r: 4.5 }
        } else if (props.case === 1) {
            pointTopLeft = { x: 8, y: 4, r: 4 }
            pointTopRight = { x: ctx.canvas.width - 11.5, y: 16.5, r: 3.5 }
            pointBottomLeft = { x: 3.5, y: ctx.canvas.height - 3.5, r: 3.5 }
            pointBottomRight = { x: ctx.canvas.width - 5.5, y: ctx.canvas.height - 16.5, r: 5.5 }
        } else if (props.case === 2) {
            pointTopLeft = { x: Math.random() * 10 + 3, y: Math.random() * 10 + 3, r: 4 }
            pointTopRight = { x: ctx.canvas.width - Math.random() * 10 - 4.5, y: Math.random() * 10 + 4.5, r: 4 }
            pointBottomLeft = { x: Math.random() * 10 + 4.5, y: ctx.canvas.height - Math.random() * 10 - 4.5, r: 4 }
            pointBottomRight = { x: ctx.canvas.width - Math.random() * 10 - 4.5, y: ctx.canvas.height - Math.random() * 10 - 4.5, r: 4 }
        }

        // draw rectangle
        ctx.beginPath()
        ctx.moveTo(pointTopLeft.x, pointTopLeft.y)
        ctx.lineTo(pointTopRight.x, pointTopRight.y)
        ctx.lineTo(pointBottomRight.x, pointBottomRight.y)
        ctx.lineTo(pointBottomLeft.x, pointBottomLeft.y)
        ctx.fill();

        // draw line
        let grad = ctx.createLinearGradient(pointTopLeft.x, pointTopLeft.y, pointTopRight.x, pointTopRight.y);
        grad.addColorStop(0, "white");
        grad.addColorStop(1, "black");
        ctx.strokeStyle = grad;
        ctx.beginPath();
        ctx.moveTo(pointTopLeft.x, pointTopLeft.y);
        ctx.lineTo(pointTopRight.x, pointTopRight.y);
        ctx.stroke();

        grad = ctx.createLinearGradient(pointTopLeft.x, pointTopLeft.y, pointBottomLeft.x, pointBottomLeft.y);
        grad.addColorStop(0, "white");
        grad.addColorStop(1, "black");
        ctx.strokeStyle = grad;
        ctx.beginPath();
        ctx.moveTo(pointTopLeft.x, pointTopLeft.y);
        ctx.lineTo(pointBottomLeft.x, pointBottomLeft.y);
        ctx.stroke();

        grad = ctx.createLinearGradient(pointBottomRight.x, pointBottomRight.y, pointTopRight.x, pointTopRight.y);
        grad.addColorStop(0, "white");
        grad.addColorStop(1, "black");
        ctx.strokeStyle = grad;
        ctx.beginPath();
        ctx.moveTo(pointBottomRight.x, pointBottomRight.y);
        ctx.lineTo(pointTopRight.x, pointTopRight.y);
        ctx.stroke();

        grad = ctx.createLinearGradient(pointBottomRight.x, pointBottomRight.y, pointBottomLeft.x, pointBottomLeft.y);
        grad.addColorStop(0, "white");
        grad.addColorStop(1, "black");
        ctx.strokeStyle = grad;
        ctx.beginPath();
        ctx.moveTo(pointBottomRight.x, pointBottomRight.y);
        ctx.lineTo(pointBottomLeft.x, pointBottomLeft.y);
        ctx.stroke();

        // draw circle
        ctx.fillStyle = 'white'
        ctx.beginPath()
        ctx.arc(pointTopLeft.x, pointTopLeft.y, pointTopLeft.r, 0, 2 * Math.PI)
        ctx.fill()
        ctx.beginPath()
        ctx.arc(pointTopRight.x, pointTopRight.y, pointTopRight.r, 0, 2 * Math.PI)
        ctx.fill()
        ctx.beginPath()
        ctx.arc(pointBottomRight.x, pointBottomRight.y, pointBottomRight.r, 0, 2 * Math.PI)
        ctx.fill()
        ctx.beginPath()
        ctx.arc(pointBottomLeft.x, pointBottomLeft.y, pointBottomLeft.r, 0, 2 * Math.PI)
        ctx.fill()
    }

    useEffect(() => {
        const canvas = canvasRef.current
        const context = canvas.getContext('2d')
        canvas.style.width = "100%";
        canvas.style.height = "100%";
        canvas.width = canvas.offsetWidth;
        canvas.height = canvas.offsetHeight;
        draw(context)
    }, [draw])

    return <canvas ref={canvasRef} {...props} />
}

export default Canvas