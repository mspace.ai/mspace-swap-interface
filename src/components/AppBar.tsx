import { ChainId, ChainIdDefault, Currency } from '../sdk'
import React, { useEffect, useState } from 'react'
import { useLocation } from 'react-router-dom'
import { useActiveWeb3React } from '../hooks/useActiveWeb3React'
import { useMetaBalances } from '../state/wallet/hooks'
import Web3Status from './Web3Status'
import MoreMenu from './Menu'
import { ExternalLink, NavLink } from './Link'
import { Disclosure } from '@headlessui/react'
import { ANALYTICS_URL } from '../constants'
import { t } from '@lingui/macro'
import LanguageSwitch from './LanguageSwitch'
import { useLingui } from '@lingui/react'
import Settings from './Settings'
import IconChart from '../assets/mspace/icon_chart.svg'
import Logo from '../assets/mspace/logo.svg'
import useWindowSize from '../hooks/useWindowSize'

type AppProps = {
    refScroll: any
}

const AppBar = ({ refScroll }: AppProps) => {
    const { i18n } = useLingui()
    const { account, chainId, library } = useActiveWeb3React()
    const { pathname } = useLocation()

    const [navClassList, setNavClassList] = useState(
        'w-screen bg-transparent z-10 backdrop-filter backdrop-blur'
    )

    const userMetaBalance = useMetaBalances(account ? [account] : [])?.[account ?? '']

    const [scrolling, setScrolling] = useState(true);
    const [scrollTop, setScrollTop] = useState(0);
    const { width } = useWindowSize()

    useEffect(() => {
        function onScroll() {
            let currentPosition = refScroll.current.scrollTop;
            if (currentPosition > scrollTop) {
                // downscroll code
                if (currentPosition > scrollTop + 50) {
                    setScrollTop(currentPosition <= 0 ? 0 : currentPosition)
                    setScrolling(false)
                }
            } else {
                // upscroll code
                if (currentPosition < scrollTop - 50) {
                    setScrollTop(currentPosition <= 0 ? 0 : currentPosition)
                    setScrolling(true)
                }
            }
        }

        refScroll.current.addEventListener("scroll", onScroll);
        return () => refScroll.current.removeEventListener("scroll", onScroll);
    }, [scrollTop])

    useEffect(() => {
        if (pathname === '/trade') {
            setNavClassList('w-screen bg-transparent z-10 backdrop-filter backdrop-blur')
        } else {
            setNavClassList('w-screen bg-transparent z-10 backdrop-filter backdrop-blur')
        }
    }, [pathname])

    return (
        <header className="flex flex-row flex-nowrap justify-between w-screen">
            <Disclosure as="nav" className={navClassList}>
                {({ open }) => (
                    <>
                        <div className="px-4 py-0 md:py-1.5" style={{ backgroundColor: 'rgba(0, 0, 0, 0.36)' }}>
                            <div className="flex items-center justify-between h-16">
                                <div className="flex items-center">
                                    <a className="flex flex-row flex-shrink-0 items-center" href={'/swap'}>
                                        <img src={Logo} alt="MSP" className="h-11 w-auto" />
                                    </a>
                                    <div className="hidden md:block md:ml-4">
                                        <div className="flex space-x-2">
                                            <NavLink id={`swap-nav-link`} to={'/swap'}>
                                                {i18n._(t`Swap`)}
                                            </NavLink>
                                            <NavLink
                                                id={`pool-nav-link`}
                                                to={'/pool'}
                                                isActive={(match, { pathname }) =>
                                                    Boolean(match) ||
                                                    pathname.startsWith('/add') ||
                                                    pathname.startsWith('/remove') ||
                                                    pathname.startsWith('/create') ||
                                                    pathname.startsWith('/find')
                                                }
                                            >
                                                {i18n._(t`Pool`)}
                                            </NavLink>
                                            {chainId && [ChainId.METADIUM, ChainId.META_TESTNET].includes(chainId) && (
                                                <NavLink id={`yield-nav-link`} to={'/mining'}>
                                                    {i18n._(t`Mining`)}
                                                </NavLink>
                                            )}
                                            <NavLink id={`bank-nav-link`} to={'/bank'}>
                                                {i18n._(t`Bank`)}
                                            </NavLink>
                                            {chainId &&
                                                [
                                                    ChainId.METADIUM,
                                                    ChainId.META_TESTNET
                                                ].includes(chainId) && (
                                                    <ExternalLink
                                                        id={`analytics-nav-link`}
                                                        className="flex flex-row justify-center items-center p-2 md:p-6"
                                                        href={ANALYTICS_URL[chainId ?? ChainIdDefault]}
                                                    >
                                                        {i18n._(t`Chart`)}
                                                        <img src={IconChart} style={{ width: 26, height: 26, marginLeft: 5 }} />
                                                    </ExternalLink>
                                                )}
                                        </div>
                                    </div>
                                </div>

                                <div className="flex flex-row items-center justify-center w-auto bg-transparent">
                                    <div className="flex items-center justify-end space-x-2 w-full">

                                        <div className="flex w-auto items-center whitespace-nowrap text-base font-bold cursor-pointer select-none 
                                        pointer-events-auto">
                                            {account && chainId && userMetaBalance && (
                                                <>
                                                    <div className="text-black font-bold rounded-full bg-white hidden md:inline-block" style={{ padding: "6px 20px", marginRight: 8 }}>
                                                        {userMetaBalance?.toSignificant(4)}{' '}
                                                        {Currency.getNativeCurrencySymbol(chainId)}
                                                    </div>
                                                </>
                                            )}
                                            <div className={!account ? "inline-block" : (width < 768 ? "hidden" : "inline-block")}>
                                                <Web3Status />
                                            </div>
                                        </div>
                                        <div className="md:hidden">
                                            <MoreMenu />
                                        </div>

                                        <div className="p-0 md:px-1 hidden md:inline-flex">
                                            <Settings />
                                        </div>

                                        <LanguageSwitch />
                                    </div>
                                </div>
                                <div className="flex flex-row items-center justify-center w-full p-4 fixed left-0 bottom-0 bg-transparent md:hidden">
                                    <div className={`flex items-center w-full rounded-full p-2`}
                                        style={{
                                            backgroundColor: '#313348', height: 50,
                                            transition: `all 200ms ${scrolling ? 'ease-in' : 'ease-out'}`,
                                            transform: scrolling ? 'translateY(0px)' : 'translateY(100px)'
                                        }}>
                                        <NavLink id={`swap-nav-link`} to={'/swap'} className="flex flex-1 justify-center">
                                            {i18n._(t`Swap`)}
                                        </NavLink>
                                        <NavLink className="flex flex-1 justify-center"
                                            id={`pool-nav-link`}
                                            to={'/pool'}
                                            isActive={(match, { pathname }) =>
                                                Boolean(match) ||
                                                pathname.startsWith('/add') ||
                                                pathname.startsWith('/remove') ||
                                                pathname.startsWith('/create') ||
                                                pathname.startsWith('/find')
                                            }
                                        >
                                            {i18n._(t`Pool`)}
                                        </NavLink>
                                        {chainId && [ChainId.METADIUM, ChainId.META_TESTNET].includes(chainId) && (
                                            <NavLink id={`yield-nav-link`} to={'/mining'} className="flex flex-1 justify-center">
                                                {i18n._(t`Mining`)}
                                            </NavLink>
                                        )}
                                        <NavLink id={`bank-nav-link`} to={'/bank'} className="flex flex-1 justify-center">
                                            {i18n._(t`Bank`)}
                                        </NavLink>
                                        {chainId &&
                                            [
                                                ChainId.METADIUM,
                                                ChainId.META_TESTNET
                                            ].includes(chainId) && (
                                                <ExternalLink
                                                    className="flex flex-1 justify-center items-center flex flex-row"
                                                    id={`analytics-nav-link`}
                                                    href={ANALYTICS_URL[chainId ?? ChainIdDefault]}
                                                >
                                                    {i18n._(t`Chart`)}
                                                    <img src={IconChart} style={{ width: 18, height: 18, marginLeft: 3 }} />
                                                </ExternalLink>
                                            )}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </>
                )}
            </Disclosure>
        </header>
    )
}

export default AppBar
