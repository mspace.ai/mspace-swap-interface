import { JSBI, Pair, Percent, TokenAmount, ChainIdDefault } from '../../sdk'
import { darken, transparentize } from 'polished'
import React, { useState, useContext } from 'react'
import { ChevronDown, ChevronUp } from 'react-feather'
import { Link } from 'react-router-dom'
import { Text } from 'rebass'
import styled from 'styled-components'
import { ANALYTICS_URL, BIG_INT_ZERO } from '../../constants'
import { useTotalSupply } from '../../data/TotalSupply'
import { useActiveWeb3React } from '../../hooks/useActiveWeb3React'
import { useColor } from '../../hooks/useColor'
import { useTokenBalance } from '../../state/wallet/hooks'
import { currencyId } from '../../utils/currencyId'
import { unwrappedToken } from '../../utils/wrappedCurrency'
import { ButtonEmpty, ButtonPrimary } from '../ButtonLegacy'
import Card, { LightCard } from '../CardLegacy'
import { AutoColumn } from '../Column'
import CurrencyLogo from '../CurrencyLogo'
import DoubleCurrencyLogo from '../DoubleLogo'
import { AutoRow, RowBetween, RowFixed } from '../Row'
import { Dots } from '../swap/styleds'
import { t } from '@lingui/macro'
import { useLingui } from '@lingui/react'
import { ThemeContext } from 'styled-components'
import IconChart from 'assets/mspace/blue_chart.svg'

export const FixedHeightRow = styled(RowBetween)`
    height: 24px;
`

export const HoverCard = styled(Card)`
    border: 1px solid transparent;
    :hover {
        border: 1px solid ${({ theme }) => darken(0.06, theme.bg2)};
    }
`
const StyledPositionCard = styled(LightCard) <{ bgColor: any }>`
  /* border: 1px solid ${({ theme }) => theme.text4}; */
  border: none
  background: ${({ theme }) => transparentize(0.6, theme.bg1)};
  /* background: ${({ theme, bgColor }) =>
        `radial-gradient(91.85% 100% at 1.84% 0%, ${transparentize(0.8, bgColor)} 0%, ${theme.bg3} 100%) `}; */
  position: relative;
  overflow: hidden;
`

interface PositionCardProps {
    pair: Pair
    showUnwrapped?: boolean
    border?: string
    stakedBalance?: TokenAmount // optional balance to indicate that liquidity is deposited in mining pool
}

export function MinimalPositionCard({ pair, showUnwrapped = false, border }: PositionCardProps) {
    const { i18n } = useLingui()
    const { account, chainId } = useActiveWeb3React()

    const currency0 = showUnwrapped ? pair.token0 : unwrappedToken(pair.token0)
    const currency1 = showUnwrapped ? pair.token1 : unwrappedToken(pair.token1)

    const [showMore, setShowMore] = useState(false)

    const userPoolBalance = useTokenBalance(account ?? undefined, pair.liquidityToken)
    const totalPoolTokens = useTotalSupply(pair.liquidityToken)

    const poolTokenPercentage =
        !!userPoolBalance && !!totalPoolTokens && JSBI.greaterThanOrEqual(totalPoolTokens.raw, userPoolBalance.raw)
            ? new Percent(userPoolBalance.raw, totalPoolTokens.raw)
            : undefined

    const [token0Deposited, token1Deposited] =
        !!pair &&
            !!totalPoolTokens &&
            !!userPoolBalance &&
            // this condition is a short-circuit in the case where useTokenBalance updates sooner than useTotalSupply
            JSBI.greaterThanOrEqual(totalPoolTokens.raw, userPoolBalance.raw)
            ? [
                pair.getLiquidityValue(pair.token0, totalPoolTokens, userPoolBalance, false),
                pair.getLiquidityValue(pair.token1, totalPoolTokens, userPoolBalance, false)
            ]
            : [undefined, undefined]

    return (
        <>
            {userPoolBalance && JSBI.greaterThan(userPoolBalance.raw, JSBI.BigInt(0)) ? (
                <div className="grid grid-cols-1 gap-3 bg-black p-5 bg-opacity-30 w-full mt-4 whitespace-nowrap text-sm">
                    <div className="flex justify-between">
                        <div className="text-white">{i18n._(t`My Pool Tokens`)}</div>
                        <div className="text-white font-bold">
                            {userPoolBalance ? userPoolBalance.toSignificant(4) : '-'}
                        </div>
                    </div>
                    <div className="flex justify-between">
                        <div className="text-white">{i18n._(t`Pooled ${currency0.getSymbol(chainId)}`)}</div>
                        <div className="text-white font-bold">{token0Deposited?.toSignificant(6)}</div>
                    </div>
                    <div className="flex justify-between">
                        <div className="text-white">{i18n._(t`My Pool Share`)}</div>
                        <div className="text-white font-bold">
                            {poolTokenPercentage ? poolTokenPercentage.toFixed(6) + '%' : '-'}
                        </div>
                    </div>
                    <div className="flex justify-between">
                        <div className="text-white">{i18n._(t`Pooled ${currency1.getSymbol(chainId)}`)}</div>
                        <div className="text-white font-bold">{token1Deposited?.toSignificant(6)}</div>
                    </div>
                </div>
            ) : (
                <div className="bg-purple rounded p-4 bg-opacity-20 w-full mt-4">
                    <p>
                        <span role="img" aria-label="wizard-icon">
                            ⭐️
                        </span>{' '}
                        {t`By adding liquidity you'll earn 0.25% of all trades on this pair proportional to your share
                        of the pool. Fees are added to the pool, accrue in real time and can be claimed by withdrawing
                        your liquidity.`}
                    </p>
                </div>
            )}
        </>
    )
}

export default function FullPositionCard({ pair, border, stakedBalance }: PositionCardProps) {
    const { i18n } = useLingui()
    const { account, chainId } = useActiveWeb3React()

    const currency0 = unwrappedToken(pair.token0)
    const currency1 = unwrappedToken(pair.token1)

    const [showMore, setShowMore] = useState(false)

    const userDefaultPoolBalance = useTokenBalance(account ?? undefined, pair.liquidityToken)
    const totalPoolTokens = useTotalSupply(pair.liquidityToken)

    // if staked balance balance provided, add to standard liquidity amount
    const userPoolBalance = stakedBalance ? userDefaultPoolBalance?.add(stakedBalance) : userDefaultPoolBalance

    const poolTokenPercentage =
        !!userPoolBalance && !!totalPoolTokens && JSBI.greaterThanOrEqual(totalPoolTokens.raw, userPoolBalance.raw)
            ? new Percent(userPoolBalance.raw, totalPoolTokens.raw)
            : undefined

    const [token0Deposited, token1Deposited] =
        !!pair &&
            !!totalPoolTokens &&
            !!userPoolBalance &&
            // this condition is a short-circuit in the case where useTokenBalance updates sooner than useTotalSupply
            JSBI.greaterThanOrEqual(totalPoolTokens.raw, userPoolBalance.raw)
            ? [
                pair.getLiquidityValue(pair.token0, totalPoolTokens, userPoolBalance, false),
                pair.getLiquidityValue(pair.token1, totalPoolTokens, userPoolBalance, false)
            ]
            : [undefined, undefined]

    const backgroundColor = useColor(pair?.token0, chainId)
    const theme = useContext(ThemeContext)


    return (
        <div className="relative bg-black bg-opacity-60 rounded">
            <StyledPositionCard border={border} bgColor={backgroundColor} className="rounded-none bg-transparent px-4 md:px-10 py-8">
                <AutoColumn gap="12px">
                    <FixedHeightRow>
                        <AutoRow gap="8px">
                            <DoubleCurrencyLogo currency0={currency0} currency1={currency1} size={30} />
                            <Text className="text-white text-base md:text-2xl">
                                {!currency0 || !currency1 ? (
                                    <Dots>{i18n._(t`Loading`)}</Dots>
                                ) : (
                                    `${currency0.getSymbol(chainId)} - ${currency1.getSymbol(chainId)}`
                                )}
                            </Text>
                        </AutoRow>
                        <RowFixed gap="8px">
                            <ButtonEmpty
                                padding="6px 8px"
                                borderRadius="20px"
                                width="fit-content"
                                className="text-sm text-white font-normal"
                                onClick={() => setShowMore(!showMore)}
                            >
                                <span style={{ whiteSpace: 'nowrap' }}>{i18n._(t`Manage`)}</span>
                                {showMore ? (
                                    <ChevronUp size="32" style={{ marginLeft: '10px' }} />
                                ) : (
                                    <ChevronDown size="32" style={{ marginLeft: '10px' }} />
                                )}
                            </ButtonEmpty>
                        </RowFixed>
                    </FixedHeightRow>

                    {showMore && (
                        <AutoColumn gap="8px" style={{ marginTop: 12 }}>
                            <FixedHeightRow>
                                <Text fontSize={16} fontWeight={400} color={theme.text3}>
                                    {i18n._(t`My pool tokens`)}
                                </Text>
                                <Text fontSize={16} color={theme.white}>
                                    {userPoolBalance ? userPoolBalance.toSignificant(4) + ' MP' : '-'}
                                </Text>
                            </FixedHeightRow>
                            {stakedBalance && (
                                <FixedHeightRow>
                                    <Text fontSize={16} fontWeight={400} color={theme.text3}>
                                        {i18n._(t`Pool tokens in rewards pool`)}
                                    </Text>
                                    <Text fontSize={16} color={theme.white}>
                                        {stakedBalance.toSignificant(4)}
                                    </Text>
                                </FixedHeightRow>
                            )}
                            <FixedHeightRow>
                                <RowFixed>
                                    <Text fontSize={16} fontWeight={400} color={theme.text3}>
                                        {i18n._(t`Pooled ${currency0?.getSymbol(chainId)}`)}
                                    </Text>
                                </RowFixed>
                                {token0Deposited ? (
                                    <RowFixed>
                                        <Text fontSize={16} color={theme.white} marginLeft={'6px'}>
                                            {token0Deposited?.toSignificant(6)}
                                        </Text>
                                        <CurrencyLogo size="30px" style={{ marginLeft: '8px' }} currency={currency0} />
                                    </RowFixed>
                                ) : (
                                    '-'
                                )}
                            </FixedHeightRow>

                            <FixedHeightRow>
                                <RowFixed>
                                    <Text fontSize={16} fontWeight={400} color={theme.text3}>
                                        {i18n._(t`Pooled ${currency1?.getSymbol(chainId)}`)}
                                    </Text>
                                </RowFixed>
                                {token1Deposited ? (
                                    <RowFixed>
                                        <Text fontSize={16} color={theme.white} marginLeft={'6px'}>
                                            {token1Deposited?.toSignificant(6)}
                                        </Text>
                                        <CurrencyLogo size="30px" style={{ marginLeft: '8px' }} currency={currency1} />
                                    </RowFixed>
                                ) : (
                                    '-'
                                )}
                            </FixedHeightRow>

                            <FixedHeightRow style={{ marginBottom: 20 }}>
                                <Text fontSize={16} fontWeight={400} color={theme.text3}>
                                    {i18n._(t`My pool share`)}
                                </Text>
                                <Text fontSize={16} color={theme.white}>
                                    {poolTokenPercentage
                                        ? (poolTokenPercentage.toFixed(2) === '0.00'
                                            ? '<0.01'
                                            : poolTokenPercentage.toFixed(2)) + '%'
                                        : '-'}
                                </Text>
                            </FixedHeightRow>

                            <a
                                className="w-full flex items-center justify-center text-sm"
                                style={{ textAlign: 'center', color: '#50caff' }}
                                href={`${ANALYTICS_URL[chainId ?? ChainIdDefault]}/users/${account}`}
                                rel="noreferrer"
                                target="_blank"
                            >
                                {i18n._(t`View accrued fees and analytics`)}
                                <img src={IconChart} style={{ width: 30, height: 24, marginLeft: 5 }} />
                            </a>

                            {userDefaultPoolBalance && JSBI.greaterThan(userDefaultPoolBalance.raw, BIG_INT_ZERO) && (
                                <RowBetween marginTop="10px">
                                    <ButtonPrimary
                                        padding="8px"
                                        as={Link}
                                        to={`/add/${currencyId(currency0)}/${currencyId(currency1)}`}
                                        width="49%"
                                    >
                                        {i18n._(t`Add`)}
                                    </ButtonPrimary>
                                    <ButtonPrimary
                                        padding="8px"
                                        as={Link}
                                        width="49%"
                                        to={`/remove/${currencyId(currency0)}/${currencyId(currency1)}`}
                                    >
                                        {i18n._(t`Remove`)}
                                    </ButtonPrimary>
                                </RowBetween>
                            )}
                            {stakedBalance && JSBI.greaterThan(stakedBalance.raw, BIG_INT_ZERO) && (
                                <ButtonPrimary
                                    padding="8px"
                                    borderRadius="8px"
                                    as={Link}
                                    to={`/uni/${currencyId(currency0)}/${currencyId(currency1)}`}
                                    width="100%"
                                >
                                    {i18n._(t`Manage Liquidity in Rewards Pool`)}
                                </ButtonPrimary>
                            )}
                        </AutoColumn>
                    )}
                </AutoColumn>
            </StyledPositionCard>
        </div>
    )
}
