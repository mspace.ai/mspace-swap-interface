import React from 'react'
import Typography from './Typography'
import Close from '../assets/mspace/icon_close.svg'

function ModalHeader({
    title = undefined,
    onClose,
    className = ''
}: {
    title?: string
    className?: string
    onClose: () => void
}): JSX.Element {
    return (
        <div className={`relative mb-2 ${className}`}>
            {title && (
                <div className="font-bold text-white text-xl font-bold text-center">
                    {title}
                </div>
            )}
            <div
                className="absolute -top-1 right-0 cursor-pointer"
                onClick={onClose}
            >
                <img src={Close} alt="close" style={{ width: 34, height: 34, objectFit: 'contain' }} />
            </div>
        </div>
    )
}

export default ModalHeader
