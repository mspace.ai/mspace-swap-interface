import React, { useContext } from 'react'
import { Input as PercentInput } from '../PercentInput'
import { ThemeContext } from 'styled-components'
import { useLingui } from '@lingui/react'
import { t } from '@lingui/macro'

interface PercentInputPanelProps {
    value: string
    onUserInput: (value: string) => void
    id: string
}

export default function PercentInputPanel({ value, onUserInput, id }: PercentInputPanelProps) {
    const theme = useContext(ThemeContext)
    const { i18n } = useLingui()
    return (
        <>
            <div id={id} style={{
                borderStyle: 'solid',
                borderWidth: 2,
                borderImageSource: 'linear-gradient(97deg, #92009d 3%, #0089ff 51%, #00a482 96%)',
                borderImageSlice: 1,
                backgroundColor: '#0f0a22'

            }}>
                <div className="flex flex-row justify-between items-center">
                    <div className="text-white text-center w-2/5"
                        style={{ margin: 10, backgroundColor: '#322f3f', borderRadius: 7, lineHeight: '36px', height: 40 }}>
                        <span className="font-bold text-sm">{i18n._(t`Amount to Remove`)}</span>
                    </div>
                    <div className="flex items-center bg-transparent font-bold text-2xl space-x-3 p-3 w-3/5">
                        <PercentInput
                            className="token-amount-input"
                            value={value}
                            onUserInput={val => {
                                onUserInput(val)
                            }}
                            align="right"
                        />
                        <div className="font-bold text-2xl text-white">%</div>
                    </div>
                </div>
            </div>
            <div className="flex flex-row justify-end">
                <div className="text-base font-bold cursor-pointer pl-6 underline hover:text-white" style={{ color: theme.text3 }}
                    onClick={() => onUserInput('25')}>25%</div>
                <div className="text-base font-bold cursor-pointer pl-6 underline hover:text-white" style={{ color: theme.text3 }}
                    onClick={() => onUserInput('50')}>50%</div>
                <div className="text-base font-bold cursor-pointer pl-6 underline hover:text-white" style={{ color: theme.text3 }}
                    onClick={() => onUserInput('75')}>75%</div>
                <div className="text-base font-bold cursor-pointer pl-6 underline hover:text-white" style={{ color: theme.text3 }}
                    onClick={() => onUserInput('100')}>100%</div>
            </div>
        </>
    )
}
