import React, { useCallback, useContext } from 'react';
import { MiniRpcProvider } from 'connectors/NetworkConnector';
import { MIGRATOR_ADDRESS } from 'constants/abis/migrator';
import styled from 'styled-components';
import { useActiveWeb3React } from 'hooks/useActiveWeb3React';
import { ThemeContext } from 'styled-components';
import { Plus } from 'react-feather';
import { MouseoverTooltip } from './Tooltip';

const Button = styled.button`
  margin-left: 8px;
  border-radius: 30px;
  border: 1px solid #FFF;
`

interface AddToMetamaskProps {
  token: any
}

export function AddToMetamask({ token }: AddToMetamaskProps) {

  const { chainId, connector } = useActiveWeb3React()
  const theme = useContext(ThemeContext)
  const isMetamask = window.ethereum && window.ethereum.isMetaMask

  const addTokenToMastermark = useCallback(async (e) => {
    e.stopPropagation();
    const provider: MiniRpcProvider = await connector?.getProvider();

    provider.request({
      method: 'wallet_watchAsset',
      params: {
        type: 'ERC20',
        options: {
          address: token.address,
          symbol: token.symbol,
          decimals: 18,
        }
      }
    }).then((success: any) => {
      if (success) {
        console.log('Successfully added token to wallet!');
      } else {
        throw new Error('Something went wrong.');
      }
    })
      .catch(console.error);
  }, [])

  return isMetamask && token.address !== MIGRATOR_ADDRESS && token.chainId === chainId ? (
    <MouseoverTooltip text="Add to Metamask">
      <Button onClick={addTokenToMastermark}>
        <Plus size="12" color={theme.white} />
      </Button>
    </MouseoverTooltip>
  ) : null;
};
