/* This example requires Tailwind CSS v2.0+ */
import React, { Fragment } from 'react'
import { Popover, Transition } from '@headlessui/react'
import { classNames } from '../functions/styling'
import { ExternalLink } from './Link'
import { ReactComponent as MenuIcon } from '../assets/images/menu.svg'
import { t } from '@lingui/macro'
import { I18n } from '@lingui/core'
import { useLingui } from '@lingui/react'
import { useToggleSettingsMenu, useWalletModalToggle } from '../state/application/hooks'
import { useActiveWeb3React } from '../hooks/useActiveWeb3React'
import { useMetaBalances } from '../state/wallet/hooks'
import { ChainId, Currency } from '../sdk'

// TODO change this
const items = (i18n: I18n) => [
    {
        name: i18n._(t`Docs`),
        description: i18n._(t`Documentation for users of Sushi.`),
        href: 'https://docs.sushi.com'
    },
    {
        name: i18n._(t`Dev`),
        description: i18n._(t`Documentation for developers of Sushi.`),
        href: 'https://dev.sushi.com'
    },
    {
        name: i18n._(t`Open Source`),
        description: i18n._(t`Sushi is a supporter of Open Source.`),
        href: 'https://github.com/sushiswap'
    },
    {
        name: i18n._(t`Tools`),
        description: i18n._(t`Tools to optimize your workflow.`),
        href: '/tools'
    },
    {
        name: i18n._(t`Discord`),
        description: i18n._(t`Join the community on Discord.`),
        href: 'https://discord.gg/NVPXN4e'
    }
]

export default function Menu() {
    const { i18n } = useLingui()
    const solutions = items(i18n)
    const toggleWalletModal = useWalletModalToggle()
    const toggleSettings = useToggleSettingsMenu()
    const { account, chainId, library } = useActiveWeb3React()
    const userMetaBalance = useMetaBalances(account ? [account] : [])?.[account ?? '']

    return (
        <Popover className="relative">
            {({ open }) => (
                <>
                    <Popover.Button
                        className={classNames(open ? 'text-secondary' : 'text-primary', 'focus:outline-none')}
                    >
                        <div className="ml-2">
                            <MenuIcon
                                title="More"
                                className={classNames(
                                    open ? 'text-white' : 'text-white',
                                    'inline-flex items-center h-6 w-6 group-hover:text-secondary hover:text-high-emphesis transform rotate-90'
                                )}
                                aria-hidden="true"
                            />
                        </div>
                    </Popover.Button>

                    <Transition
                        show={open}
                        as={Fragment}
                        enter="transition ease-out duration-200"
                        enterFrom="opacity-0 translate-y-1"
                        enterTo="opacity-100 translate-y-0"
                        leave="transition ease-in duration-150"
                        leaveFrom="opacity-100 translate-y-0"
                        leaveTo="opacity-0 translate-y-1"
                    >
                        <Popover.Panel
                            static
                            className="absolute z-10 top-8 left-full transform -translate-x-full mt-3 w-56 max-w-xs"
                        >
                            <div className="rounded-lg shadow-lg ring-1 ring-black ring-opacity-5 overflow-hidden">
                                <div className="relative grid bg-black">
                                    {/* {solutions.map(item => (
                                        <ExternalLink
                                            key={item.name}
                                            href={item.href}
                                            className="-m-3 p-3 block rounded-md hover:bg-dark-800 transition ease-in-out duration-150"
                                        >
                                            <p className="text-base font-medium text-high-emphesis">{item.name}</p>
                                            <p className="mt-1 text-sm text-secondary">{item.description}</p>
                                        </ExternalLink>
                                    ))} */}
                                    {account && chainId && userMetaBalance && (
                                        <>
                                            <div className="p-3 hover:bg-dark-800 transition ease-in-out duration-150 flex justify-end cursor-pointer font-bold text-white">
                                                {userMetaBalance?.toSignificant(4)}{' '}
                                                {Currency.getNativeCurrencySymbol(chainId)}
                                            </div>
                                        </>
                                    )}
                                    {account && (
                                        <div className="p-3 hover:bg-dark-800 transition ease-in-out duration-150 flex justify-end cursor-pointer font-bold text-white"
                                            onClick={toggleWalletModal}>
                                            Account
                                        </div>
                                    )}

                                    <div className="p-3 hover:bg-dark-800 transition ease-in-out duration-150 flex justify-end cursor-pointer font-bold text-white"
                                        onClick={toggleSettings}>
                                        Settings
                                    </div>
                                    <ExternalLink
                                        href={'/'}
                                        className="p-3 hover:bg-dark-800 transition ease-in-out duration-150 flex justify-end cursor-pointer font-bold"
                                    >
                                        <span className="text-white">Bridge</span>
                                    </ExternalLink>
                                </div>
                            </div>
                        </Popover.Panel>
                    </Transition>
                </>
            )}
        </Popover>
    )
}
