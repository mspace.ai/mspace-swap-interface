import React from 'react'
import { currencyEquals, META, WMETA, Currency } from '../../sdk'
import { AutoColumn } from '../../components/Column'
import CurrencyLogo from '../../components/CurrencyLogo'
import { AutoRow, RowBetween } from '../../components/Row'
import { useActiveWeb3React } from '../../hooks/useActiveWeb3React'
import { StyledInternalLink, TYPE } from '../../theme'
import { currencyId } from '../../utils/currencyId'
import { useLingui } from '@lingui/react'
import { t } from '@lingui/macro'

interface RemoveLiquidityReceiveDetailsProps {
    currencyA?: Currency
    amountA: string
    currencyB?: Currency
    amountB: string
    hasWMETA: Boolean
    hasMETA: Boolean
    id: string
}

export default function RemoveLiquidityReceiveDetails({
    currencyA,
    amountA,
    currencyB,
    amountB,
    hasWMETA,
    hasMETA,
    id
}: RemoveLiquidityReceiveDetailsProps) {
    const { chainId } = useActiveWeb3React()
    const { i18n } = useLingui()
    // if (!chainId || !currencyA || !currencyB) throw new Error('missing dependencies')
    return (
        <div id={id}>
            <div className="flex flex-col space-y-3 justify-between">
                <div className="w-full text-white" style={{ margin: 'auto 0px' }}>
                    <AutoColumn>
                        <div className="text-xl font-bold">{i18n._(t`You Will Receive`)}</div>
                        {/* <RowBetween>
                            {hasWMETA ? (
                                <StyledInternalLink
                                    className="text-base font-bold" style={{ color: '#50caff' }}
                                    to={`/remove/${currencyA === META ? WMETA[chainId].address : currencyId(currencyA)
                                        }/${currencyB === META ? WMETA[chainId].address : currencyId(currencyB)}`}
                                >
                                    Receive W{Currency.getNativeCurrencySymbol(chainId)}
                                </StyledInternalLink>
                            ) : hasMETA ? (
                                <StyledInternalLink
                                    className="text-base font-bold" style={{ color: '#50caff' }}
                                    to={`/remove/${currencyA && currencyEquals(currencyA, WMETA[chainId])
                                        ? 'META'
                                        : currencyId(currencyA)
                                        }/${currencyB && currencyEquals(currencyB, WMETA[chainId])
                                            ? 'META'
                                            : currencyId(currencyB)
                                        }`}
                                >
                                    Receive {Currency.getNativeCurrencySymbol(chainId)}
                                </StyledInternalLink>
                            ) : null}
                        </RowBetween> */}
                    </AutoColumn>
                </div>
                {/* <RowBetween className="space-x-6"> */}
                <div className="flex flex-col space-y-1 font-black text-2xl text-white">
                    <div className="flex flex-row items-center w-full justify-end">
                        <div>{amountA}</div>
                        <CurrencyLogo currency={currencyA} size="32px" style={{ margin: '0 12px' }} />
                        <div>{currencyA?.getSymbol(chainId)}</div>
                    </div>
                    <div className="flex flex-row items-center w-full justify-end">
                        <div>{amountB}</div>
                        <CurrencyLogo currency={currencyB} size="32px" style={{ margin: '0 12px' }} />
                        <div>{currencyB?.getSymbol(chainId)}</div>
                    </div>
                </div>
                {/* </RowBetween> */}
            </div>
        </div>
    )
}
