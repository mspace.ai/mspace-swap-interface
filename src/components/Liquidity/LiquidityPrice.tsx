import React, { useContext, useState } from 'react'
import { Price, Currency } from '../../sdk'
import { AutoRow } from '../../components/Row'
import { useActiveWeb3React } from '../../hooks/useActiveWeb3React'
import { ThemeContext } from 'styled-components'
import { Repeat } from 'react-feather'
import { useLingui } from '@lingui/react'
import { t } from '@lingui/macro'

export default function LiquidityPrice({
    input,
    output,
    price
}: {
    input?: Currency
    output?: Currency
    price?: Price
}): JSX.Element {
    const { i18n } = useLingui()
    const { chainId } = useActiveWeb3React()
    const theme = useContext(ThemeContext)
    const [showInverted, setShowInverted] = useState(false)

    const formattedPrice = showInverted ? price?.toSignificant(6) : price?.invert()?.toSignificant(6)
    const label = showInverted
        ? <div><span>{price?.quoteCurrency?.getSymbol(chainId)}</span> / {price?.baseCurrency?.getSymbol(chainId)}</div>
        : <div><span>{price?.baseCurrency?.getSymbol(chainId)}</span> / {price?.quoteCurrency?.getSymbol(chainId)}</div>

    return (
        <div className="pb-1">
            <AutoRow
                justify={'flex-end'}
            >
                <div className="font-sm text-white">{i18n._(t`Current Rate`)}</div>
                <div className="font-sm flex flex-row" style={{ color: theme.text3, marginLeft: 5 }}>{formattedPrice ?? '-'} &nbsp;{label}</div>
                {/* <div className="font-sm" style={{ color: theme.text3, marginLeft: 5 }}>
                    {price?.toSignificant(6) ?? '-'} {output?.getSymbol(chainId)} per {input?.getSymbol(chainId)}
                </div> */}
                <div style={{ marginLeft: 6 }} className="cursor-pointer" onClick={() => setShowInverted(!showInverted)}>
                    <Repeat size={14} color={'white'} />
                </div>
            </AutoRow>
        </div>
    )
}
