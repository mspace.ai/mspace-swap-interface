import { ChainId } from '../sdk'
import Metadium from '../assets/networks/metadium.png'

export const NETWORK_ICON = {
    [ChainId.METADIUM]: Metadium,
    [ChainId.META_TESTNET]: Metadium
}

export const NETWORK_LABEL: { [chainId in ChainId]?: string } = {
    [ChainId.METADIUM]: 'OKExChain',
    [ChainId.META_TESTNET]: 'Metadium Testnet'
}
