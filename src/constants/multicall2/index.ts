import { ChainId } from '../../sdk'
import MULTICALL2_ABI from './abi.json'
import { METADIUM_CONTRACTS, METATESTNET_CONTRACTS } from '../../constants-metadium'

const MULTICALL2_NETWORKS: { [chainId in ChainId]: string } = {
    [ChainId.METADIUM]: METADIUM_CONTRACTS.Multicall2,
    [ChainId.META_TESTNET]: METATESTNET_CONTRACTS.Multicall2
}

export { MULTICALL2_ABI, MULTICALL2_NETWORKS }
