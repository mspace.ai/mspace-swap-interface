import { JSBI, Percent, Token, WMETA, ChainId } from '../sdk'
import { fortmatic, injected, lattice, portis, torus, walletconnect, walletlink } from '../connectors'

import { AbstractConnector } from '@web3-react/abstract-connector'
import { METADIUM_CONTRACTS, METADIUM_TOKENS, METATESTNET_TOKENS, METATESTNET_CONTRACTS } from '../constants-metadium'

export const POOL_DENY = ['14', '29', '45', '30']

// a list of tokens by chain
type ChainTokenList = {
    readonly [chainId in ChainId]: Token[]
    // readonly [chainId: number]: Token[]
}

type ChainTokenMap = {
    readonly [chainId in ChainId]?: Token
}

// Block time here is slightly higher (~1s) than average in order to avoid ongoing proposals past the displayed time
export const AVERAGE_BLOCK_TIME_IN_SECS = 13
export const PROPOSAL_LENGTH_IN_BLOCKS = 40_320
export const PROPOSAL_LENGTH_IN_SECS = AVERAGE_BLOCK_TIME_IN_SECS * PROPOSAL_LENGTH_IN_BLOCKS

export const MSP: ChainTokenMap = {
    [ChainId.METADIUM]: new Token(ChainId.METADIUM, METADIUM_CONTRACTS.MSpace, 18, 'MSP', 'MSP Token'),
    [ChainId.META_TESTNET]: new Token(ChainId.META_TESTNET, METATESTNET_CONTRACTS.MSpace, 18, "MSP", "MSP Token")
}

export const COMMON_CONTRACT_NAMES: { [address: string]: string } = {
    // [UNI_ADDRESS]: 'UNI',
    [METADIUM_CONTRACTS.Timelock]: 'Timelock',
    [METATESTNET_CONTRACTS.Timelock]: 'Timelock',
}

// TODO: SDK should have two maps, WMETA map and WNATIVE map.
const WRAPPED_NATIVE_ONLY: ChainTokenList = {
    [ChainId.METADIUM]: [WMETA[ChainId.METADIUM]],
    [ChainId.META_TESTNET]: [WMETA[ChainId.META_TESTNET]]
}

// Default Ethereum chain tokens
export const DAI: {[chainId in ChainId]: Token} = {
  [ChainId.METADIUM]: new Token(ChainId.METADIUM, METADIUM_TOKENS.DAI, 18, 'mDAI', 'Dai Stablecoin'),
  [ChainId.META_TESTNET]: new Token(ChainId.META_TESTNET, METATESTNET_TOKENS.DAI, 18, 'mDAI', 'Dai Stablecoin'),
}
export const USDC: {[chainId in ChainId]: Token} = {
  [ChainId.METADIUM]: new Token(ChainId.METADIUM, METADIUM_TOKENS.USDC, 6, 'mUSDC', 'USD Coin'),
  [ChainId.META_TESTNET]: new Token(ChainId.META_TESTNET, METATESTNET_TOKENS.USDC, 6, 'mUSDC', 'USD Coin')
}
// export const BTC: {[chainId in ChainId]: Token} = {
//   [ChainId.METADIUM]: new Token(ChainId.METADIUM, METADIUM_TOKENS.WBTC, 6, 'WBTC', 'Wrapped BTC'),
//   [ChainId.META_TESTNET]: new Token(ChainId.META_TESTNET, METATESTNET_TOKENS.WBTC, 6, 'WBTC', 'Wrapped BTC')
// }
// export const WETH: {[chainId in ChainId]: Token} = {
//   [ChainId.METADIUM]: new Token(ChainId.METADIUM, METADIUM_TOKENS.WETH, 18, 'WETH', 'Wrapped Ether'),
//   [ChainId.META_TESTNET]: new Token(ChainId.META_TESTNET, METATESTNET_TOKENS.WETH, 18, 'WETH', 'Wrapped Ether')
// }
export const BTC: {[chainId in ChainId]: Token} = {
  [ChainId.METADIUM]: new Token(ChainId.METADIUM, METADIUM_TOKENS.WBTC, 6, 'mWBTC', 'BTC'),
  [ChainId.META_TESTNET]: new Token(ChainId.META_TESTNET, METATESTNET_TOKENS.WBTC, 6, 'mWBTC', 'BTC')
}
export const WETH: {[chainId in ChainId]: Token} = {
  [ChainId.METADIUM]: new Token(ChainId.METADIUM, METADIUM_TOKENS.WETH, 18, 'mETH', 'Ether'),
  [ChainId.META_TESTNET]: new Token(ChainId.META_TESTNET, METATESTNET_TOKENS.WETH, 18, 'mETH', 'Ether')
}

// used to construct intermediary pairs for trading
export const BASES_TO_CHECK_TRADES_AGAINST: ChainTokenList = {
  // ...WRAPPED_NATIVE_ONLY,
  [ChainId.METADIUM]: [
    ...WRAPPED_NATIVE_ONLY[ChainId.METADIUM], 
    DAI[ChainId.METADIUM], USDC[ChainId.METADIUM], BTC[ChainId.METADIUM]
  ],
  [ChainId.META_TESTNET]: [
    ...WRAPPED_NATIVE_ONLY[ChainId.META_TESTNET], 
    DAI[ChainId.META_TESTNET], USDC[ChainId.META_TESTNET], BTC[ChainId.META_TESTNET]
  ]
}

const XMSP_MAINNET = new Token(ChainId.METADIUM, METADIUM_CONTRACTS.Bank, 18, 'xMSP', 'Bank')
export const XMSP: {[chainId in ChainId]?: Token} = {
    [ChainId.METADIUM]: XMSP_MAINNET,
    [ChainId.META_TESTNET]: new Token(ChainId.META_TESTNET, METATESTNET_CONTRACTS.Bank, 18, 'xMSP', 'Bank')
}

/**
 * Some tokens can only be swapped via certain pairs, so we override the list of bases that are considered for these
 * tokens.
 */
export const CUSTOM_BASES: { [chainId in ChainId]?: { [tokenAddress: string]: Token[] } } = {
    [ChainId.METADIUM]: {
        //
    }
}

// used for display in the default list when adding liquidity
export const SUGGESTED_BASES: ChainTokenList = {
  ...WRAPPED_NATIVE_ONLY,
  [ChainId.METADIUM]: [
    // ...WRAPPED_NATIVE_ONLY[ChainId.METADIUM], 
    DAI[ChainId.METADIUM], USDC[ChainId.METADIUM], BTC[ChainId.METADIUM]
  ],
  [ChainId.META_TESTNET]: [
    // ...WRAPPED_NATIVE_ONLY[ChainId.META_TESTNET],
    DAI[ChainId.META_TESTNET], USDC[ChainId.META_TESTNET], BTC[ChainId.META_TESTNET]
  ],
}

// used to construct the list of all pairs we consider by default in the frontend
export const BASES_TO_TRACK_LIQUIDITY_FOR: ChainTokenList = {
    ...WRAPPED_NATIVE_ONLY,
    [ChainId.METADIUM]: [...WRAPPED_NATIVE_ONLY[ChainId.METADIUM]],
    [ChainId.META_TESTNET]: [...WRAPPED_NATIVE_ONLY[ChainId.META_TESTNET]],
}

export const PINNED_PAIRS: { readonly [chainId in ChainId]?: [Token, Token][] } = {
  [ChainId.METADIUM]: [
    [MSP[ChainId.METADIUM] as Token, WMETA[ChainId.METADIUM]],
    [WETH[ChainId.METADIUM], WMETA[ChainId.METADIUM]],
    [USDC[ChainId.METADIUM], WMETA[ChainId.METADIUM]],
    [DAI[ChainId.METADIUM], WMETA[ChainId.METADIUM]],
    [BTC[ChainId.METADIUM], WMETA[ChainId.METADIUM]],
  ],
  [ChainId.META_TESTNET]: [
    [MSP[ChainId.META_TESTNET] as Token, WMETA[ChainId.META_TESTNET]],
    [WETH[ChainId.META_TESTNET], WMETA[ChainId.META_TESTNET]],
    [USDC[ChainId.META_TESTNET], WMETA[ChainId.META_TESTNET]],
    [DAI[ChainId.META_TESTNET], WMETA[ChainId.META_TESTNET]],
    [BTC[ChainId.META_TESTNET], WMETA[ChainId.META_TESTNET]],
  ]
}

export interface WalletInfo {
    connector?: AbstractConnector
    name: string
    iconName: string
    description: string
    href: string | null
    color: string
    primary?: true
    mobile?: true
    mobileOnly?: true
}

export const SUPPORTED_WALLETS: { [key: string]: WalletInfo } = {
    INJECTED: {
        connector: injected,
        name: 'Injected',
        iconName: 'arrow-right.svg',
        description: 'Injected web3 provider.',
        href: null,
        color: '#010101',
        primary: true
    },
    METAMASK: {
        connector: injected,
        name: 'MetaMask',
        iconName: 'metamask.png',
        description: 'Easy-to-use browser extension.',
        href: null,
        color: '#E8831D'
    },
    // WALLET_CONNECT: {
    //     connector: walletconnect,
    //     name: 'WalletConnect',
    //     iconName: 'walletConnectIcon.svg',
    //     description: 'Connect to Trust Wallet, Rainbow Wallet and more...',
    //     href: null,
    //     color: '#4196FC',
    //     mobile: true
    // },
    // LATTICE: {
    //     connector: lattice,
    //     name: 'Lattice',
    //     iconName: 'gridPlusWallet.png',
    //     description: 'Connect to GridPlus Wallet.',
    //     href: null,
    //     color: '#40a9ff',
    //     mobile: true
    // },
    // WALLET_LINK: {
    //     connector: walletlink,
    //     name: 'Coinbase Wallet',
    //     iconName: 'coinbaseWalletIcon.svg',
    //     description: 'Use Coinbase Wallet app on mobile device',
    //     href: null,
    //     color: '#315CF5'
    // },
    // COINBASE_LINK: {
    //     name: 'Open in Coinbase Wallet',
    //     iconName: 'coinbaseWalletIcon.svg',
    //     description: 'Open in Coinbase Wallet app.',
    //     href: 'https://go.cb-w.com',
    //     color: '#315CF5',
    //     mobile: true,
    //     mobileOnly: true
    // },
    // FORTMATIC: {
    //     connector: fortmatic,
    //     name: 'Fortmatic',
    //     iconName: 'fortmaticIcon.png',
    //     description: 'Login using Fortmatic hosted wallet',
    //     href: null,
    //     color: '#6748FF',
    //     mobile: true
    // },
    // Portis: {
    //     connector: portis,
    //     name: 'Portis',
    //     iconName: 'portisIcon.png',
    //     description: 'Login using Portis hosted wallet',
    //     href: null,
    //     color: '#4A6C9B',
    //     mobile: true
    // },
    // Torus: {
    //     connector: torus,
    //     name: 'Torus',
    //     iconName: 'torusIcon.png',
    //     description: 'Login using Torus hosted wallet',
    //     href: null,
    //     color: '#315CF5',
    //     mobile: true
    // }
}

export const NetworkContextName = 'NETWORK'

// default allowed slippage, in bips
export const INITIAL_ALLOWED_SLIPPAGE = 50
// 20 minutes, denominated in seconds
export const DEFAULT_DEADLINE_FROM_NOW = 60 * 20

// used for rewards deadlines
export const BIG_INT_SECONDS_IN_WEEK = JSBI.BigInt(60 * 60 * 24 * 7)

export const BIG_INT_ZERO = JSBI.BigInt(0)

// one basis point
export const ONE_BIPS = new Percent(JSBI.BigInt(1), JSBI.BigInt(10000))
export const BIPS_BASE = JSBI.BigInt(10000)
// used for warning states
export const ALLOWED_PRICE_IMPACT_LOW: Percent = new Percent(JSBI.BigInt(100), BIPS_BASE) // 1%
export const ALLOWED_PRICE_IMPACT_MEDIUM: Percent = new Percent(JSBI.BigInt(300), BIPS_BASE) // 3%
export const ALLOWED_PRICE_IMPACT_HIGH: Percent = new Percent(JSBI.BigInt(500), BIPS_BASE) // 5%
// if the price slippage exceeds this number, force the user to type 'confirm' to execute
export const PRICE_IMPACT_WITHOUT_FEE_CONFIRM_MIN: Percent = new Percent(JSBI.BigInt(1000), BIPS_BASE) // 10%
// for non expert mode disable swaps above this
export const BLOCKED_PRICE_IMPACT_NON_EXPERT: Percent = new Percent(JSBI.BigInt(1500), BIPS_BASE) // 15%

// used to ensure the user doesn't send so much META so they end up with <300
export const MIN_META: JSBI = JSBI.exponentiate(JSBI.BigInt(10), JSBI.BigInt(16)) // 300 META
export const BETTER_TRADE_LESS_HOPS_THRESHOLD = new Percent(JSBI.BigInt(200), JSBI.BigInt(1))

export const ZERO_PERCENT = new Percent('0')
export const ONE_HUNDRED_PERCENT = new Percent('1')

// SDN OFAC addresses
export const BLOCKED_ADDRESSES: string[] = [
    // '0x7F367cC41522cE07553e823bf3be79A889DEbe1B',
    // '0xd882cFc20F52f2599D84b8e8D58C7FB62cfE344b',
    // '0x901bb9583b24D97e995513C6778dc6888AB6870e',
    // '0xA7e5d5A720f06526557c513402f2e6B5fA20b008'
]

// Boring Helper
export const ANALYTICS_URL: { [chainId in ChainId]: string } = {
    [ChainId.METADIUM]: 'https://beta.analytics.mspace.ai',
    [ChainId.META_TESTNET]: 'https://analytics.gocheetah.co',
}
