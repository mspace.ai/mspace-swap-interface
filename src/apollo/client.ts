import { ApolloClient, InMemoryCache, createHttpLink, NormalizedCacheObject } from '@apollo/client'
import { ChainId, ChainIdDefault } from '../sdk'
import { graphAPIEndpoints } from "../subgraph/constants"

// export const miningClientMainnet = new ApolloClient({
//     link: createHttpLink({
//         uri: graphAPIEndpoints.mining[11]
//     }),
//     cache: new InMemoryCache()
// });
export const miningClientMap: { readonly [chainId in ChainId]: ApolloClient<NormalizedCacheObject> } = {
    [ChainId.METADIUM]: new ApolloClient({
        link: createHttpLink({
            uri: graphAPIEndpoints.mining[11]
        }),
        cache: new InMemoryCache()
    }),
    [ChainId.META_TESTNET]: new ApolloClient({
        link: createHttpLink({
            uri: graphAPIEndpoints.mining[12]
        }),
        cache: new InMemoryCache()
    })
}
export const miningClientDefault = miningClientMap[ChainIdDefault]

// export const exchangeClientMainnet = new ApolloClient({
//     link: createHttpLink({
//         uri: graphAPIEndpoints.exchange[11]
//     }),
//     cache: new InMemoryCache()
// })
export const exchangeClientMap: { readonly [chainId in ChainId]: ApolloClient<NormalizedCacheObject> } = {
    [ChainId.METADIUM]: new ApolloClient({
        link: createHttpLink({
            uri: graphAPIEndpoints.exchange[11]
        }),
        cache: new InMemoryCache()
    }),
    [ChainId.META_TESTNET]: new ApolloClient({
        link: createHttpLink({
            uri: graphAPIEndpoints.exchange[12]
        }),
        cache: new InMemoryCache()
    })
}
export const exchangeClientDefault = exchangeClientMap[ChainIdDefault]

// export const healthClient = new ApolloClient({
//     link: createHttpLink({
//         uri: 'https://api.thegraph.com/index-node/graphql'
//     }),
//     cache: new InMemoryCache()
// })

export const blockClientMainnet = new ApolloClient({
    link: createHttpLink({
        uri: graphAPIEndpoints.blocklytics[11]
    }),
    cache: new InMemoryCache()
})
export const blockClientMap: {readonly [chainId in ChainId]: ApolloClient<NormalizedCacheObject>} = {
    [ChainId.METADIUM]: new ApolloClient({
        link: createHttpLink({
            uri: graphAPIEndpoints.blocklytics[11]
        }),
        cache: new InMemoryCache()
    }),
    [ChainId.META_TESTNET]: new ApolloClient({
        link: createHttpLink({
            uri: graphAPIEndpoints.blocklytics[12]
        }),
        cache: new InMemoryCache()
    })
}
export const blockClientDefault = blockClientMap[ChainIdDefault]
