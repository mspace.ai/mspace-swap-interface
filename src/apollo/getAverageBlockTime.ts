import { blockClientMap, blockClientDefault } from './client'
import { blockQuery, blocksQuery } from './queries'
import { getUnixTime, startOfHour, startOfMinute, startOfSecond, subDays, subHours } from 'date-fns'

import { ChainId, ChainIdDefault } from '../sdk'
import { useActiveWeb3React } from '../hooks/useActiveWeb3React'

export async function getOneDayBlock(chainId: ChainId = ChainIdDefault): Promise<{ number: number }> {
    const date = startOfMinute(subDays(Date.now(), 1))
    const start = Math.floor(Number(date) / 1000)
    const end = Math.floor(Number(date) / 1000) + 600
    const client = blockClientMap[chainId] ?? blockClientDefault

    const blocksData = await client.query({
        query: blockQuery,
        variables: {
            start,
            end
        },
        context: {
            clientName: 'blocklytics'
        },
        fetchPolicy: 'network-only'
    })

    return { number: Number(blocksData?.data?.blocks[0].number) }
}

export async function getAverageBlockTime(chainId: ChainId = ChainIdDefault): Promise<{ timestamp: null; difference: number }> {
    // Course timestamps used to make better use of the cache (startOfHour + startOfMinuite + startOfSecond)
    const now = startOfSecond(startOfMinute(startOfHour(Date.now())))
    const start = getUnixTime(subHours(now, 6))
    const end = getUnixTime(now)
    const client = blockClientMap[chainId] ?? blockClientDefault

    let query = await client.query({
        query: blocksQuery,
        variables: {
            start,
            end
        }
    })
    const blocks = query?.data.blocks

    const averageBlockTime = blocks.reduce(
        (previousValue: any, currentValue: any, currentIndex: any) => {
            if (previousValue.timestamp) {
                const difference = previousValue.timestamp - currentValue.timestamp
                previousValue.difference = previousValue.difference + difference
            }
            previousValue.timestamp = currentValue.timestamp
            if (currentIndex === blocks.length - 1) {
                return previousValue.difference / blocks.length
            }
            return previousValue
        },
        { timestamp: null, difference: 0 }
    )
    return averageBlockTime
}
