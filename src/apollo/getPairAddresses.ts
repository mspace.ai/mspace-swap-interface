import { exchangeClientMap, exchangeClientDefault } from './client'
import { ChainId, ChainIdDefault } from '../sdk'
import gql from 'graphql-tag'

export class PairAddressCache {
  public static CACHE: { [chainId: number]: { [address: string]: {[address: string]: string|undefined} } } = {}
  public static IS_CACHED: { [chainId: number]: { [address: string]: {[address: string]: boolean} } } = {}
  private static _initPathIfNotExists(map: any, key1:any, key2: any) {
    if (map[key1] === undefined) {
      map[key1] = {}
    }
    if (map[key1][key2] === undefined) {
      map[key1][key2] = {}
    }
  }
  public static isCached(chainId:number, token0: string, token1: string): boolean {
    PairAddressCache._initPathIfNotExists(PairAddressCache.IS_CACHED, chainId, token0)
    return PairAddressCache.IS_CACHED[chainId][token0][token1] === true
  }
  public static set(chainId:number, token0: string|undefined, token1: string|undefined, address: string|undefined) {
    if (token0 !== undefined && token1 !== undefined) {
      PairAddressCache._initPathIfNotExists(PairAddressCache.CACHE, chainId, token0)
      PairAddressCache.CACHE[chainId][token0][token1] = address
      PairAddressCache._initPathIfNotExists(PairAddressCache.CACHE, chainId, token1)
      PairAddressCache.CACHE[chainId][token1][token0] = address
    }
  }
  public static get(chainId:number, token0: string, token1: string): string|undefined {
    PairAddressCache._initPathIfNotExists(PairAddressCache.CACHE, chainId, token0)
    return PairAddressCache.CACHE[chainId][token0][token1]
  }
}

export type PairAddressResult = {
  tokens: [string|undefined, string|undefined], 
  pairAddress: string|undefined
}

export async function getPairAddresses(
    pairTokens: [string | undefined, string | undefined][],
    chainId: ChainId = ChainIdDefault
): 
  Promise<PairAddressResult[]> {
  
  const results: PairAddressResult[] = []
  const inputs: [string, string, number][] = []
  pairTokens.map(([token0, token1], i) => {
    const result: PairAddressResult = {
      tokens: [token0, token1], 
      pairAddress: token0 !== undefined && token1 !== undefined
        ? PairAddressCache.get(chainId, token0, token1) : undefined
    }
    if (
      token0 !== undefined && token1 !== undefined 
      && !PairAddressCache.isCached(chainId, token0, token1)
    ) {
      const token0Id = token0.toLowerCase()
      const token1Id = token1.toLowerCase()
      inputs.push(token0Id < token1Id ? [token0Id, token1Id, i]:[token1Id, token0Id, i])
    }
    results.push(result);
  })

  if (inputs.length > 0) {
    const client = exchangeClientMap[chainId] ?? exchangeClientDefault
    const pairResults = await client.query({
      query: gql`{
        ${inputs.map(([token0, token1, index]) => (`
          pair${index}: pairs(
            where: {
              token0: "${token0}", token1: "${token1}"
            }
          ) {
            id, token0 {id}, token1 {id}
          }`))}
      }`
    });
    Object.keys(pairResults.data).forEach(key => {
      const i = parseInt(key.replace("pair", ""))
      results[i].pairAddress = pairResults.data[key].length === 1 ? pairResults.data[key][0].id:undefined;
      PairAddressCache.set(chainId, results[i].tokens[0], results[i].tokens[1], results[i].pairAddress)
    });
  }
  return results;
}