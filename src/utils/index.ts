import { CurrencyAmount, META, WMETA, JSBI, Percent, Token, Currency, ChainId, ROUTER_ADDRESS, DEFAULT_CHAIN_ID } from '../sdk'
import { JsonRpcSigner, Web3Provider } from '@ethersproject/providers'

import { AddressZero } from '@ethersproject/constants'
import { BigNumber } from '@ethersproject/bignumber'
import { Contract } from '@ethersproject/contracts'
import Fraction from 'entities/Fraction'
import UniswapV2Router02Abi from '../constants/abis/router.json'
import Numeral from 'numeral'
import { TokenAddressMap } from '../state/lists/hooks'
import { ethers } from 'ethers'
import { getAddress } from '@ethersproject/address'

export const formatFromBalance = (value: BigNumber | undefined, decimals = 18): string => {
    if (value) {
        return Fraction.from(BigNumber.from(value), BigNumber.from(10).pow(decimals)).toString()
    } else {
        return ''
    }
}
export const formatToBalance = (value: string | undefined, decimals = 18) => {
    if (value) {
        return { value: ethers.utils.parseUnits(Number(value).toFixed(decimals), decimals), decimals: decimals }
    } else {
        return { value: BigNumber.from(0), decimals: decimals }
    }
}

export const fixedFromBalance = (value: BigNumber | undefined, decimals = 18): string => {
    if (value) {
        return Number(parseFloat(Fraction.from(BigNumber.from(value), BigNumber.from(10).pow(decimals)).toString()).toFixed(4)).toLocaleString()
    }
    return ''
}

export function isWMETA(value: any, chainId: ChainId | undefined): string {
    if (
      value.toLowerCase() === (
        chainId === undefined ? WMETA[DEFAULT_CHAIN_ID].address : 
        (WMETA[chainId] ? WMETA[DEFAULT_CHAIN_ID].address : WMETA[chainId].address)
      )
    ) {
        return 'META'
    }
    return value
}

export const formatBalance = (value: ethers.BigNumberish, decimals = 18, maxFraction = 0) => {
    const formatted = ethers.utils.formatUnits(value, decimals)
    if (maxFraction > 0) {
        const split = formatted.split('.')
        if (split.length > 1) {
            return split[0] + '.' + split[1].substr(0, maxFraction)
        }
    }
    return formatted
}

export const parseBalance = (value: string, decimals = 18) => {
    return ethers.utils.parseUnits(value || '0', decimals)
}

export const isEmptyValue = (text: string) =>
    ethers.BigNumber.isBigNumber(text)
        ? ethers.BigNumber.from(text).isZero()
        : text === '' || text.replace(/0/g, '').replace(/\./, '') === ''

// returns the checksummed address if the address is valid, otherwise returns false
export function isAddress(value: any): string | false {
    try {
        return getAddress(value)
    } catch {
        return false
    }
}
export function isAddressString(value: any): string {
    try {
        return getAddress(value)
    } catch {
        return ''
    }
}

// Vision Formatting
export const toK = (num: string) => {
    return Numeral(num).format('0.[00]a')
}

// using a currency library here in case we want to add more in future
const priceFormatter = new Intl.NumberFormat('en-US', {
    style: 'currency',
    currency: 'USD',
    minimumFractionDigits: 2
})

export const formattedNum = (number: any, usd = false) => {
    if (isNaN(number) || number === '' || number === undefined) {
        return usd ? '$0.00' : '0'
    }
    const num = parseFloat(number)

    if (num > 500000000) {
        return (usd ? '$' : '') + toK(num.toFixed(0))
    }

    if (num === 0) {
        if (usd) {
            return '$0.00'
        }
        return '0'
    }

    if (num < 0.0001 && num > 0) {
        return usd ? '< $0.0001' : '< 0.0001'
    }

    if (num > 1000) {
        return usd
            ? '$' + Number(parseFloat(String(num)).toFixed(0)).toLocaleString()
            : '' + Number(parseFloat(String(num)).toFixed(0)).toLocaleString()
    }

    if (usd) {
        if (num < 0.1) {
            return '$' + Number(parseFloat(String(num)).toFixed(4))
        } else {
            const usdString = priceFormatter.format(num)
            return '$' + usdString.slice(1, usdString.length)
        }
    }

    return parseFloat(String(num)).toPrecision(4)
}

export function gradientColor(percent: any) {
    percent = parseFloat(percent)
    if (percent < 100 && percent >= 90) {
        return '#ff3a31'
    }
    if (percent < 90 && percent >= 80) {
        return '#f85815'
    }
    if (percent < 80 && percent >= 70) {
        return '#ed7000'
    }
    if (percent < 70 && percent >= 60) {
        return '#de8400'
    }
    if (percent < 60 && percent >= 50) {
        return '#ce9700'
    }
    if (percent < 50 && percent >= 40) {
        return '#bba700'
    }
    if (percent < 40 && percent >= 30) {
        return '#a6b500'
    }
    if (percent < 30 && percent >= 20) {
        return '#8fc21b'
    }
    if (percent < 20 && percent >= 10) {
        return '#73ce42'
    }
    if (percent < 10 && percent >= 0) {
        return '#4ed864'
    }
    if (percent == 0) {
        return '#4ed864'
    }
    return '#ff3a31'
}

export function gradientColorAsc(percent: any) {
    percent = parseFloat(percent)
    if (percent < 10 && percent >= 0) {
        return '#ff3a31'
    }
    if (percent < 20 && percent >= 10) {
        return '#f85815'
    }
    if (percent < 30 && percent >= 20) {
        return '#ed7000'
    }
    if (percent < 40 && percent >= 30) {
        return '#de8400'
    }
    if (percent < 50 && percent >= 40) {
        return '#ce9700'
    }
    if (percent < 60 && percent >= 50) {
        return '#bba700'
    }
    if (percent < 70 && percent >= 60) {
        return '#a6b500'
    }
    if (percent < 80 && percent >= 70) {
        return '#8fc21b'
    }
    if (percent < 90 && percent >= 80) {
        return '#73ce42'
    }
    if (percent < 100 && percent >= 90) {
        return '#4ed864'
    }
    if (percent >= 100) {
        return '#4ed864'
    }
    return '#ff3a31'
}

export function formattedPercent(percentString: any) {
    const percent = parseFloat(percentString)

    if (!percent) {
        return '-'
    }

    if (percent === Infinity || percent === 0) {
        return '0%'
    }

    if (percent < 0.0001 && percent > 0) {
        return '< 0.0001%'
    }
    if (percent < 0 && percent > -0.0001) {
        return '< 0.0001%'
    }
    const fixedPercent = percent.toFixed(2)
    if (fixedPercent === '0.00') {
        return '0%'
    }
    if (Number(fixedPercent) > 0) {
        if (Number(fixedPercent) > 100) {
            return `${percent?.toFixed(0).toLocaleString()}%`
        } else {
            return `${fixedPercent}%`
        }
    } else {
        return `${fixedPercent}%`
    }
}

// Multichain Explorer
const builders = {
    metadium: (chainName = '', data: string, type: 'transaction' | 'token' | 'address' | 'block') => {
        const prefix = 'https://explorer.metadium.com'
        switch (type) {
            case 'transaction':
                return `${prefix}/tx/${data}`
            case 'token':
                return `${prefix}/address/${data}`
            default:
                return `${prefix}/${type}/${data}`
        }
    },
    metaTestnet: (chainName = '', data: string, type: 'transaction' | 'token' | 'address' | 'block') => {
        const prefix = 'https://testnetexplorer.metadium.com'
        switch (type) {
            case 'transaction':
                return `${prefix}/tx/${data}`
            case 'token':
                return `${prefix}/address/${data}`
            default:
                return `${prefix}/${type}/${data}`
        }
    }
}

interface ChainObject {
    [chainId: number]: {
        chainName: string
        builder: (chainName: string, data: string, type: 'transaction' | 'token' | 'address' | 'block') => string
    }
}

const chains: ChainObject = {
    [ChainId.METADIUM]: {
        chainName: '',
        builder: builders.metadium
    },
    [ChainId.META_TESTNET]: {
        chainName: 'testnet',
        builder: builders.metaTestnet
    }
}

export function getExplorerLink(
    chainId: ChainId,
    data: string,
    type: 'transaction' | 'token' | 'address' | 'block'
): string {
    const chain = chains[chainId]
    return chain.builder(chain.chainName, data, type)
}

// shorten the checksummed version of the input address to have 0x + 4 characters at start and end
export function shortenAddress(address: string, chars = 4): string {
    const parsed = isAddress(address)
    if (!parsed) {
        throw Error(`Invalid 'address' parameter '${address}'.`)
    }
    return `${parsed.substring(0, chars + 2)}...${parsed.substring(42 - chars)}`
}

// add 10%
export function calculateGasMargin(value: BigNumber): BigNumber {
    return value.mul(BigNumber.from(10000).add(BigNumber.from(1000))).div(BigNumber.from(10000))
}

// converts a basis points value to a sdk percent
export function basisPointsToPercent(num: number): Percent {
    return new Percent(JSBI.BigInt(num), JSBI.BigInt(10000))
}

export function calculateSlippageAmount(value: CurrencyAmount, slippage: number): [JSBI, JSBI] {
    if (slippage < 0 || slippage > 10000) {
        throw Error(`Unexpected slippage value: ${slippage}`)
    }
    return [
        JSBI.divide(JSBI.multiply(value.raw, JSBI.BigInt(10000 - slippage)), JSBI.BigInt(10000)),
        JSBI.divide(JSBI.multiply(value.raw, JSBI.BigInt(10000 + slippage)), JSBI.BigInt(10000))
    ]
}

// account is not optional
export function getSigner(library: Web3Provider, account: string): JsonRpcSigner {
    return library.getSigner(account).connectUnchecked()
}

// account is optional
export function getProviderOrSigner(library: Web3Provider, account?: string): Web3Provider | JsonRpcSigner {
    return account ? getSigner(library, account) : library
}

// account is optional
export function getContract(address: string, ABI: any, library: Web3Provider, account?: string): Contract {
    if (!isAddress(address) || address === AddressZero) {
        throw Error(`Invalid 'address' parameter '${address}'.`)
    }

    return new Contract(address, ABI, getProviderOrSigner(library, account) as any)
}

export function getRouterAddress(chainId?: ChainId) {
    if (!chainId) {
        throw Error(`Undefined 'chainId' parameter '${chainId}'.`)
    }
    return ROUTER_ADDRESS[chainId]
}

// account is optional
export function getRouterContract(chainId: number, library: Web3Provider, account?: string): Contract {
    return getContract(getRouterAddress(chainId), UniswapV2Router02Abi, library, account)
}

export function escapeRegExp(string: string): string {
    return string.replace(/[.*+?^${}()|[\]\\]/g, '\\$&') // $& means the whole matched string
}

export function isTokenOnList(defaultTokens: TokenAddressMap, currency?: Currency): boolean {
    if (currency === META) return true
    return Boolean(currency instanceof Token && defaultTokens[currency.chainId]?.[currency.address])
}

export function convertToInternationalCurrencySystem (number: any) {
    // Nine Zeroes for Billions
    return Math.abs(Number(number)) >= 1.0e+9

    ? (Math.abs(Number(number)) / 1.0e+9).toFixed(2) + "b"
    // Six Zeroes for Millions 
    : Math.abs(Number(number)) >= 1.0e+6

    ? (Math.abs(Number(number)) / 1.0e+6).toFixed(2) + "m"

    : Math.abs(Number(number));
}

export function formatSymbol(symbol: string) {
    const value = symbol !== 'WBTC' ? symbol.replace('W', '') : symbol
    return value !== 'META' && value !== 'MSP' ? `m${value}` : value
}
