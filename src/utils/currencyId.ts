import { META, Token, Currency } from '../sdk'

export function currencyId(currency: Currency): string {
    if (currency === META) return 'META'
    if (currency instanceof Token) return currency.address
    throw new Error('invalid currency')
}
