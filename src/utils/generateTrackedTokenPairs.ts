import { Pair, Token, ChainId } from '../sdk'
import { SerializedPair, SerializedToken } from '../state/user/actions'
import { BASES_TO_TRACK_LIQUIDITY_FOR, PINNED_PAIRS } from '../constants'
import flatMap from 'lodash.flatmap'

function deserializeToken(serializedToken: SerializedToken): Token {
  return new Token(
      serializedToken.chainId,
      serializedToken.address,
      serializedToken.decimals,
      serializedToken.symbol,
      serializedToken.name
  )
}

export function generateTrackedTokenPairs(
  chainId: ChainId | undefined,
  allTokens: {[address: string]: Token},
  savedSerializedPairs: {
    [chainId: number]: {
      [key: string]: SerializedPair
    }
  }
): [Token, Token][] {
    // pairs for every token against every base
    const generatedPairs: [Token, Token][] = chainId
          ? flatMap(Object.keys(allTokens), tokenAddress => {
                const token = allTokens[tokenAddress]
                // for each token on the current chain,
                return (
                    // loop though all bases on the current chain
                    (BASES_TO_TRACK_LIQUIDITY_FOR[chainId] ?? [])
                        // to construct pairs of the given token with each base
                        .map(base => {
                            if (base.address === token.address) {
                                return null
                            } else {
                                return [base, token]
                            }
                        })
                        .filter((p): p is [Token, Token] => p !== null)
                )
            }) : []
    const userPairs: [Token, Token][] = 
        !chainId 
            || !savedSerializedPairs 
            || !savedSerializedPairs[chainId]
        ? []
        : Object.keys(savedSerializedPairs[chainId]).map(pairId => {
            return [
                deserializeToken(savedSerializedPairs[chainId][pairId].token0), 
                deserializeToken(savedSerializedPairs[chainId][pairId].token1)
            ]
        })
    const pinnedPairs = chainId ? PINNED_PAIRS[chainId] ?? [] : []
    const combinedList =  userPairs.concat(generatedPairs).concat(pinnedPairs)
    const keyed = combinedList.reduce<{ [key: string]: [Token, Token] }>((memo, [tokenA, tokenB]) => {
        const sorted = tokenA.sortsBefore(tokenB)
        const key = sorted ? `${tokenA.address}:${tokenB.address}` : `${tokenB.address}:${tokenA.address}`
        if (memo[key]) return memo
        memo[key] = sorted ? [tokenA, tokenB] : [tokenB, tokenA]
        return memo
    }, {})

    return Object.keys(keyed).map(key => keyed[key])
}
