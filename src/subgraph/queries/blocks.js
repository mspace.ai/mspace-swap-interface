const ws = require('isomorphic-ws');
const { SubscriptionClient } = require('subscriptions-transport-ws');

const { request, gql } = require('graphql-request');

const { graphAPIEndpoints, graphWSEndpoints, DEFAULT_CHAIN_ID } = require('./../constants')
const { timestampToBlock } = require('./../utils')


export async function latestBlock({ chainId = DEFAULT_CHAIN_ID } = {}) {
  const result = await request(graphAPIEndpoints.blocklytics[chainId],
    gql`{
          blocks(first: 1, orderBy: number, orderDirection: desc) {
            ${_latestBlock.properties.toString()}
          }
        }`
  );

  return _latestBlock.callback(result.blocks);
}

export function observeLatestBlock({ chainId = DEFAULT_CHAIN_ID } = {}) {
  const query = gql`
    subscription {
      blocks(first: 1, orderBy: number, orderDirection: desc) {
        ${_latestBlock.properties.toString()}
      }
  }`;

  const client = new SubscriptionClient(graphWSEndpoints.blocklytics[chainId], { reconnect: true, }, ws,);
  const observable = client.request({ query });

  return {
    subscribe({ next, error, complete }) {
      return observable.subscribe({
        next(results) {
          next(_latestBlock.callback(results.data.blocks));
        },
        error,
        complete
      })
    }
  };
}

export async function getBlock({ block = undefined, timestamp = undefined, chainId = DEFAULT_CHAIN_ID } = {}) {
  block = block ? block : timestamp ? (await timestampToBlock({ timestamp: timestamp, chainId: chainId })) : undefined;
  block = block ? `block: { number: ${block} }` : "";

  const result = await request(graphAPIEndpoints.blocklytics[chainId],
    gql`{
          blocks(first: 1, orderBy: number, orderDirection: desc, ${block}) {
            ${_getBlock.properties.toString()}
          }
        }`
  );

  return _getBlock.callback(result.blocks[0]);
}


const _latestBlock = {
  properties: [
    'id',
    'number',
    'timestamp'
  ],

  callback([{ id, number, timestamp }]) {
    return ({
      id: id,
      number: Number(number),
      timestamp: Number(timestamp),
      date: new Date(timestamp * 1000)
    });
  }
};

const _getBlock = {
  properties: [
    'id',
    'number',
    'timestamp',
    'author',
    'difficulty',
    'gasUsed',
    'gasLimit'
  ],

  callback(results) {
    return ({
      id: results.id,
      number: Number(results.number),
      timestamp: Number(results.timestamp),
      author: results.author,
      difficulty: Number(results.difficulty),
      gasUsed: Number(results.gasUsed),
      gasLimit: Number(results.gasLimit)
    })
  }
}