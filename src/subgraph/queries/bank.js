const ws = require('isomorphic-ws');
const { SubscriptionClient } = require('subscriptions-transport-ws');

const { request, gql } = require('graphql-request');

const { graphAPIEndpoints, graphWSEndpoints, bankAddress, DEFAULT_CHAIN_ID } = require('./../constants')
const { timestampToBlock } = require('./../utils');


export async function info({ block = undefined, timestamp = undefined, chainId = DEFAULT_CHAIN_ID } = {}) {
  block = block ? block : timestamp ? (await timestampToBlock({ timestamp: timestamp, chainId: chainId })) : undefined;
  block = block ? `block: { number: ${block} }` : "";

  const result = await request(graphAPIEndpoints.bank[chainId],
    gql`{
          bank(id: "${bankAddress[chainId]}", ${block}) {
            ${_info.properties.toString()}
          }
        }`
  );

  return _info.callback(result.bank);
}

export function observeInfo({ chainId = DEFAULT_CHAIN_ID } = {}) {
  const query = gql`
    subscription {
      bank(id: "${bankAddress[chainId]}") {
        ${_info.properties.toString()}
      }
  }`;

  const client = new SubscriptionClient(graphWSEndpoints.bank[chainId], { reconnect: true, }, ws,);
  const observable = client.request({ query });

  return {
    subscribe({ next, error, complete }) {
      return observable.subscribe({
        next(results) {
          next(_info.callback(results.data.bank));
        },
        error,
        complete
      });
    }
  };
}

export async function user({ block = undefined, timestamp = undefined, user_address = undefined, chainId = DEFAULT_CHAIN_ID } = {}) {
  if (!user_address) { throw new Error("subgraph: User address undefined"); }

  block = block ? block : timestamp ? (await timestampToBlock({ timestamp: timestamp, chainId: chainId })) : undefined;
  block = block ? `block: { number: ${block} }` : "";

  const result = await request(graphAPIEndpoints.bank[chainId],
    gql`{
          user(id: "${user_address.toLowerCase()}", ${block}) {
            ${_user.properties.toString()}
          }
        }`
  );

  return _user.callback(result.user);
}


const _info = {
  properties: [
    'decimals',
    'name',
    'mspace',
    'symbol',
    'totalSupply',
    'ratio',
    'xMSpaceMinted',
    'xMSpaceBurned',
    'mspaceStaked',
    'mspaceStakedUSD',
    'mspaceHarvested',
    'mspaceHarvestedUSD',
    'xMSpaceAge',
    'xMSpaceAgeDestroyed',
    'updatedAt'
  ],

  callback(results) {
    return ({
      decimals: Number(results.decimals),
      name: results.name,
      mspace: results.mspace,
      symbol: results.symbol,
      totalSupply: Number(results.totalSupply),
      ratio: Number(results.ratio),
      xMSpaceMinted: Number(results.xMSpaceMinted),
      xMSpaceBurned: Number(results.xMSpaceBurned),
      mspaceStaked: Number(results.totalSupply) * Number(results.ratio),
      mspaceStakedUSD: Number(results.mspaceStakedUSD),
      mspaceHarvested: Number(results.mspaceHarvested),
      mspaceHarvestedUSD: Number(results.mspaceHarvestedUSD),
      xMSpaceAge: Number(results.xMSpaceAge),
      xMSpaceAgeDestroyed: Number(results.xMSpaceAgeDestroyed),
      updatedAt: Number(results.updatedAt)
    })
  }
};

const _user = {
  properties: [
    'xMSpace',
    'xMSpaceIn',
    'xMSpaceOut',
    'xMSpaceMinted',
    'xMSpaceBurned',
    'xMSpaceOffset',
    'xMSpaceAge',
    'xMSpaceAgeDestroyed',
    'mspaceStaked',
    'mspaceStakedUSD',
    'mspaceHarvested',
    'mspaceHarvestedUSD',
    'mspaceIn',
    'mspaceOffset',
    'usdOut',
    'usdIn',
    'updatedAt',
    'mspaceOffset',
    'usdOffset'
  ],

  callback(results) {
    return ({
      xMSpace: Number(results.xMSpace),
      xMSpaceIn: Number(results.xMSpaceIn),
      xMSpaceOut: Number(results.xMSpaceOut),
      xMSpaceMinted: Number(results.xMSpaceMinted),
      xMSpaceBurned: Number(results.xMSpaceBurned),
      xMSpaceOffset: Number(results.xMSpaceOffset),
      xMSpaceAge: Number(results.xMSpaceAge),
      xMSpaceAgeDestroyed: Number(results.xMSpaceAgeDestroyed),
      mspaceStaked: Number(results.mspaceStaked),
      mspaceStakedUSD: Number(results.mspaceStakedUSD),
      mspaceHarvested: Number(results.mspaceHarvested),
      mspaceHarvestedUSD: Number(results.mspaceHarvestedUSD),
      mspaceIn: Number(results.mspaceIn),
      mspaceOffset: Number(results.mspaceOffset),
      usdOut: Number(results.usdOut),
      usdIn: Number(results.usdIn),
      updatedAt: Number(results.updatedAt),
      mspaceOffset: Number(results.mspaceOffset),
      usdOffset: Number(results.usdOffset)
    })
  }
};