const pageResults = require('graph-results-pager');

const ws = require('isomorphic-ws');
const { SubscriptionClient } = require('subscriptions-transport-ws');

const { request, gql } = require('graphql-request');

const { graphAPIEndpoints, graphWSEndpoints, DEFAULT_CHAIN_ID } = require('./../constants')
const { timestampToBlock } = require('./../utils')


export async function user({ block = undefined, timestamp = undefined, user_address = undefined, chainId = DEFAULT_CHAIN_ID } = {}) {
  return pageResults({
    api: graphAPIEndpoints.lockup[chainId],
    query: {
      entity: 'users',
      selection: {
        where: {
          address: `\\"${user_address.toLowerCase()}\\"`
        },
      },
      block: block ? { number: block } : timestamp ? { number: await timestampToBlock({ timestamp: timestamp, chainId: chainId }) } : undefined,
      properties: _user.properties
    }
  })
    .then(results => _user.callback(results))
    .catch(err => console.log(err));
}


const _user = {
  properties: [
    'id',
    'address',
    'amount',
    'rewardDebt',
    'pool { id, balance, allocPoint, lastUpdate, totalShare }',
    'mspaceHarvestedSinceLockup',
  ],

  callback(results) {
    return results.map(entry => ({
      id: entry.id,
      address: entry.address,
      amount: Number(entry.amount),
      rewardDebt: BigInt(entry.rewardDebt),
      pool: {
        id: entry.pool.id,
        balance: Number(entry.pool.balance),
        allocPoint: BigInt(entry.pool.allocPoint),
        lastUpdate: BigInt(entry.pool.lastUpdate),
        totalShare: BigInt(entry.pool.totalShare),
      },
      mspaceHarvestedSinceLockup: Number(entry.mspaceHarvestedSinceLockup),
    }));
  }
};
