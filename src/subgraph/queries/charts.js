const {
  getWeek,
  subWeeks,
  subYears,
  startOfMinute,
  getUnixTime
} = require("date-fns");

const { TWENTY_FOUR_HOURS, DEFAULT_CHAIN_ID } = require('./../constants');
const { dayData, tokenHourData, tokenDayData, pairHourData, pairDayData, metaPriceHourly } = require('./exchange');


export async function factory({ chainId = DEFAULT_CHAIN_ID } = {}) {
  let data = await dayData({ chainId: chainId });
  let weeklyData = [];

  let startIndexWeekly = -1;
  let currentWeek = -1;

  data.forEach((entry, i) => {
    const week = getWeek(data[i].date)

    if (week !== currentWeek) {
      currentWeek = week;
      startIndexWeekly++;
    }

    weeklyData[startIndexWeekly] = weeklyData[startIndexWeekly] || {};
    weeklyData[startIndexWeekly].date = data[i].date;
    weeklyData[startIndexWeekly].weeklyVolumeUSD = ((
      weeklyData[startIndexWeekly].weeklyVolumeUSD ?
        weeklyData[startIndexWeekly].weeklyVolumeUSD : 0) + data[i].volumeUSD
    );
  });

  return [data, weeklyData];
}

export async function tokenHourly({ token_address = undefined, startTime = undefined, chainId = DEFAULT_CHAIN_ID }) {
  if (!token_address) { throw new Error("subgraph: Token address undefined"); }

  let [tokenData, metaPrices] = await Promise.all([
    tokenHourData({ minTimestamp: startTime, token_address: token_address, chainId: chainId }),
    metaPriceHourly({ minTimestamp: startTime, chainId: chainId })]);

  tokenData = tokenData.map(tokenEntry => {
    const metaPriceUSD = metaPrices.find(metaEntry => metaEntry.timestamp === tokenEntry.timestamp).priceUSD;
    return ({
      ...tokenEntry,
      priceUSD: tokenEntry.derivedMETA * metaPriceUSD,
    })
  });

  tokenData = tokenData.map((tokenEntry, i) => ({
    ...tokenEntry,
    volume: tokenData[i - 1] ? tokenEntry.volume - tokenData[i - 1].volume : undefined,
    volumeUSD: tokenData[i - 1] ? tokenEntry.volumeUSD - tokenData[i - 1].volumeUSD : undefined,
    untrackedVolumeUSD: tokenData[i - 1] ? tokenEntry.untrackedVolumeUSD - tokenData[i - 1].untrackedVolumeUSD : undefined,

    txCount: tokenData[i - 1] ? tokenEntry.txCount - tokenData[i - 1].txCount : undefined,

    open: tokenEntry.priceUSD,
    close: tokenData[i + 1] ? tokenData[i + 1].priceUSD : undefined,
  }));

  return tokenData;
}

export async function tokenDaily({ token_address = undefined, chainId = undefined } = {}) {
  chainId = chainId ? chainId : 11;
  if (!token_address) { throw new Error("subgraph: Token address undefined"); }

  let data = await tokenDayData({ token_address: token_address, chainId: chainId });
  const endTime = getUnixTime(new Date());
  const startTime = getUnixTime(startOfMinute(subYears(new Date(), 1)));

  let dayIndexSet = new Set();
  let dayIndexArray = [];

  data.forEach((dayData, i) => {
    // add the day index to the set of days
    dayIndexSet.add((data[i].timestamp / TWENTY_FOUR_HOURS).toFixed(0));
    dayIndexArray.push(data[i]);
  });

  // fill in empty days
  let timestamp = data[0] && data[0].timestamp ? data[0].timestamp : startTime;
  let latestLiquidity = data[0] && data[0].liquidity;
  let latestLiquidityUSD = data[0] && data[0].liquidityUSD;
  let latestLiquidityMETA = data[0] && data[0].liquidityMETA;
  let latestPriceUSD = data[0] && data[0].priceUSD;
  let index = 1;

  while (timestamp < endTime - TWENTY_FOUR_HOURS) {
    const nextDay = timestamp + TWENTY_FOUR_HOURS;
    let currentDayIndex = (nextDay / TWENTY_FOUR_HOURS).toFixed(0);
    if (!dayIndexSet.has(currentDayIndex)) {
      data.push({
        id: `${data[0].id.split("-")[0]}-${nextDay / TWENTY_FOUR_HOURS}`,
        date: new Date(nextDay * 1000),
        timestamp: nextDay,
        volume: 0,
        volumeMETA: 0,
        volumeUSD: 0,
        liquidity: latestLiquidity,
        liquidityMETA: latestLiquidityMETA,
        liquidityUSD: latestLiquidityUSD,
        priceUSD: latestPriceUSD,
        txCount: 0
      });
    } else {
      latestLiquidity = dayIndexArray[index].liquidity;
      latestLiquidityMETA = dayIndexArray[index].liquidityMETA;
      latestLiquidityUSD = dayIndexArray[index].liquidityUSD;

      latestPriceUSD = dayIndexArray[index].priceUSD;
      index = index + 1;
    }
    timestamp = nextDay;
  }

  data = data.sort((a, b) => (parseInt(a.timestamp) > parseInt(b.timestamp) ? 1 : -1));

  return data;
}

export async function pairHourly({ pair_address = undefined, startTime = undefined, chainId = undefined }) {
  chainId = chainId ? chainId : 11;
  if (!pair_address) { throw new Error("subgraph: Pair address undefined"); }

  let pairData = await pairHourData({ minTimestamp: startTime, pair_address: pair_address, chainId: chainId });

  pairData = pairData.map((pairEntry, i) => ({
    ...pairEntry,
    volumeToken0: pairData[i - 1] ? pairEntry.volumeToken0 - pairData[i - 1].volumeToken0 : undefined,
    volumeToken1: pairData[i - 1] ? pairEntry.volumeToken1 - pairData[i - 1].volumeToken1 : undefined,

    rate0: {
      open: pairEntry.token0Price,
      close: pairData[i + 1] ? pairData[i + 1].token0Price : undefined,
    },

    rate1: {
      open: pairEntry.token1Price,
      close: pairData[i + 1] ? pairData[i + 1].token1Price : undefined,
    },

    txCount: pairData[i - 1] ? pairEntry.txCount - pairData[i - 1].txCount : undefined,
  }));

  return pairData;
}

export async function pairDaily({ pair_address = undefined, chainId = undefined } = {}) {
  chainId = chainId ? chainId : 11;
  if (!pair_address) { throw new Error("subgraph: Pair address undefined"); }

  let data = await pairDayData({ pair_address: pair_address, chainId: chainId });
  const endTime = getUnixTime(new Date());
  const startTime = getUnixTime(startOfMinute(subYears(new Date(), 1)));

  let dayIndexSet = new Set();
  let dayIndexArray = [];

  data.forEach((dayData, i) => {
    // add the day index to the set of days
    dayIndexSet.add((data[i].timestamp / TWENTY_FOUR_HOURS).toFixed(0));
    dayIndexArray.push(data[i]);
  });

  let timestamp = data[0].timestamp ? data[0].timestamp : startTime;
  let latestLiquidityUSD = data[0].liquidityUSD;
  let index = 1;

  while (timestamp < endTime - TWENTY_FOUR_HOURS) {
    const nextDay = timestamp + TWENTY_FOUR_HOURS;
    let currentDayIndex = (nextDay / TWENTY_FOUR_HOURS).toFixed(0);
    if (!dayIndexSet.has(currentDayIndex)) {
      data.push({
        id: `${data[0].id.split("-")[0]}-${nextDay / TWENTY_FOUR_HOURS}`,
        date: new Date(nextDay * 1000),
        timestamp: nextDay,
        volumeUSD: 0,
        volumeToken0: 0,
        volumeToken1: 0,
        liquidityUSD: latestLiquidityUSD,
        txCount: 0
      });
    } else {
      latestLiquidityUSD = dayIndexArray[index].liquidityUSD;

      index = index + 1;
    }
    timestamp = nextDay;
  }

  data = data.sort((a, b) => (parseInt(a.timestamp) > parseInt(b.timestamp) ? 1 : -1));

  return data;
}