const pageResults = require('graph-results-pager');

const ws = require('isomorphic-ws');
const { SubscriptionClient } = require('subscriptions-transport-ws');

const { gql } = require('graphql-request');

const { graphAPIEndpoints, graphWSEndpoints, mspaceAddress, DEFAULT_CHAIN_ID } = require('./../constants')
const { timestampToBlock } = require('./../utils')

const { metaPrice } = require('./exchange')


export async function priceUSD({ block = undefined, timestamp = undefined, chainId = DEFAULT_CHAIN_ID } = {}) {
  return (await metaPrice({ block: block, timestamp: timestamp, chainId: chainId }) * (await priceMETA({ block: block, timestamp: timestamp, chainId: chainId })));
}

export async function priceMETA({ block = undefined, timestamp = undefined, chainId = DEFAULT_CHAIN_ID } = {}) {
  return pageResults({
    api: graphAPIEndpoints.exchange[chainId],
    query: {
      entity: 'tokens',
      selection: {
        where: {
          id: `\\"${mspaceAddress[chainId]}\\"`,
        },
        block: block ? { number: block } : timestamp ? { number: await timestampToBlock({ timestamp: timestamp, chainId: chainId }) } : undefined,
      },
      properties: _priceMETA.properties,
    }
  })
    .then(results => _priceMETA.callback(results[0]))
    .catch(err => console.error(err));
}

export function observePriceMETA({ chainId = DEFAULT_CHAIN_ID } = {}) {
  const query = gql`
      subscription {
        token(id: "${mspaceAddress[chainId]}") {
          derivedMETA
        }
  }`;

  const client = new SubscriptionClient(graphWSEndpoints.exchange[chainId], { reconnect: true, }, ws,);
  const observable = client.request({ query });

  return {
    subscribe({ next, error, complete }) {
      return observable.subscribe({
        next(results) {
          next(_priceMETA.callback(results.data.token));
        },
        error,
        complete
      });
    }
  };
}


const _priceMETA = {
  properties: [
    'derivedMETA'
  ],

  callback(results) {
    return Number(results.derivedMETA);
  },
};