const pageResults = require('graph-results-pager');

const ws = require('isomorphic-ws');
const { SubscriptionClient } = require('subscriptions-transport-ws');

const { request, gql } = require('graphql-request');

const {
  subWeeks,
  getUnixTime,
  fromUnixTime
} = require("date-fns");

const { graphAPIEndpoints, graphWSEndpoints, TWENTY_FOUR_HOURS, DEFAULT_CHAIN_ID } = require('./../../constants')
const { timestampToBlock, timestampsToBlocks, blockToTimestamp } = require('./../../utils');

const { metaPrice } = require('./meta');


export async function pair({ block = undefined, timestamp = undefined, pair_address = undefined, chainId = DEFAULT_CHAIN_ID } = {}) {
  if (!pair_address) { throw new Error("subgraph: Pair address undefined"); }

  block = block ? block : timestamp ? (await timestampToBlock({ timestamp: timestamp, chainId: chainId })) : undefined;
  block = block ? `block: { number: ${block} }` : "";

  const result = await request(graphAPIEndpoints.exchange[chainId],
    gql`{
          pair(id: "${pair_address.toLowerCase()}", ${block}) {
            ${_pairs.properties.toString()}
          }
        }`
  );

  return _pairs.callback([result.pair])[0];
}

export async function pair24h({ block = undefined, timestamp = undefined, pair_address = undefined, chainId = DEFAULT_CHAIN_ID } = {}) {
  if (!pair_address) { throw new Error("subgraph: Pair address undefined"); }

  let timestampNow = timestamp ? timestamp : block ? await blockToTimestamp({ block: block, chainId: chainId }) : (Math.floor(Date.now() / 1000));
  let timestamp24ago = timestampNow - TWENTY_FOUR_HOURS;
  let timestamp48ago = timestamp24ago - TWENTY_FOUR_HOURS;

  block = timestamp ? await timestampToBlock({ timestamp: timestamp, chainId: chainId }) : block;
  let block24ago = await timestampToBlock({ timestamp: timestamp24ago, chainId: chainId });
  let block48ago = await timestampToBlock({ timestamp: timestamp48ago, chainId: chainId });

  const result = await pair({
    block: block, pair_address: pair_address, chainId: chainId
  });
  const result24ago = await pair({
    block: block24ago, pair_address: pair_address, chainId: chainId
  });
  const result48ago = await pair({
    block: block48ago, pair_address: pair_address, chainId: chainId
  });

  const metaPriceUSD = await metaPrice({ block: block, chainId: chainId });
  const metaPriceUSD24ago = await metaPrice({ block: block24ago, chainId: chainId });

  return _pairs.callback24h([result], [result24ago], [result48ago], metaPriceUSD, metaPriceUSD24ago)[0];
}

export async function pairHourData({ minTimestamp = undefined, maxTimestamp = undefined, minBlock = undefined, maxBlock = undefined, pair_address = undefined, chainId = DEFAULT_CHAIN_ID } = {}) {
  if (!pair_address) { throw new Error("subgraph: Pair address undefined"); }

  minTimestamp = minBlock ? blockToTimestamp({ block: minBlock, chainId: chainId }) : minTimestamp;
  maxTimestamp = maxBlock ? blockToTimestamp({ block: maxBlock, chainId: chainId }) : maxTimestamp;

  const endTime = maxTimestamp ? fromUnixTime(maxTimestamp) : new Date();
  let time = minTimestamp ? minTimestamp : getUnixTime(subWeeks(endTime, 1));

  // create an array of hour start times until we reach current hour
  const timestamps = [];
  while (time <= getUnixTime(endTime) - 3600) {
    timestamps.push(time);
    time += 3600;
  }

  let blocks = await timestampsToBlocks({ timestamps: timestamps, chainId: chainId });

  const query = (
    gql`{
          ${blocks.map((block, i) => (gql`
            timestamp${timestamps[i]}: pair(id: "${pair_address.toLowerCase()}", block: {number: ${block}}) {
              ${_pairs.properties.toString()}
          }`))}
        }`
  );

  let result = await request(graphAPIEndpoints.exchange[chainId], query)
  result = Object.keys(result)
    .map(key => ({ ...result[key], timestamp: Number(key.split("timestamp")[1]) }))
    .sort((a, b) => (a.timestamp) - (b.timestamp));

  return _pairs.callbackHourData(result);
}

export async function pairDayData({ minTimestamp = undefined, maxTimestamp = undefined, minBlock = undefined, maxBlock = undefined, pair_address = undefined, chainId = DEFAULT_CHAIN_ID } = {}) {
  if (!pair_address) { throw new Error("subgraph: Pair address undefined"); }

  return pageResults({
    api: graphAPIEndpoints.exchange[chainId],
    query: {
      entity: 'pairDayDatas',
      selection: {
        orderDirection: 'desc',
        where: {
          pair: `\\"${pair_address.toLowerCase()}\\"`,
          date_gte: minTimestamp || (minBlock ? await blockToTimestamp({ block: minBlock, chainId: chainId }) : undefined),
          date_lte: maxTimestamp || (maxBlock ? await blockToTimestamp({ block: maxBlock, chainId: chainId }) : undefined),
        },
      },
      properties: _pairs.propertiesDayData
    }
  })
    .then(results => _pairs.callbackDayData(results))
    .catch(err => console.log(err));
}

export function observePair({ pair_address = undefined, chainId = DEFAULT_CHAIN_ID }) {
  if (!pair_address) { throw new Error("subgraph: Pair address undefined"); }

  const query = gql`
    subscription {
      pair(id: "${pair_address.toLowerCase()}") {
        ${_pairs.properties.toString()}
      }
  }`

  const client = new SubscriptionClient(graphWSEndpoints.exchange[chainId], { reconnect: true, }, ws,);
  const observable = client.request({ query });

  return {
    subscribe({ next, error, complete }) {
      return observable.subscribe({
        next(results) {
          next(_pairs.callback([results.data.pair])[0]);
        },
        error,
        complete
      });
    }
  };
}

export async function pairs({ block = undefined, timestamp = undefined, max = undefined, pair_addresses = undefined, chainId = DEFAULT_CHAIN_ID } = {}) {
  if (pair_addresses) {

    block = block ? block : timestamp ? (await timestampToBlock({ timestamp: timestamp, chainId: chainId })) : undefined;
    block = block ? `block: { number: ${block} }` : "";

    const query = (
      gql`{
            ${pair_addresses.map((pair, i) => (`
              pair${i}: pair(id: "${pair.toLowerCase()}", ${block}) {
                ${_pairs.properties.toString()}
            }`))}
          }`
    );

    const result = Object.values(await request(graphAPIEndpoints.exchange[chainId], query));

    return _pairs.callback(result);
  }

  return pageResults({
    api: graphAPIEndpoints.exchange[chainId],
    query: {
      entity: 'pairs',
      selection: {
        block: block ? { number: block } : timestamp ? { number: await timestampToBlock({ timestamp: timestamp, chainId: chainId }) } : undefined,
      },
      properties: _pairs.properties
    },
    max
  })
    .then(results => _pairs.callback(results))
    .catch(err => console.log(err));
}

export async function pairs24h({ block = undefined, timestamp = undefined, max = undefined, chainId = DEFAULT_CHAIN_ID } = {}) {
  let timestampNow = timestamp ? timestamp : block ? await blockToTimestamp({ block: block, chainId: chainId }) : (Math.floor(Date.now() / 1000));
  let timestamp24ago = timestampNow - TWENTY_FOUR_HOURS;
  let timestamp48ago = timestamp24ago - TWENTY_FOUR_HOURS;

  block = timestamp ? await timestampToBlock({ timestamp: timestamp, chainId: chainId }) : block;
  let block24ago = await timestampToBlock({ timestamp: timestamp24ago, chainId: chainId });
  let block48ago = await timestampToBlock({ timestamp: timestamp48ago, chainId: chainId });

  const results = await pairs({ block: block, max: max, chainId: chainId });
  const results24ago = await pairs({ block: block24ago, max: max, chainId: chainId });
  const results48ago = await pairs({ block: block48ago, max: max, chainId: chainId });

  const metaPriceUSD = await metaPrice({ block: block, chainId: chainId });
  const metaPriceUSD24ago = await metaPrice({ block: block24ago, chainId: chainId });

  return _pairs.callback24h(results, results24ago, results48ago, metaPriceUSD, metaPriceUSD24ago);
}

export function observePairs({ chainId = DEFAULT_CHAIN_ID } = {}) {
  const query = gql`
    subscription {
      pairs(first: 1000, orderBy: reserveUSD, orderDirection: desc) {
        ${_pairs.properties.toString()}
      }
  }`;

  const client = new SubscriptionClient(graphWSEndpoints.exchange[chainId], { reconnect: true, }, ws,);
  const observable = client.request({ query });

  return {
    subscribe({ next, error, complete }) {
      return observable.subscribe({
        next(results) {
          next(_pairs.callback(results.data.pairs));
        },
        error,
        complete
      });
    }
  };
}

export async function getPairAddresses({ tokenPairs = [], chainId = DEFAULT_CHAIN_ID } = {}) {
  if (tokenPairs.length > 0) {
    const inputs = tokenPairs.map(tokens => [
      tokens[0] === undefined ? "":tokens[0].toLowerCase(), 
      tokens[1] === undefined ? "":tokens[1].toLowerCase()
    ]).map(tokens => tokens[0] < tokens[1] ?  tokens : [tokens[1], tokens[0]]);

    const query = (
      gql`{
            ${inputs.map(([token0, token1], i) => (`
              pair${i}: pairs(
                where: {
                  token0: "${token0}"
                  token1: "${token1}"
                }
              ) {
                ${_pairs.minimalProperties.toString()}
            }`))}
          }`
    );

    const results = Object.values(await request(graphAPIEndpoints.exchange[chainId], query));
    
    return results.map(result => result? result[0]:undefined);
  }
  return [];
}


const _pairs = {
  properties: [
    'id',
    'token0 { id, name, symbol, totalSupply, derivedMETA }',
    'token1 { id, name, symbol, totalSupply, derivedMETA }',
    'reserve0',
    'reserve1',
    'totalSupply',
    'reserveMETA',
    'reserveUSD',
    'trackedReserveMETA',
    'token0Price',
    'token1Price',
    'volumeToken0',
    'volumeToken1',
    'volumeUSD',
    'untrackedVolumeUSD',
    'txCount',
  ],

  minimalProperties: [
    'id',
    'token0 { id }',
    'token1 { id }',
  ],

  callback(results) {
    return results
      .map(result => ({
        id: result.id,
        token0: {
          id: result.token0.id,
          name: result.token0.name,
          symbol: result.token0.symbol,
          totalSupply: Number(result.token0.totalSupply),
          derivedMETA: Number(result.token0.derivedMETA),
        },
        token1: {
          id: result.token1.id,
          name: result.token1.name,
          symbol: result.token1.symbol,
          totalSupply: Number(result.token1.totalSupply),
          derivedMETA: Number(result.token1.derivedMETA),
        },
        reserve0: Number(result.reserve0),
        reserve1: Number(result.reserve1),
        totalSupply: Number(result.totalSupply),
        reserveMETA: Number(result.reserveMETA),
        reserveUSD: Number(result.reserveUSD),
        trackedReserveMETA: Number(result.trackedReserveMETA),
        token0Price: Number(result.token0Price),
        token1Price: Number(result.token1Price),
        volumeToken0: Number(result.volumeToken0),
        volumeToken1: Number(result.volumeToken1),
        volumeUSD: Number(result.volumeUSD),
        untrackedVolumeUSD: Number(result.untrackedVolumeUSD),
        txCount: Number(result.txCount),
      }))
      .sort((a, b) => b.reserveUSD - a.reserveUSD);
  },

  callback24h(results, results24h, results48h, metaPriceUSD, metaPriceUSD24ago) {
    return results.map(result => {
      const result24h = results24h.find(e => e.id === result.id) || result;
      const result48h = results48h.find(e => e.id === result.id) || result;

      return ({
        ...result,

        trackedReserveUSD: result.trackedReserveMETA * metaPriceUSD,
        trackedReserveUSDChange: (result.trackedReserveMETA * metaPriceUSD) / (result24h.trackedReserveMETA * metaPriceUSD24ago) * 100 - 100,
        trackedReserveUSDChangeCount: result.trackedReserveMETA * metaPriceUSD - result24h.trackedReserveMETA * metaPriceUSD24ago,

        trackedReserveMETAChange: (result.trackedReserveMETA / result24h.trackedReserveMETA) * 100 - 100,
        trackedReserveMETAChangeCount: result.trackedReserveMETA - result24h.trackedReserveMETA,

        volumeUSDOneDay: result.volumeUSD - result24h.volumeUSD,
        volumeUSDChange: (result.volumeUSD - result24h.volumeUSD) / (result24h.volumeUSD - result48h.volumeUSD) * 100 - 100,
        volumeUSDChangeCount: (result.volumeUSD - result24h.volumeUSD) - (result24h.volumeUSD - result48h.volumeUSD),

        untrackedVolumeUSDOneDay: result.untrackedVolumeUSD - result24h.untrackedVolumeUSD,
        untrackedVolumeUSDChange: (result.untrackedVolumeUSD - result24h.untrackedVolumeUSD) / (result24h.untrackedVolumeUSD - result48h.untrackedVolumeUSD) * 100 - 100,
        untrackedVolumeUSDChangeCount: (result.untrackedVolumeUSD - result24h.untrackedVolumeUSD) - (result24h.untrackedVolumeUSD - result48h.untrackedVolumeUSD),

        txCountOneDay: result.txCount - result24h.txCount,
        txCountChange: (result.txCount - result24h.txCount) / (result24h.txCount - result48h.txCount) * 100 - 100,
        txCountChangeCount: (result.txCount - result24h.txCount) - (result24h.txCount - result48h.txCount),
      })
    });
  },

  callbackHourData(results) {
    return results.map(result => ({
      id: result.id,
      token0: {
        id: result.token0.id,
        name: result.token0.name,
        symbol: result.token0.symbol,
        totalSupply: Number(result.token0.totalSupply),
        derivedMETA: Number(result.token0.derivedMETA),
      },
      token1: {
        id: result.token1.id,
        name: result.token1.name,
        symbol: result.token1.symbol,
        totalSupply: Number(result.token1.totalSupply),
        derivedMETA: Number(result.token1.derivedMETA),
      },
      reserve0: Number(result.reserve0),
      reserve1: Number(result.reserve1),
      totalSupply: Number(result.totalSupply),
      reserveMETA: Number(result.reserveMETA),
      reserveUSD: Number(result.reserveUSD),
      trackedReserveMETA: Number(result.trackedReserveMETA),
      token0Price: Number(result.token0Price),
      token1Price: Number(result.token1Price),
      volumeToken0: Number(result.volumeToken0),
      volumeToken1: Number(result.volumeToken1),
      volumeUSD: Number(result.volumeUSD),
      untrackedVolumeUSD: Number(result.untrackedVolumeUSD),
      txCount: Number(result.txCount),
      timestamp: result.timestamp
    }));
  },

  propertiesDayData: [
    'id',
    'date',
    'volumeUSD',
    'volumeToken0',
    'volumeToken1',
    'reserveUSD',
    'txCount'
  ],

  callbackDayData(results) {
    return results.map(result => ({
      id: result.id,
      date: new Date(result.date * 1000),
      timestamp: Number(result.date),
      volumeUSD: Number(result.volumeUSD),
      volumeToken0: Number(result.volumeToken0),
      volumeToken1: Number(result.volumeToken1),
      liquidityUSD: Number(result.reserveUSD),
      txCount: Number(result.txCount)
    }));
  }
}