const pageResults = require('graph-results-pager');

const ws = require('isomorphic-ws');
const { SubscriptionClient } = require('subscriptions-transport-ws');

const { request, gql } = require('graphql-request');

const { graphAPIEndpoints, graphWSEndpoints, factoryAddress, TWENTY_FOUR_HOURS, DEFAULT_CHAIN_ID } = require('./../../constants')
const { timestampToBlock, blockToTimestamp } = require('./../../utils');


export async function factory({ block = undefined, timestamp = undefined, chainId = DEFAULT_CHAIN_ID } = {}) {
  block = block ? block : timestamp ? (await timestampToBlock({ timestamp: timestamp, chainId: chainId })) : undefined;
  block = block ? `block: { number: ${block} }` : "";

  const result = await request(graphAPIEndpoints.exchange[chainId],
    gql`{
          factory(id: "${factoryAddress[chainId]}", ${block}) {
            ${_factory.properties.toString()}
          }
        }`
  );

  return _factory.callback(result.factory);
}

export function observeFactory({ chainId = DEFAULT_CHAIN_ID } = {}) {
  const query = gql`
    subscription {
      factory(id: "${factoryAddress[chainId]}") {
        ${_factory.properties.toString()}
      }
  }`;

  const client = new SubscriptionClient(graphWSEndpoints.exchange[chainId], { reconnect: true, }, ws,);
  const observable = client.request({ query });

  return {
    subscribe({ next, error, complete }) {
      return observable.subscribe({
        next(results) {
          next(_factory.callback(results.data.factory));
        },
        error,
        complete
      });
    }
  };
}

export async function dayData({ minTimestamp = undefined, maxTimestamp = undefined, minBlock = undefined, maxBlock = undefined, chainId = DEFAULT_CHAIN_ID } = {}) {
  return pageResults({
    api: graphAPIEndpoints.exchange[chainId],
    query: {
      entity: 'dayDatas',
      selection: {
        orderDirection: 'desc',
        where: {
          date_gte: minTimestamp || (minBlock ? await blockToTimestamp({ block: minBlock, chainId: chainId }) : undefined),
          date_lte: maxTimestamp || (maxBlock ? await blockToTimestamp({ block: maxBlock, chainId: chainId }) : undefined),
        },
      },
      properties: _dayData.properties
    }
  })
    .then(results => _dayData.callback(results))
    .catch(err => console.log(err));
}

export async function twentyFourHourData({ block = undefined, timestamp = undefined, chainId = DEFAULT_CHAIN_ID } = {}) {
  timestamp = timestamp ? timestamp : block ? await blockToTimestamp({ block: block, chainId: chainId }) : (Date.now() / 1000)
  let timestamp24ago = timestamp - TWENTY_FOUR_HOURS;

  block = await timestampToBlock({ timestamp: timestamp, chainId: chainId });
  let block24ago = await timestampToBlock({ timestamp: timestamp24ago, chainId: chainId });

  block = `block: { number: ${block} }`;
  block24ago = `block: { number: ${block24ago} }`;

  const result = await request(graphAPIEndpoints.exchange[chainId],
    gql`{
          factory(id: "${factoryAddress[chainId]}", ${block}) {
            ${_twentyFourHourData.properties.toString()}
          }
        }`
  );

  const result24ago = await request(graphAPIEndpoints.exchange[chainId],
    gql`{
          factory(id: "${factoryAddress[chainId]}", ${block24ago}) {
            ${_twentyFourHourData.properties.toString()}
          }
        }`
  );

  return _twentyFourHourData.callback(result.factory, result24ago.factory);
}


const _factory = {
  properties: [
    'pairCount',
    'volumeUSD',
    'volumeMETA',
    'untrackedVolumeUSD',
    'liquidityUSD',
    'liquidityMETA',
    'txCount',
    'tokenCount',
    'userCount',
  ],

  callback(results) {
    return ({
      pairCount: Number(results.pairCount),
      volumeUSD: Number(results.volumeUSD),
      volumeMETA: Number(results.volumeMETA),
      untrackedVolumeUSD: Number(results.untrackedVolumeUSD),
      liquidityUSD: Number(results.liquidityUSD),
      liquidityMETA: Number(results.liquidityMETA),
      txCount: Number(results.txCount),
      tokenCount: Number(results.tokenCount),
      userCount: Number(results.userCount),
    });
  }
};

const _dayData = {
  properties: [
    'id',
    'date',
    'volumeMETA',
    'volumeUSD',
    'liquidityMETA',
    'liquidityUSD',
    'txCount'
  ],

  callback(results) {
    return results.map(({ id, date, volumeMETA, volumeUSD, liquidityMETA, liquidityUSD, txCount }) => ({
      id: Number(id),
      date: new Date(date * 1000),
      volumeMETA: Number(volumeMETA),
      volumeUSD: Number(volumeUSD),
      liquidityMETA: Number(liquidityMETA),
      liquidityUSD: Number(liquidityUSD),
      txCount: Number(txCount),
    }));
  }
};

const _twentyFourHourData = {
  properties: [
    'id',
    'volumeUSD',
    'volumeMETA',
    'untrackedVolumeUSD',
    'liquidityUSD',
    'liquidityMETA',
    'txCount',
    'pairCount'
  ],

  callback(results, results24ago) {
    return ({
      id: results.id,
      volumeUSD: Number(results.volumeUSD) - Number(results24ago.volumeUSD),
      volumeMETA: Number(results.volumeMETA) - Number(results24ago.volumeMETA),
      untrackedVolumeUSD: Number(results.untrackedVolumeUSD) - Number(results24ago.untrackedVolumeUSD),
      liquidityMETA: Number(results.liquidityMETA) - Number(results24ago.liquidityMETA),
      liquidityUSD: Number(results.liquidityUSD) - Number(results24ago.liquidityUSD),
      txCount: Number(results.txCount) - Number(results24ago.txCount),
      pairCount: Number(results.pairCount) - Number(results24ago.pairCount)
    })
  }
}
