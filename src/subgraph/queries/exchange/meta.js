const ws = require('isomorphic-ws');
const { SubscriptionClient } = require('subscriptions-transport-ws');

const { request, gql } = require('graphql-request');

const {
  subWeeks,
  getUnixTime,
  fromUnixTime
} = require("date-fns");

const { graphAPIEndpoints, graphWSEndpoints, DEFAULT_CHAIN_ID } = require('./../../constants')
const { timestampToBlock, timestampsToBlocks, blockToTimestamp } = require('./../../utils');

export async function metaPrice({ block = undefined, timestamp = undefined, chainId = DEFAULT_CHAIN_ID } = {}) {
  block = block ? block : timestamp ? (await timestampToBlock(timestamp)) : undefined;
  block = block ? `block: { number: ${block} }` : "";

  const result = await request(graphAPIEndpoints.exchange[chainId],
    gql`{
          bundle(id: 1, ${block}) {
            ${_metaPrice.properties.toString()}
          }
        }`
  );

  return _metaPrice.callback(result.bundle);
}

export async function metaPriceHourly({ minTimestamp = undefined, maxTimestamp = undefined, minBlock = undefined, maxBlock = undefined, chainId = DEFAULT_CHAIN_ID } = {}) {
  minTimestamp = minBlock ? blockToTimestamp(minBlock) : minTimestamp;
  maxTimestamp = maxBlock ? blockToTimestamp(maxBlock) : maxTimestamp;

  const endTime = maxTimestamp ? fromUnixTime(maxTimestamp) : new Date();
  let time = minTimestamp ? minTimestamp : getUnixTime(subWeeks(endTime, 1));

  // create an array of hour start times until we reach current hour
  const timestamps = [];
  while (time <= getUnixTime(endTime) - 3600) {
    timestamps.push(time);
    time += 3600;
  }

  let blocks = await timestampsToBlocks(timestamps);

  const query = (
    gql`{
          ${blocks.map((block, i) => (gql`
            timestamp${timestamps[i]}: bundle(id: 1, block: {number: ${block}}) {
              ${_metaPrice.properties.toString()}
          }`))}
        }`
  );

  let result = await request(graphAPIEndpoints.exchange[chainId], query)

  result = Object.keys(result)
    .map(key => ({ ...result[key], timestamp: key.split("timestamp")[1] }))
    .sort((a, b) => Number(a.timestamp) - (b.timestamp));

  return _metaPrice.callbackHourly(result);
}

export function observeMetaPrice({ chainId = undefined } = {}) {
  chainId = chainId ? chainId : 11;
  const query = gql`
    subscription {
      bundle(id: 1) {
        ${_metaPrice.properties.toString()}
      }
  }`;

  const client = new SubscriptionClient(graphWSEndpoints.exchange[chainId], { reconnect: true, }, ws,);
  const observable = client.request({ query });

  return {
    subscribe({ next, error, complete }) {
      return observable.subscribe({
        next(results) {
          next(_metaPrice.callback(results.data.bundle));
        },
        error,
        complete
      });
    }
  };
}


const _metaPrice = {
  properties: [
    'metaPrice'
  ],

  callback(results) {
    return Number(results.metaPrice);
  },

  callbackHourly(results) {
    return results.map(result => ({
      timestamp: Number(result.timestamp),
      priceUSD: Number(result.metaPrice)
    }))
  }
}