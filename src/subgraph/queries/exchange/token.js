const pageResults = require('graph-results-pager');

const ws = require('isomorphic-ws');
const { SubscriptionClient } = require('subscriptions-transport-ws');

const { request, gql } = require('graphql-request');

const {
  subWeeks,
  getUnixTime,
  fromUnixTime
} = require("date-fns");

const { graphAPIEndpoints, graphWSEndpoints, TWENTY_FOUR_HOURS, DEFAULT_CHAIN_ID } = require('./../../constants')
const { timestampToBlock, timestampsToBlocks, blockToTimestamp } = require('./../../utils');

const { metaPrice } = require('./meta');


export async function token({ block = undefined, timestamp = undefined, token_address = undefined, chainId = DEFAULT_CHAIN_ID } = {}) {
  if (!token_address) { throw new Error("subgraph: Token address undefined"); }

  block = block ? block : timestamp ? (await timestampToBlock({ timestamp: timestamp, chainId: chainId })) : undefined;
  block = block ? `block: { number: ${block} }` : "";

  const result = await request(graphAPIEndpoints.exchange[chainId],
    gql`{
          token(id: "${token_address.toLowerCase()}", ${block}) {
            ${_tokens.properties.toString()}
          }
        }`
  );

  return _tokens.callback([result.token])[0];
}

export async function token24h({ block = undefined, timestamp = undefined, token_address = undefined, chainId = DEFAULT_CHAIN_ID } = {}) {
  if (!token_address) { throw new Error("subgraph: Token address undefined"); }

  let timestampNow = timestamp ? timestamp : block ? await blockToTimestamp({ block: block, chainId: chainId }) : (Math.floor(Date.now() / 1000));
  let timestamp24ago = timestampNow - TWENTY_FOUR_HOURS;
  let timestamp48ago = timestamp24ago - TWENTY_FOUR_HOURS;

  block = timestamp ? await timestampToBlock({ timestamp: timestamp, chainId: chainId }) : block;
  let block24ago = await timestampToBlock({ timestamp: timestamp24ago, chainId: chainId });
  let block48ago = await timestampToBlock({ timestamp: timestamp48ago, chainId: chainId });

  const result = await token({
    block: block, token_address: token_address, chainId: chainId
  });
  const result24ago = await token({
    block: block24ago, token_address: token_address, chainId: chainId
  });
  const result48ago = await token({
    block: block48ago, token_address: token_address, chainId: chainId
  });

  const metaPriceUSD = await metaPrice({ block: block, chainId: chainId });
  const metaPriceUSD24ago = await metaPrice({ block: block24ago, chainId: chainId });

  return _tokens.callback24h([result], [result24ago], [result48ago], metaPriceUSD, metaPriceUSD24ago)[0];
}

export async function tokenHourData({ minTimestamp = undefined, maxTimestamp = undefined, minBlock = undefined, maxBlock = undefined, token_address = undefined, chainId = DEFAULT_CHAIN_ID } = {}) {
  if (!token_address) { throw new Error("subgraph: Token address undefined"); }

  minTimestamp = minBlock ? blockToTimestamp({ block: minBlock, chainId: chainId }) : minTimestamp;
  maxTimestamp = maxBlock ? blockToTimestamp({ block: maxBlock, chainId: chainId }) : maxTimestamp;

  const endTime = maxTimestamp ? fromUnixTime(maxTimestamp) : new Date();
  let time = minTimestamp ? minTimestamp : getUnixTime(subWeeks(endTime, 1));

  // create an array of hour start times until we reach current hour
  const timestamps = [];
  while (time <= getUnixTime(endTime) - 3600) {
    timestamps.push(time);
    time += 3600;
  }

  let blocks = await timestampsToBlocks({ timestamps: timestamps, chainId: chainId });

  const query = (
    gql`{
          ${blocks.map((block, i) => (gql`
            timestamp${timestamps[i]}: token(id: "${token_address.toLowerCase()}", block: {number: ${block}}) {
              ${_tokens.properties.toString()}
          }`))}
        }`
  );

  let result = await request(graphAPIEndpoints.exchange[chainId], query)
  result = Object.keys(result)
    .map(key => ({ ...result[key], timestamp: Number(key.split("timestamp")[1]) }))
    .sort((a, b) => (a.timestamp) - (b.timestamp));

  return _tokens.callbackHourData(result);
}

export async function tokenDayData({ minTimestamp = undefined, maxTimestamp = undefined, minBlock = undefined, maxBlock = undefined, token_address = undefined, chainId = DEFAULT_CHAIN_ID } = {}) {
  if (!token_address) { throw new Error("subgraph: Token address undefined"); }

  return pageResults({
    api: graphAPIEndpoints.exchange[chainId],
    query: {
      entity: 'tokenDayDatas',
      selection: {
        orderDirection: 'desc',
        where: {
          token: `\\"${token_address.toLowerCase()}\\"`,
          date_gte: minTimestamp || (minBlock ? await blockToTimestamp({ block: minBlock, chainId: chainId }) : undefined),
          date_lte: maxTimestamp || (maxBlock ? await blockToTimestamp({ block: maxBlock, chainId: chainId }) : undefined),
        },
      },
      properties: _tokens.propertiesDayData
    }
  })
    .then(results => _tokens.callbackDayData(results))
    .catch(err => console.log(err));
}

export function observeToken({ token_address = undefined, chainId = DEFAULT_CHAIN_ID } = {}) {
  if (!token_address) { throw new Error("subgraph: Token address undefined"); }

  const query = gql`
    subscription {
      token(id: "${token_address.toLowerCase()}") {
        ${_tokens.properties.toString()}
      }
  }`;

  const client = new SubscriptionClient(graphWSEndpoints.exchange[chainId], { reconnect: true, }, ws,);
  const observable = client.request({ query });

  return {
    subscribe({ next, error, complete }) {
      return observable.subscribe({
        next(results) {
          next(_tokens.callback([results.data.token])[0]);
        },
        error,
        complete
      });
    }
  };
}

export async function tokens({ block = undefined, timestamp = undefined, max = undefined, chainId = DEFAULT_CHAIN_ID } = {}) {
  return pageResults({
    api: graphAPIEndpoints.exchange[chainId],
    query: {
      entity: 'tokens',
      selection: {
        block: block ? { number: block } : timestamp ? { number: await timestampToBlock({ timestamp: timestamp, chainId: chainId }) } : undefined,
      },
      properties: _tokens.properties
    },
    max
  })
    .then(results => _tokens.callback(results))
    .catch(err => console.log(err));
}

export async function tokens24h({ block = undefined, timestamp = undefined, max = undefined, chainId = DEFAULT_CHAIN_ID } = {}) {
  let timestampNow = timestamp ? timestamp : block ? await blockToTimestamp({ block: block, chainId: chainId }) : (Math.floor(Date.now() / 1000));
  let timestamp24ago = timestampNow - TWENTY_FOUR_HOURS;
  let timestamp48ago = timestamp24ago - TWENTY_FOUR_HOURS;

  block = timestamp ? await timestampToBlock({ timestamp: timestamp, chainId: chainId }) : block;
  let block24ago = await timestampToBlock({ timestamp: timestamp24ago, chainId: chainId });
  let block48ago = await timestampToBlock({ timestamp: timestamp48ago, chainId: chainId });

  const results = await tokens({ block: block, max: max, chainId: chainId });
  const results24ago = await tokens({ block: block24ago, max: max, chainId: chainId });
  const results48ago = await tokens({ block: block48ago, max: max, chainId: chainId });

  const metaPriceUSD = await metaPrice({ block: block, chainId: chainId });
  const metaPriceUSD24ago = await metaPrice({ block: block24ago, chainId: chainId });

  return _tokens.callback24h(results, results24ago, results48ago, metaPriceUSD, metaPriceUSD24ago);
}

export function observeTokens({ chainId = DEFAULT_CHAIN_ID } = {}) {
  const query = gql`
    subscription {
      tokens(first: 1000, orderBy: volumeUSD, orderDirection: desc) {
        ${_tokens.properties.toString()}
      }
  }`;

  const client = new SubscriptionClient(graphWSEndpoints.exchange[chainId], { reconnect: true, }, ws,);
  const observable = client.request({ query });

  return {
    subscribe({ next, error, complete }) {
      return observable.subscribe({
        next(results) {
          next(_tokens.callback(results.data.tokens));
        },
        error,
        complete
      });
    }
  };
}


const _tokens = {
  properties: [
    'id',
    'symbol',
    'name',
    'decimals',
    'totalSupply',
    'volume',
    'volumeUSD',
    'untrackedVolumeUSD',
    'txCount',
    'liquidity',
    'derivedMETA'
  ],

  callback(results) {
    return results
      .map(({ id, symbol, name, decimals, totalSupply, volume, volumeUSD, untrackedVolumeUSD, txCount, liquidity, derivedMETA }) => ({
        id: id,
        symbol: symbol,
        name: name,
        decimals: Number(decimals),
        totalSupply: Number(totalSupply),
        volume: Number(volume),
        volumeUSD: Number(volumeUSD),
        untrackedVolumeUSD: Number(untrackedVolumeUSD),
        txCount: Number(txCount),
        liquidity: Number(liquidity),
        derivedMETA: Number(derivedMETA)
      }))
      .sort((a, b) => b.volumeUSD - a.volumeUSD);
  },

  callback24h(results, results24h, results48h, metaPriceUSD, metaPriceUSD24ago) {
    return results.map(result => {
      const result24h = results24h.find(e => e.id === result.id) || result;
      const result48h = results48h.find(e => e.id === result.id) || result;

      return ({
        ...result,

        priceUSD: result.derivedMETA * metaPriceUSD,
        priceUSDChange: (result.derivedMETA * metaPriceUSD) / (result24h.derivedMETA * metaPriceUSD24ago) * 100 - 100,
        priceUSDChangeCount: (result.derivedMETA * metaPriceUSD) - (result24h.derivedMETA * metaPriceUSD24ago),

        liquidityUSD: result.liquidity * result.derivedMETA * metaPriceUSD,
        liquidityUSDChange: (result.liquidity * result.derivedMETA * metaPriceUSD) / (result24h.liquidity * result24h.derivedMETA * metaPriceUSD24ago) * 100 - 100,
        liquidityUSDChangeCount: result.liquidity * result.derivedMETA * metaPriceUSD - result24h.liquidity * result24h.derivedMETA * metaPriceUSD24ago,

        liquidityMETA: result.liquidity * result.derivedMETA,
        liquidityMETAChange: (result.liquidity * result.derivedMETA) / (result24h.liquidity * result24h.derivedMETA) * 100 - 100,
        liquidityMETAChangeCount: result.liquidity * result.derivedMETA - result24h.liquidity * result24h.derivedMETA,

        volumeUSDOneDay: result.volumeUSD - result24h.volumeUSD,
        volumeUSDChange: (result.volumeUSD - result24h.volumeUSD) / (result24h.volumeUSD - result48h.volumeUSD) * 100 - 100,
        volumeUSDChangeCount: (result.volumeUSD - result24h.volumeUSD) - (result24h.volumeUSD - result48h.volumeUSD),

        untrackedVolumeUSDOneDay: result.untrackedVolumeUSD - result24h.untrackedVolumeUSD,
        untrackedVolumeUSDChange: (result.untrackedVolumeUSD - result24h.untrackedVolumeUSD) / (result24h.untrackedVolumeUSD - result48h.untrackedVolumeUSD) * 100 - 100,
        untrackedVolumeUSDChangeCount: (result.untrackedVolumeUSD - result24h.untrackedVolumeUSD) - (result24h.untrackedVolumeUSD - result48h.untrackedVolumeUSD),

        txCountOneDay: result.txCount - result24h.txCount,
        txCountChange: (result.txCount - result24h.txCount) / (result24h.txCount - result48h.txCount) * 100 - 100,
        txCountChangeCount: (result.txCount - result24h.txCount) - (result24h.txCount - result48h.txCount),
      })
    });
  },

  callbackHourData(results) {
    return results.map(result => ({
      id: result.id,
      symbol: result.symbol,
      name: result.name,
      decimals: Number(result.decimals),
      totalSupply: Number(result.totalSupply),
      volume: Number(result.volume),
      volumeUSD: Number(result.volumeUSD),
      untrackedVolumeUSD: Number(result.untrackedVolumeUSD),
      txCount: Number(result.txCount),
      liquidity: Number(result.liquidity),
      derivedMETA: Number(result.derivedMETA),
      timestamp: result.timestamp
    }));
  },

  propertiesDayData: [
    'id',
    'date',
    'volume',
    'volumeMETA',
    'volumeUSD',
    'liquidity',
    'liquidityMETA',
    'liquidityUSD',
    'priceUSD',
    'txCount'
  ],

  callbackDayData(results) {
    return results.map(result => ({
      id: result.id,
      date: new Date(result.date * 1000),
      timestamp: Number(result.date),
      volume: Number(result.volume),
      volumeMETA: Number(result.volumeMETA),
      volumeUSD: Number(result.volumeUSD),
      liquidity: Number(result.liquidity),
      liquidityMETA: Number(result.liquidityMETA),
      liquidityUSD: Number(result.liquidityUSD),
      priceUSD: Number(result.priceUSD),
      txCount: Number(result.txCount)
    }));
  }
};
