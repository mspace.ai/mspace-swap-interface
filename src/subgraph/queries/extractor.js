const pageResults = require('graph-results-pager');

const ws = require('isomorphic-ws');
const { SubscriptionClient } = require('subscriptions-transport-ws');

const { request, gql } = require('graphql-request');

const { graphAPIEndpoints, graphWSEndpoints, extractorAddress, DEFAULT_CHAIN_ID } = require('../constants')
const { timestampToBlock } = require('../utils')


export async function info({ block = undefined, timestamp = undefined, chainId = DEFAULT_CHAIN_ID } = {}) {
  block = block ? block : timestamp ? (await timestampToBlock({ timestamp: timestamp, chainId: chainId })) : undefined;
  block = block ? `block: { number: ${block} }` : "";

  const result = await request(graphAPIEndpoints.extractor[chainId],
    gql`{
          extractors(first: 1, ${block}) {
            ${_info.properties.toString()}
          }
        }`
  );

  return _info.callback(result.extractors[0]);
}

export function servings({ minTimestamp = undefined, maxTimestamp = undefined, minBlock = undefined, maxBlock = undefined, max = undefined, chainId = DEFAULT_CHAIN_ID } = {}) {
  return pageResults({
    api: graphAPIEndpoints.extractor[chainId],
    query: {
      entity: 'servings',
      selection: {
        where: {
          block_gte: minBlock || undefined,
          block_lte: maxBlock || undefined,
          timestamp_gte: minTimestamp || undefined,
          timestamp_lte: maxTimestamp || undefined,
        }
      },
      properties: _servings.properties
    },
    max
  })
    .then(results => _servings.callback(results))
    .catch(err => console.log(err));
}

export async function servers({ block = undefined, timestamp = undefined, max = undefined, chainId = DEFAULT_CHAIN_ID } = {}) {
  return pageResults({
    api: graphAPIEndpoints.extractor[chainId],
    query: {
      entity: 'servers',
      block: block ? { number: block } : timestamp ? { number: await timestampToBlock({ timestamp: timestamp, chainId: chainId }) } : undefined,
      properties: _servers.properties
    },
    max
  })
    .then(results => _servers.callback(results))
    .catch(err => console.log(err));
}

export async function pendingServings({ block = undefined, timestamp = undefined, max = undefined, chainId = DEFAULT_CHAIN_ID } = {}) {
  return pageResults({
    api: graphAPIEndpoints.exchange[chainId],
    query: {
      entity: 'users',
      selection: {
        where: {
          id: `\\"${extractorAddress[chainId]}\\"`,
        },
      },
      block: block ? { number: block } : timestamp ? { number: await timestampToBlock({ timestamp: timestamp, chainId: chainId }) } : undefined,
      properties: _pendingServings.properties
    },
    max
  })
    .then(results => _pendingServings.callback(results))
    .catch(err => console.log(err));
}

export function observePendingServings({ chainId = undefined } = {}) {
  chainId = chainId ? chainId : 11;
  const query = gql`
    subscription {
      users(first: 1000, where: {id: "${extractorAddress[chainId]}"}) {
        ${_pendingServings.properties.toString()}
      }
  }`;

  const client = new SubscriptionClient(graphWSEndpoints.exchange[chainId], { reconnect: true, }, ws,);
  const observable = client.request({ query });

  return {
    subscribe({ next, error, complete }) {
      return observable.subscribe({
        next(results) {
          next(_pendingServings.callback(results.data.users));
        },
        error,
        complete
      });
    }
  };
}


const _info = {
  properties: [
    'id',
    'mspaceServed'
  ],

  callback(results) {
    return ({
      address: results.id,
      mspaceServed: Number(results.mspaceServed)
    });
  }
}

const _servings = {
  properties: [
    'server { id }',
    'tx',
    'pair',
    'token0',
    'token1',
    'mspaceServed',
    'block',
    'timestamp'
  ],

  callback(results) {
    return results.map(({ server, tx, pair, token0, token1, mspaceServed, block, timestamp }) => ({
      serverAddress: server.id,
      tx: tx,
      pair: pair,
      token0: token0,
      token1: token1,
      mspaceServed: Number(mspaceServed),
      block: Number(block),
      timestamp: Number(timestamp * 1000),
      date: new Date(timestamp * 1000)
    }));
  }
};

const _servers = {
  properties: [
    'id',
    'mspaceServed',
    'servings(first: 1000, orderBy: block, orderDirection: desc) { tx, block, pair, mspaceServed }'
  ],

  callback(results) {
    return results.map(({ id, mspaceServed, servings }) => ({
      serverAddress: id,
      mspaceServed: Number(mspaceServed),
      servings: servings.map(({ tx, block, pair, mspaceServed }) => ({
        tx,
        block: Number(block),
        pair,
        mspaceServed: Number(mspaceServed)
      })),
    }));
  }
};

const _pendingServings = {
  properties: [
    'liquidityPositions(first: 1000) { id, liquidityTokenBalance, pair { id, totalSupply, reserveUSD, token0 { id, name, symbol }, token1 { id, symbol, name } } }'
  ],

  callback(results) {
    return results[0].liquidityPositions.map(({ liquidityTokenBalance, pair }) => ({
      address: pair.id,
      token0: pair.token0,
      token1: pair.token1,
      valueUSD: (liquidityTokenBalance / pair.totalSupply) * pair.reserveUSD
    })).sort((a, b) => b.valueUSD - a.valueUSD);
  }
};
