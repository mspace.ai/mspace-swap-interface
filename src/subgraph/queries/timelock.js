const pageResults = require('graph-results-pager');

const { graphAPIEndpoints, DEFAULT_CHAIN_ID } = require('./../constants');
const { timestampToBlock } = require('./../utils');


export async function queuedTxs({ minTimestamp = undefined, maxTimestamp = undefined, minBlock = undefined, maxBlock = undefined, max = undefined, chainId = DEFAULT_CHAIN_ID } = {}) {
  return pageResults({
    api: graphAPIEndpoints.timelock[chainId],
    query: {
      entity: 'timelocks',
      selection: {
        where: {
          isCanceled: false,
          isExecuted: false,
          createdBlock_gte: minBlock || (minTimestamp ? await timestampToBlock({ timestamp: minTimestamp, chainId: chainId }) : undefined),
          createdBlock_lte: maxBlock || (maxTimestamp ? await timestampToBlock({ timestamp: maxTimestamp, chainId: chainId }) : undefined),
        }
      },
      properties: _queuedTxs.properties
    },
    max
  })
    .then(results => _queuedTxs.callback(results))
    .catch(err => console.log(err));
}

export async function canceledTxs({ minTimestamp = undefined, maxTimestamp = undefined, minBlock = undefined, maxBlock = undefined, max = undefined, chainId = DEFAULT_CHAIN_ID } = {}) {
  return pageResults({
    api: graphAPIEndpoints.timelock[chainId],
    query: {
      entity: 'timelocks',
      selection: {
        where: {
          isCanceled: true,
          createdBlock_gte: minBlock || (minTimestamp ? await timestampToBlock({ timestamp: minTimestamp, chainId: chainId }) : undefined),
          createdBlock_lte: maxBlock || (maxTimestamp ? await timestampToBlock({ timestamp: maxTimestamp, chainId: chainId }) : undefined),
        },
      },
      properties: _canceledTxs.properties
    },
    max
  })
    .then(results => _canceledTxs.callback(results))
    .catch(err => console.log(err));
}

export async function executedTxs({ minTimestamp = undefined, maxTimestamp = undefined, minBlock = undefined, maxBlock = undefined, max = undefined, chainId = DEFAULT_CHAIN_ID } = {}) {
  return pageResults({
    api: graphAPIEndpoints.timelock[chainId],
    query: {
      entity: 'timelocks',
      selection: {
        where: {
          isExecuted: true,
          createdBlock_gte: minBlock || (minTimestamp ? await timestampToBlock({ timestamp: minTimestamp, chainId: chainId }) : undefined),
          createdBlock_lte: maxBlock || (maxTimestamp ? await timestampToBlock({ timestamp: maxTimestamp, chainId: chainId }) : undefined),
        }
      },
      properties: _executedTxs.properties
    },
    max
  })
    .then(results => _executedTxs.callback(results))
    .catch(err => console.log(err));
}

export async function allTxs({ minTimestamp = undefined, maxTimestamp = undefined, minBlock = undefined, maxBlock = undefined, max = undefined, chainId = DEFAULT_CHAIN_ID } = {}) {
  return pageResults({
    api: graphAPIEndpoints.timelock[chainId],
    query: {
      entity: 'timelocks',
      selection: {
        where: {
          createdBlock_gte: minBlock || (minTimestamp ? await timestampToBlock({ timestamp: minTimestamp, chainId: chainId }) : undefined),
          createdBlock_lte: maxBlock || (maxTimestamp ? await timestampToBlock({ timestamp: maxTimestamp, chainId: chainId }) : undefined),
        },
      },
      properties: _allTxs.properties
    },
    max
  })
    .then(results => _allTxs.callback(results))
    .catch(err => console.log(err));
}


const _queuedTxs = {
  properties: [
    'id',
    'description',
    'value',
    'eta',
    'functionName',
    'data',
    'targetAddress',
    'createdBlock',
    'createdTs',
    'expiresTs',
    'createdTx',
  ],

  callback(results) {
    return results
      .map(({ id, description, value, eta, functionName, data, targetAddress, createdBlock, createdTs, expiresTs, createdTx }) => ({
        txHash: id,
        description: description,
        value: Number(value),
        etaTs: Number(eta * 1000),
        etaDate: new Date(eta * 1000),
        functionName: functionName,
        data: data,
        targetAddress: targetAddress,
        createdBlock: Number(createdBlock),
        createdTs: Number(createdTs * 1000),
        createdDate: new Date(createdTs * 1000),
        expiresTs: Number(expiresTs * 1000),
        expiresDate: new Date(expiresTs * 1000),
        createdTx: createdTx,
      }))
      .sort((a, b) => b.createdBlock - a.createdBlock);
  }
};

const _canceledTxs = {
  properties: [
    'id',
    'description',
    'value',
    'eta',
    'functionName',
    'data',
    'targetAddress',
    'createdBlock',
    'createdTs',
    'expiresTs',
    'canceledBlock',
    'canceledTs',
    'createdTx',
    'canceledTx',
  ],

  callback(results) {
    return results
      .map(({ id, description, value, eta, functionName, data, targetAddress, createdBlock, createdTs, expiresTs, canceledBlock, canceledTs, createdTx, canceledTx }) => ({
        txHash: id,
        description: description,
        value: Number(value),
        etaTs: Number(eta * 1000),
        etaDate: new Date(eta * 1000),
        functionName: functionName,
        data: data,
        targetAddress: targetAddress,
        createdBlock: Number(createdBlock),
        createdTs: Number(createdTs * 1000),
        createdDate: new Date(createdTs * 1000),
        expiresTs: Number(expiresTs * 1000),
        expiresDate: new Date(expiresTs * 1000),
        canceledBlock: canceledTx ? Number(canceledBlock) : null,
        canceledTs: canceledTx ? Number(canceledTs * 1000) : null,
        canceledDate: canceledTx ? new Date(canceledTs * 1000) : null,
        createdTx: createdTx,
        canceledTx: canceledTx,
      }))
      .sort((a, b) => b.createdBlock - a.createdBlock);
  }
};

const _executedTxs = {
  properties: [
    'id',
    'description',
    'value',
    'eta',
    'functionName',
    'data',
    'targetAddress',
    'createdBlock',
    'createdTs',
    'expiresTs',
    'executedBlock',
    'executedTs',
    'createdTx',
    'executedTx'
  ],

  callback(results) {
    return results
      .map(({ id, description, value, eta, functionName, data, targetAddress, createdBlock, createdTs, expiresTs, executedBlock, executedTs, createdTx, executedTx }) => ({
        txHash: id,
        description: description,
        value: Number(value),
        etaTs: Number(eta * 1000),
        etaDate: new Date(eta * 1000),
        functionName: functionName,
        data: data,
        targetAddress: targetAddress,
        createdBlock: Number(createdBlock),
        createdTs: Number(createdTs * 1000),
        createdDate: new Date(createdTs * 1000),
        expiresTs: Number(expiresTs * 1000),
        expiresDate: new Date(expiresTs * 1000),
        executedBlock: executedTx ? Number(executedBlock) : null,
        executedTs: executedTx ? Number(executedTs * 1000) : null,
        executedDate: executedTx ? new Date(executedTs * 1000) : null,
        createdTx: createdTx,
        executedTx: executedTx
      }))
      .sort((a, b) => b.createdBlock - a.createdBlock);;
  }
};

const _allTxs = {
  properties: [
    'id',
    'description',
    'value',
    'eta',
    'functionName',
    'data',
    'targetAddress',
    'isCanceled',
    'isExecuted',
    'createdBlock',
    'createdTs',
    'expiresTs',
    'canceledBlock',
    'canceledTs',
    'executedBlock',
    'executedTs',
    'createdTx',
    'canceledTx',
    'executedTx'
  ],

  callback(results) {
    return results
      .map(({ id, description, value, eta, functionName, data, targetAddress, isCanceled, isExecuted, createdBlock, createdTs, expiresTs, canceledBlock, canceledTs, executedBlock, executedTs, createdTx, canceledTx, executedTx }) => ({
        txHash: id,
        description: description,
        value: Number(value),
        etaTs: Number(eta * 1000),
        etaDate: new Date(eta * 1000),
        functionName: functionName,
        data: data,
        targetAddress: targetAddress,
        isCanceled: isCanceled,
        isExecuted: isExecuted,
        createdBlock: Number(createdBlock),
        createdTs: Number(createdTs * 1000),
        createdDate: new Date(createdTs * 1000),
        expiresTs: Number(expiresTs * 1000),
        expiresDate: new Date(expiresTs * 1000),
        canceledBlock: canceledTx ? Number(canceledBlock) : null,
        canceledTs: canceledTx ? Number(canceledTs * 1000) : null,
        canceledDate: canceledTx ? new Date(canceledTs * 1000) : null,
        executedTs: executedTx ? Number(executedTs * 1000) : null,
        executedDate: executedTx ? new Date(executedTs * 1000) : null,
        createdTx: createdTx,
        canceledTx: canceledTx,
        executedTx: executedTx
      }))
      .sort((a, b) => b.createdBlock - a.createdBlock);
  }
};
