const pageResults = require('graph-results-pager');

const { request, gql } = require('graphql-request');

const { graphAPIEndpoints, miningAddress, TWENTY_FOUR_HOURS, DEFAULT_CHAIN_ID } = require('./../constants')
const { timestampToBlock, getAverageBlockTime, blockToTimestamp } = require('./../utils');

const { pairs: exchangePairs } = require('./exchange');
const { priceUSD: mspacePriceUSD } = require('./mspace');

export async function info({ block = undefined, timestamp = undefined, chainId = DEFAULT_CHAIN_ID } = {}) {
  block = block ? block : timestamp ? (await timestampToBlock({ timestamp: timestamp, chainId: chainId })) : undefined;
  block = block ? `block: { number: ${block} }` : "";

  const result = await request(graphAPIEndpoints.mining[chainId],
    gql`{
        mining(id: "${miningAddress[chainId]}", ${block}) {
            ${_info.properties.toString()}
          }
        }`
  );

  return _info.callback(result.mining);
}

export async function pool({ block = undefined, timestamp = undefined, pool_id = undefined, pool_address = undefined, chainId = DEFAULT_CHAIN_ID } = {}) {
  if (!pool_id && !pool_address) { throw new Error("subgraph: Pool ID / Address undefined"); }

  block = block ? block : timestamp ? (await timestampToBlock({ timestamp: timestamp, chainId: chainId })) : undefined;
  block = block ? `block: { number: ${block} }` : "";

  let result;
  if (pool_id) {
    result = await request(graphAPIEndpoints.mining[chainId],
      gql`{
            pool(id: ${pool_id}, ${block}) {
              ${_pools.properties.toString()}
            }
          }`
    );
  }

  else {
    result = await request(graphAPIEndpoints.mining[chainId],
      gql`{
            pools(first: 1, where: {pair: "${pool_address.toLowerCase()}"}, ${block}) {
              ${_pools.properties.toString()}
            }
          }`
    );
  }

  return _pools.callback(pool_id ? [result.pool] : result.pools)[0];
}

export async function pools({ block = undefined, timestamp = undefined, chainId = DEFAULT_CHAIN_ID } = {}) {
  return pageResults({
    api: graphAPIEndpoints.mining[chainId],
    query: {
      entity: 'pools',
      selection: {
        block: block ? { number: block } : timestamp ? { number: await timestampToBlock({ timestamp: timestamp, chainId: chainId }) } : undefined,
      },
      properties: _pools.properties
    }
  })
    .then(results => _pools.callback(results))
    .catch(err => console.log(err));
}

export async function stakedValue({ block = undefined, timestamp = undefined, token_address = undefined, chainId = DEFAULT_CHAIN_ID } = {}) {
  if (!token_address) { throw new Error("subgraph: Token address undefined"); }

  block = block ? block : timestamp ? (await timestampToBlock({ timestamp: timestamp, chainId: chainId })) : undefined;
  block = block ? `block: { number: ${block} }` : "";

  const result = await request(graphAPIEndpoints.exchange[chainId],
    gql`{
          liquidityPosition(id: "${token_address.toLowerCase()}-${miningAddress[chainId]}", ${block}) {
            ${_stakedValue.properties.toString()}
          }
        }`
  );

  return _stakedValue.callback(result.liquidityPosition);
}

export async function user({ block = undefined, timestamp = undefined, user_address = undefined, chainId = DEFAULT_CHAIN_ID } = {}) {
  if (!user_address) { throw new Error("subgraph: User address undefined"); }

  return pageResults({
    api: graphAPIEndpoints.mining[chainId],
    query: {
      entity: 'users',
      selection: {
        where: {
          address: `\\"${user_address.toLowerCase()}\\"`
        },
        block: block ? { number: block } : timestamp ? { number: await timestampToBlock({ timestamp: timestamp, chainId: chainId }) } : undefined,
      },
      properties: _user.properties
    }
  })
    .then(results => _user.callback(results))
    .catch(err => console.log(err));
}

export async function users({ block = undefined, timestamp = undefined, chainId = DEFAULT_CHAIN_ID } = {}) {
  return pageResults({
    api: graphAPIEndpoints.mining[chainId],
    query: {
      entity: 'users',
      selection: {
        block: block ? { number: block } : timestamp ? { number: await timestampToBlock({ timestamp: timestamp, chainId: chainId }) } : undefined,
      },
      properties: _user.properties
    }
  })
    .then(results => _user.callback(results))
    .catch(err => console.log(err));
}

export async function apys({ block = undefined, timestamp = undefined, chainId = DEFAULT_CHAIN_ID } = {}) {
  const miningList = await pools({ block: block, timestamp: timestamp, chainId: chainId });
  const exchangeList = await exchangePairs({ block: block, timestamp: timestamp, chainId: chainId });
  const mspaceUSD = await mspacePriceUSD({ block: block, timestamp: timestamp, chainId: chainId });

  const totalAllocPoint = miningList.reduce((a, b) => a + b.allocPoint, 0);

  const averageBlockTime = await getAverageBlockTime({ block: block, timestamp: timestamp, chainId: chainId });

  return miningList.map(miningPool => {
    const exchangePool = exchangeList.find(e => e.id === miningPool.pair);
    if (!exchangePool) {
      return { ...miningPool, apy: 0 };
    }

    const tvl = miningPool.lpBalance * (exchangePool.reserveUSD / exchangePool.totalSupply);
    // TODO change this
    const mspacePerBlock = (miningPool.allocPoint / (totalAllocPoint) * 100);
    const apy = mspaceUSD * (mspacePerBlock * (60 / averageBlockTime) * 60 * 24 * 365) / tvl * 100;

    return { ...miningPool, apy };
  });
}

export async function apys24h({ block = undefined, timestamp = undefined, chainId = DEFAULT_CHAIN_ID } = {}) {
  let timestampNow = timestamp ? timestamp : block ? await blockToTimestamp({ block: block, chainId: chainId }) : (Math.floor(Date.now() / 1000));
  let timestamp24ago = timestampNow - TWENTY_FOUR_HOURS;
  // let timestamp48ago = timestamp24ago - TWENTY_FOUR_HOURS;

  block = timestamp ? await timestampToBlock({ timestamp: timestamp, chainId: chainId }) : block;
  let block24ago = await timestampToBlock({ timestamp: timestamp24ago, chainId: chainId });
  // let block48ago = await timestampToBlock({ timestamp: timestamp48ago, chainId: chainId });

  const results = await apys({ block: block, chainId: chainId });
  const results24ago = await apys({ block: block24ago, chainId: chainId });

  return _apys.callback24h(results, results24ago);
}


const _info = {
  // TODO change this
  properties: [
    'treasuryFund',
    'migrator',
    'owner',
    'startReward',
    'mspace',
    'totalAllocPoint',
    'poolCount',
    'lpBalance',
    'lpAge',
    'lpAgeRemoved',
    'lpDeposited',
    'lpWithdrawn',
    'updatedAt'
  ],

  callback(results) {
    return ({
      treasuryFund: results.treasuryFund,
      migrator: results.migrator,
      owner: results.owner,
      startReward: Number(results.startReward),
      totalAllocPoint: Number(results.totalAllocPoint),
      poolCount: Number(results.poolCount),
      lpBalance: Number(results.lpBalance),
      lpAge: Number(results.lpAge),
      lpAgeRemoved: Number(results.lpAgeRemoved),
      lpDeposited: Number(results.lpDeposited),
      lpWithdrawn: Number(results.lpWithdrawn),
      updatedAt: Number(results.updatedAt)
    });
  }
};

const _pools = {
  properties: [
    'id',
    'pair',
    'allocPoint',
    'lastUpdate',
    'totalShare',
    'balance',
    'userCount',
    'lpBalance',
    'lpAge',
    'lpAgeRemoved',
    'lpDeposited',
    'lpWithdrawn',
    'timestamp',
    'block',
    'updatedAt',
    'entryUSD',
    'exitUSD',
    'mspaceHarvested',
    'mspaceHarvestedUSD'
  ],

  callback(results) {
    return results.map(({ id, pair, allocPoint, lastUpdate, totalShare, balance, userCount, lpBalance, lpAge, lpAgeRemoved, lpDeposited, lpWithdrawn, timestamp, block, updatedAt, entryUSD, exitUSD, mspaceHarvested, mspaceHarvestedUSD }) => ({
      id: Number(id),
      pair: pair,
      allocPoint: Number(allocPoint),
      lastUpdate: Number(lastUpdate),
      totalShare: BigInt(totalShare),
      userCount: Number(userCount),
      lpBalance: Number(lpBalance),
      lpAge: Number(lpAge),
      lpAgeRemoved: Number(lpAgeRemoved),
      lpDeposited: Number(lpDeposited),
      lpWithdrawn: Number(lpWithdrawn),
      addedTs: Number(timestamp),
      addedDate: new Date(timestamp * 1000),
      addedBlock: Number(block),
      lastUpdatedTs: Number(updatedAt),
      lastUpdatedDate: new Date(updatedAt * 1000),
      entryUSD: Number(entryUSD),
      exitUSD: Number(exitUSD),
      mspaceHarvested: Number(mspaceHarvested),
      mspaceHarvestedUSD: Number(mspaceHarvestedUSD)
    }));
  }
};

const _stakedValue = {
  properties: [
    'id',
    'liquidityTokenBalance',
    'pair { id, totalSupply, reserveMETA, reserveUSD }'
  ],

  callback(results) {
    return ({
      id: results.id,
      liquidityTokenBalance: Number(results.liquidityTokenBalance),
      totalSupply: Number(results.pair.totalSupply),
      totalValueMETA: Number(results.pair.reserveMETA),
      totalValueUSD: Number(results.pair.reserveUSD)
    })
  }
};

const _user = {
  properties: [
    'id',
    'address',
    'pool { id, pair, balance, lastUpdate, totalShare }',
    'amount',
    'entryUSD',
    'exitUSD',
    'mspaceHarvested',
    'mspaceHarvestedUSD',
  ],

  callback(results) {
    return results.map(entry => ({
      id: entry.id,
      address: entry.address,
      poolId: Number(entry.id.split("-")[0]),
      pool: entry.pool ? {
        id: entry.pool.id,
        pair: entry.pool.pair,
        balance: Number(entry.pool.balance),
        lastUpdate: Number(entry.pool.lastUpdate),
        totalShare: Number(entry.pool.totalShare)
      } : undefined,
      amount: Number(entry.amount),
      entryUSD: Number(entry.entryUSD),
      exitUSD: Number(entry.exitUSD),
      mspaceHarvested: Number(entry.mspaceHarvested),
      mspaceHarvestedUSD: Number(entry.mspaceHarvestedUSD),
    }));
  }
};

const _apys = {
  callback24h(results, results24h) {
    return results.map(result => {
      const result24h = results24h.find(e => e.id === result.id) || result;

      return ({
        ...result,

        lpBalanceChange: (result.lpBalance / result24h.lpBalance) * 100 - 100,
        lpBalanceChangeCount: result.lpBalance - result24h.lpBalance,

        userCountChange: (result.userCount / result24h.userCount) * 100 - 100,
        userCountChangeCount: result.userCount - result24h.userCount,

        mspaceHarvestedChange: (result.mspaceHarvested / result24h.mspaceHarvested) * 100 - 100,
        mspaceHarvestedChangeCount: result.mspaceHarvested - result24h.mspaceHarvested,
      });
    });
  }
}
