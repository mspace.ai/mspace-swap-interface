import mspace = require("./typings/mspace");
import blocks = require("./typings/blocks");
import charts = require("./typings/charts");
import exchange = require("./typings/exchange");
import mining = require("./typings/mining");
import bank = require("./typings/bank");
import extractor = require("./typings/extractor");
import timelock = require("./typings/timelock");
import lockup = require("./typings/lockup");
import utils = require("./typings/utils");

declare function timeseries({ blocks, timestamps, target }: {
    blocks?: number[];
    timestamps?: number[];
    target: Function;
}, targetArguments?: any): Promise<any>;

export {
    mspace,
    blocks,
    charts,
    exchange,
    mining,
    bank,
    extractor,
    timelock,
    lockup,
    utils,
    timeseries
};
