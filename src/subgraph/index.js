'use strict';

const { Promise } = require('bluebird')

const mspace = require('./queries/mspace');
const blocks = require('./queries/blocks');
const charts = require('./queries/charts');
const exchange = require('./queries/exchange');
const mining = require('./queries/mining');
const bank = require('./queries/bank')
const extractor = require('./queries/extractor')
const timelock =  require('./queries/timelock');
const lockup = require('./queries/lockup');
const utils = require('./utils');

async function timeseries({blocks = undefined, timestamps = undefined, target = undefined} = {}, targetArguments) {
	if(!target) { throw new Error("subgraph: Target function undefined"); }
	if(!blocks && !timestamps) { throw new Error("subgraph: Timeframe undefined"); }

	if(blocks) {
		return Promise.map(blocks, async (block) => ({
			block,
			data: await target({block, ...targetArguments})
		}));
	}

	else {
		return Promise.map(timestamps, async (timestamp) => ({
			timestamp,
			data: await target({timestamp, ...targetArguments})
		}));
	}
}

export {
	mspace,
	blocks,
	charts,
	exchange,
	mining,
  bank,
  extractor,
	timelock,
	lockup,
	utils,
	timeseries
}