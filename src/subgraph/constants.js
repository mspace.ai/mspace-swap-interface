
// const mainnetSubgraph = "beta.subgraph.mspace.ai";
const mainnetSubgraph = "beta.subgraph.mspace.ai";
const testnetSubgraph = "api-dex.gocheetah.co";

export const graphAPIEndpoints = {
  mining: {
    11: `https://${mainnetSubgraph}/api/subgraphs/name/mspace/mining`,
    12: `https://${testnetSubgraph}/api/subgraphs/name/mspace/mining`
  },
  bank: {
    11: `https://${mainnetSubgraph}/api/subgraphs/name/mspace/bank`,
    12: `https://${testnetSubgraph}/api/subgraphs/name/mspace/bank`
  },
  timelock: {
    11: `https://${mainnetSubgraph}/api/subgraphs/name/mspace/timelock`,
    12: `https://${testnetSubgraph}/api/subgraphs/name/mspace/timelock`
  },
  extractor: {
    11: `https://${mainnetSubgraph}/api/subgraphs/name/mspace/extractor`,
    12: `https://${testnetSubgraph}/api/subgraphs/name/mspace/extractor`
  },
  exchange: {
    11: `https://${mainnetSubgraph}/api/subgraphs/name/mspace/exchange`,
    12: `https://${testnetSubgraph}/api/subgraphs/name/mspace/exchange`
  },
  blocklytics: {
    11: `https://${mainnetSubgraph}/api/subgraphs/name/mspace/blocks`,
    12: `https://${testnetSubgraph}/api/subgraphs/name/mspace/blocks`,
  },
  lockup: {
    11: `https://${mainnetSubgraph}/api/subgraphs/name/mspace/lockup`,
    12: `https://${testnetSubgraph}/api/subgraphs/name/mspace/lockup`
  }
}

export const graphWSEndpoints = {
  bank: {
    11: `wss://${mainnetSubgraph}/ws/subgraphs/name/mspace/bank`,
    12: `wss://${testnetSubgraph}/ws/subgraphs/name/mspace/bank`
  },
  exchange: {
    11: `wss://${mainnetSubgraph}/ws/subgraphs/name/mspace/exchange`,
    12: `wss://${testnetSubgraph}/ws/subgraphs/name/mspace/exchange`
  },
  blocklytics: {
    11: `wss://${mainnetSubgraph}/ws/subgraphs/name/mspace/blocks`,
    12: `wss://${testnetSubgraph}/ws/subgraphs/name/mspace/blocks`
  }
}

export const bankAddress = {
  11: "0x41c2C7C4a7dC266ae4F16558CB54b904CDCA279d".toLocaleLowerCase(),
  12: "0xF74475AdD8CB29794D8105dc7E4ece99bFC38575".toLocaleLowerCase()
}
export const extractorAddress = {
  11: "0x27BE7d67B2d935E7ABCC93414D154F2492094bc9".toLocaleLowerCase(),
  12: "0xc0DA7eB11913767d1D5Cbe7a5DC64Ff31b00433a".toLocaleLowerCase()
}
export const miningAddress = {
  11: "0xb8692569c8129Ff24624552ec790d17b6B725104".toLocaleLowerCase(),
  12: "0x60550807c387A7050D0c3B2c06E8d7b14211D14d".toLocaleLowerCase()
}
export const mspaceAddress = {
  11: "0x885c993fE9Dd57F6ACcB384692395D3cDD2b6F77".toLocaleLowerCase(),
  12: "0x8a7C8755ff2d52F41346841176bEb54D4A3ad934".toLocaleLowerCase()
}
export const factoryAddress = {
  11: "0xc889864D24BD8c68cC4E7b4E3E1dFDc2455B83a3".toLocaleLowerCase(),
  12: "0x044a1a5B0Dd5a337CC815C314170657225D15d52".toLocaleLowerCase()
}

export const DEFAULT_CHAIN_ID = process.env.REACT_APP_DEFAULT_CHAIN_ID ? Number.parseInt(process.env.REACT_APP_DEFAULT_CHAIN_ID) : 11;

export const TWENTY_FOUR_HOURS = 86400;

