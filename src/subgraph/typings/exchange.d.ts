type Token = {
  id: string,
  symbol: string,
  name: string,
  decimals: number,
  totalSupply: number,
  volume: number,
  volumeUSD: number,
  untrackedVolumeUSD: number,
  txCount: number,
  liquidity: number,
  derivedMETA: number
};

type Token24h = {
  priceUSD: number,
  priceUSDChange: number,
  priceUSDChangeCount: number,

  liquidityUSD: number,
  liquidityUSDChange: number,
  liquidityUSDChangeCount: number,

  liquidityMETA: number,
  liquidityMETAChange: number,
  liquidityMETAChangeCount: number,

  volumeUSDOneDay: number,
  volumeUSDChange: number,
  volumeUSDChangeCount: number,

  untrackedVolumeUSDOneDay: number,
  untrackedVolumeUSDChange: number,
  untrackedVolumeUSDChangeCount: number,

  txCountOneDay: number,
  txCountChange: number,
  txCountChangeCount: number,
};

type TokenDayData = {
  id: string,
  date: Date,
  timestamp: number,
  volume: number,
  volumeMETA: number,
  volumeUSD: number,
  liquidity: number,
  liquidityMETA: number,
  liquidityUSD: number,
  priceUSD: number,
  txCount: number,
};

export function token({ block, timestamp, token_address, chainId }: {
  block?: number;
  timestamp?: number;
  token_address: string;
  chainId?: number;
}): Promise<Token>;

export function token24h({ block, timestamp, token_address, chainId }: {
  block?: number;
  timestamp?: number;
  token_address: string;
  chainId?: number;
}): Promise<Token & Token24h>;

export function tokenHourData({ minTimestamp, maxTimestamp, minBlock, maxBlock, token_address, chainId }: {
  minTimestamp?: number;
  maxTimestamp?: number;
  minBlock?: number;
  maxBlock?: number;
  token_address: string;
  chainId?: number;
}): Promise<(Token & { timestamp: number })[]>;

export function tokenDayData({ minTimestamp, maxTimestamp, minBlock, maxBlock, token_address, chainId }: {
  minTimestamp?: number;
  maxTimestamp?: number;
  minBlock?: number;
  maxBlock?: number;
  token_address: string;
  chainId?: number;
}): Promise<TokenDayData[]>;

export function observeToken({ token_address, chainId }: {
  token_address: string;
  chainId?: number;
}): {
  subscribe({ next, error, complete }: {
    next?(data: Token): any;
    error?(error: any): any;
    complete?: Function;
  }): any;
};

export function tokens({ block, timestamp, max, chainId }?: {
  block?: number;
  timestamp?: number;
  max?: number;
  chainId?: number;
}): Promise<Token[]>;

export function tokens24h({ block, timestamp, max, chainId }?: {
  block?: number;
  timestamp?: number;
  max?: number;
}): Promise<(Token & Token24h)[]>;

export function observeTokens({ chainId }?: {
  chainId?: number;
}): {
  subscribe({ next, error, complete }: {
    next?(data: Token[]): any;
    error?(error: any): any;
    complete?: Function;
  }): any;
};

export function getPairAddresses({ tokenPairs, chainId }?: {
  tokenPairs: [string | undefined, string | undefined][],
  chainId?: number;
}): Promise<(MinimalPair | undefined)[]>;

type MinimalPair = {
  id: string,
  token0: {id: string},
  token1: {id: string},
}

type Pair = {
  id: string,
  token0: {
    id: string,
    name: string,
    symbol: string,
    totalSupply: number,
    derivedMETA: number,
  },
  token1: {
    id: string,
    name: string,
    symbol: string,
    totalSupply: number,
    derivedMETA: number,
  },
  reserve0: number,
  reserve1: number,
  totalSupply: number,
  reserveMETA: number,
  reserveUSD: number,
  trackedReserveMETA: number,
  token0Price: number,
  token1Price: number,
  volumeToken0: number,
  volumeToken1: number,
  volumeUSD: number,
  untrackedVolumeUSD: number,
  txCount: number,
}

type Pair24h = {
  trackedReserveUSD: number,
  trackedReserveUSDChange: number,
  trackedReserveUSDChangeCount: number,

  trackedReserveMETAChange: number,
  trackedReserveMETAChangeCount: number,

  volumeUSDOneDay: number,
  volumeUSDChange: number,
  volumeUSDChangeCount: number,

  untrackedVolumeUSDOneDay: number,
  untrackedVolumeUSDChange: number,
  untrackedVolumeUSDChangeCount: number,

  txCountOneDay: number,
  txCountChange: number,
  txCountChangeCount: number
};

type PairDayData = {
  id: string,
  date: Date,
  timestamp: number,
  volumeUSD: number,
  volumeToken0: number,
  volumeToken1: number,
  liquidityUSD: number,
  txCount: number
};

export function pair({ block, timestamp, pair_address, chainId }: {
  block?: number;
  timestamp?: number;
  pair_address: string;
  chainId?: number;
}): Promise<Pair>;

export function pair24h({ block, timestamp, pair_address, chainId }: {
  block?: number;
  timestamp?: number;
  pair_address: string;
  chainId?: number;
}): Promise<Pair & Pair24h>;

export function pairHourData({ minTimestamp, maxTimestamp, minBlock, maxBlock, pair_address, chainId }: {
  minTimestamp?: number;
  maxTimestamp?: number;
  minBlock?: number;
  maxBlock?: number;
  pair_address: string;
  chainId?: number;
}): Promise<(Pair & { timestamp: number })[]>;

export function pairDayData({ minTimestamp, maxTimestamp, minBlock, maxBlock, pair_address, chainId }: {
  minTimestamp?: number;
  maxTimestamp?: number;
  minBlock?: number;
  maxBlock?: number;
  pair_address: string;
  chainId?: number;
}): Promise<PairDayData[]>;

export function observePair({ pair_address, chainId }: {
  pair_address: string;
  chainId?: number;
}): {
  subscribe({ next, error, complete }: {
    next?(data: Pair): any;
    error?(error: any): any;
    complete?: Function;
  }): any;
};

export function pairs({ block, timestamp, max, pair_addresses, chainId }?: {
  block?: number;
  timestamp?: number;
  max?: number;
  pair_addresses?: string[];
  chainId?: number;
}): Promise<Pair[]>;

export function pairs24h({ block, timestamp, max, chainId }?: {
  block?: number;
  timestamp?: number;
  max?: number;
  chainId?: number;
}): Promise<(Pair & Pair24h)[]>;

export function observePairs({ chainId }?: {
  chainId?: number;
}): {
  subscribe({ next, error, complete }: {
    next(data: Pair): any;
    error?(error: any): any;
    complete?: Function;
  }): any;
};



type MetaPrice = number;

export function metaPrice({ block, timestamp, chainId }?: {
  block?: number;
  timestamp?: number;
  chainId?: number;
}): Promise<MetaPrice>;

export function metaPriceHourly({ minTimestamp, maxTimestamp, minBlock, maxBlock, chainId }?: {
  minTimestamp?: number;
  maxTimestamp?: number;
  minBlock?: number;
  maxBlock?: number;
  chainId?: number;
}): Promise<(MetaPrice & { timestamp: number })[]>;

export function observeMetaPrice({ chainId }?: {
  chainId?: number;
}): {
  subscribe({ next, error, complete }: {
    next?(data: MetaPrice): any;
    error?(error: any): any;
    complete?: Function;
  }): any;
};



type Factory = {
  pairCount: number;
  volumeUSD: number;
  volumeMETA: number;
  untrackedVolumeUSD: number;
  liquidityUSD: number;
  liquidityMETA: number;
  txCount: number;
  tokenCount: number;
  userCount: number;
}

export function factory({ block, timestamp, chainId }?: {
  block?: number;
  timestamp?: number;
  chainId?: number;
}): Promise<Factory>;

export function observeFactory({ chainId }?: {
  chainId?: number;
}): {
  subscribe({ next, error, complete }: {
    next?(data: Factory): any;
    error?(error: any): any;
    complete?: Function;
  }): any;
};



type DayData = {
  id: number,
  date: Date,
  volumeMETA: number,
  volumeUSD: number,
  liquidityMETA: number,
  liquidityUSD: number,
  txCount: number,
}

export function dayData({ minTimestamp, maxTimestamp, minBlock, maxBlock, chainId }?: {
  minTimestamp?: number;
  maxTimestamp?: number;
  minBlock?: number;
  maxBlock?: number;
  chainId?: number;
}): Promise<DayData[]>;



type TwentyFourHourData = {
  id: string,
  volumeUSD: number,
  volumeMETA: number,
  untrackedVolumeUSD: number,
  liquidityMETA: number,
  liquidityUSD: number,
  txCount: number,
  pairCount: number
}

export function twentyFourHourData({ block, timestamp, chainId }?: {
  block?: number;
  timestamp?: number;
  chainId?: number;
}): Promise<TwentyFourHourData>