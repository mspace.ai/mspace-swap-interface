type Price = number;

export function priceUSD({ block, timestamp, chainId }?: {
  block?: number;
  timestamp?: number;
  chainId?: number;
}): Promise<Price>;

export function priceMETA({ block, timestamp, chainId }?: {
  block?: number;
  timestamp?: number;
  chainId?: number;
}): Promise<Price>;

export function observePriceMETA({ chainId }?: {
  chainId?: number;
}): {
  subscribe({ next, error, complete }: {
    next?(data: Price): any;
    error?(error: any): any;
    complete?: Function;
  }): any;
};
