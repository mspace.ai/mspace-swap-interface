export function timestampToBlock({timestmap, chainId}?: {
  timestmap: number;
  chainId?: number;
}): Promise<number>;

export function timestampsToBlocks({timestamps, chainId}?: {
  timestamps: number[];
  chainId?: number;
}): Promise<number[]>;

export function blockToTimestamp({block, chainId}?: {
  block: number;
  chainId?: number;
}): Promise<number>;

export function getAverageBlockTime({ block, timestamp, chainId }?: {
  block?: number;
  timestamp?: number;
  chainId?: number;
}): Promise<number>;