type Info = {
  decimals: number;
  name: string;
  mspace: string;
  symbol: string;
  totalSupply: number;
  ratio: number;
  xMSpaceMinted: number;
  xMSpaceBurned: number;
  mspaceStaked: number;
  mspaceStakedUSD: number;
  mspaceHarvested: number;
  mspaceHarvestedUSD: number;
  xMSpaceAge: number;
  xMSpaceAgeDestroyed: number;
  updatedAt: number;
}

export function info({ block, timestamp, chainId }?: {
  block?: number;
  timestamp?: number;
  chainId?: number;
}): Promise<Info>;

export function observeInfo({ chainId }?: {
  chainId?: number;
}): {
  subscribe({ next, error, complete }: {
    next?(data: Info): any;
    error?(error: any): any;
    complete?: Function;
  }): any;
};



type User = {
  xMSpace: number;
  xMSpaceIn: number;
  xMSpaceOut: number;
  xMSpaceMinted: number;
  xMSpaceBurned: number;
  xMSpaceOffset: number;
  xMSpaceAge: number;
  xMSpaceAgeDestroyed: number;
  mspaceStaked: number;
  mspaceStakedUSD: number;
  mspaceHarvested: number;
  mspaceHarvestedUSD: number;
  mspaceIn: number;
  mspaceOut: number;
  usdOut: number;
  usdIn: number;
  updatedAt: number;
  mspaceOffset: number;
  usdOffset: number;
}

export function user({ block, timestamp, user_address, chainId }: {
  block?: number;
  timestamp?: number;
  user_address: string;
  chainId?: number;
}): Promise<User>;
