type LatestBlock = {
  id: string;
  number: number;
  timestamp: number;
  date: Date;
}

export function latestBlock({ chainId }: {
  chainId?: number;
}): Promise<LatestBlock>;

export function observeLatestBlock({ chainId }: {
  chainId?: number;
}): {
  subscribe({ next, error, complete }: {
    next?(data: LatestBlock): any;
    error?(error: any): any;
    complete?: Function;
  }): any;
};



type GetBlock = {
  id: string;
  number: number;
  timestamp: number;
  author: string;
  difficulty: number;
  gasUsed: number;
  gasLimit: number;
}

export function getBlock({ block, timestamp, chainId }: {
  block?: number;
  timestamp?: number;
  chainId?: number;
}): Promise<GetBlock>;
