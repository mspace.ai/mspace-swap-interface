type Info = {
  treasuryFund: string;
  migrator: string;
  owner: string;
  startReward: number;
  totalAllocPoint: number;
  poolCount: number;
  lpBalance: number;
  lpAge: number;
  lpAgeRemoved: number;
  lpDeposited: number;
  lpWithdrawn: number;
  updatedAt: number;
}

export function info({ block, timestamp, chainId }?: {
  block?: number;
  timestamp?: number;
  chainId?: number;
}): Promise<Info>;



type Pool = {
  id: number,
  pair: string,
  allocPoint: number,
  lastUpdate: number,
  totalShare: number,
  userCount: number,
  lpBalance: number,
  lpAge: number,
  lpAgeRemoved: number,
  lpDeposited: number,
  lpWithdrawn: number,
  addedTs: number,
  addedDate: Date,
  addedBlock: number,
  lastUpdatedTs: number,
  lastUpdatedDate: Date,
  entryUSD: number,
  exitUSD: number,
  mspaceHarvested: number,
  mspaceHarvestedUSD: number
}

export function pool({ block, timestamp, pool_id, pool_address, chainId }: {
  block?: number;
  timestamp?: number;
  pool_id: number;
  pool_address: string;
  chainId?: number;
}): Promise<Pool>;

export function pools({ block, timestamp, chainId }?: {
  block?: any;
  timestamp?: any;
  chainId?: number;
}): Promise<Pool[]>;



type StakedValue = {
  id: string;
  liquidityTokenBalance: number;
  totalSupply: number;
  totalValueMETA: number;
  totalValueUSD: number;
}

export function stakedValue({ block, timestamp, token_address, chainId }: {
  block?: number;
  timestamp?: number;
  token_address: string;
  chainId?: number;
}): Promise<StakedValue>;



type User = {
  id: string,
  address: string,
  poolId: number,
  pool: {
    id: string,
    pair: string,
    balance: number,
    lastUpdate: number,
    totalShare: number
  } | undefined,
  amount: number,
  entryUSD: number,
  exitUSD: number,
  mspaceHarvested: number,
  mspaceHarvestedUSD: number,
}

export function user({ block, timestamp, user_address, chainId }: {
  block?: number;
  timestamp?: number;
  user_address: string;
  chainId?: number;
}): Promise<User[]>;

export function users({ block, timestamp, chainId }?: {
  block?: number;
  timestamp?: number;
  chainId?: number;
}): Promise<User[]>;



export function apys({ block, timestamp, chainId }?: {
  block?: number;
  timestamp?: number;
  chainId?: number;
}): Promise<(Pool & { apy: number })[]>;

export function apys24h({ block, timestamp, chainId }?: {
  block?: number;
  timestamp?: number;
  chainId?: number;
}): Promise<(Pool & {
  apy: number
  lpBalanceChange: number,
  lpBalanceChangeCount: number,
  userCountChange: number,
  userCountChangeCount: number,
  mspaceHarvestedChange: number,
  mspaceHarvestedChangeCount: number
})[]>;