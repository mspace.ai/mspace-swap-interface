type User = {
  id: string,
  address: string,
  amount: number,
  rewardDebt: bigint,
  pool: {
    id: string,
    balance: number,
    allocPoint: number,
    lastUpdate: number,
    totalShare: number,
  },
  mspaceHarvestedSinceLockup: number,
}

export function user({ block, timestamp, user_address, chainId }: {
  block?: number;
  timestamp?: number;
  user_address: string;
  chainId?: number;
}): Promise<User[]>;