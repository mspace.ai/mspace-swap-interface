type Info = {
  address: string;
  mspaceServed: number;
}

export function info({ block, timestamp, chainId }?: {
  block?: number;
  timestamp?: number;
  chainId?: number;
}): Promise<Info>;



type Servings = {
  serverAddress: string,
  tx: string,
  pair: string,
  token0: string,
  token1: string,
  mspaceServed: number,
  block: number,
  timestamp: number,
  date: Date
}

export function servings({ minTimestamp, maxTimestamp, minBlock, maxBlock, max, chainId }?: {
  minTimestamp?: number;
  maxTimestamp?: number;
  minBlock?: number;
  maxBlock?: number;
  max?: number;
  chainId?: number;
}): Promise<Servings[]>;



type Servers = {
  serverAddress: string,
  mspaceServed: number,
  servings: {
    tx: string,
    block: number,
    pair: string,
    mspaceServed: number
  }[]
}

export function servers({ block, timestamp, max, chainId }?: {
  block?: number;
  timestamp?: number;
  max?: number;
  chainId?: number;
}): Promise<Servers[]>;



type PendingServings = {
  address: string,
  token0: {
    id: string,
    name: string,
    symbol: string
  },
  token1: {
    id: string,
    name: string,
    symbol: string
  },
  valueUSD: number
}

export function pendingServings({ block, timestamp, max, chainId }?: {
  block?: number;
  timestamp?: number;
  max?: number;
  chainId?: number;
}): Promise<PendingServings[]>;

export function observePendingServings({ chainId }?: {
  chainId?: number;
}): {
  subscribe({ next, error, complete }: {
    next?(data: PendingServings): any;
    error?(error: any): any;
    complete?: Function;
  }): any;
};